{{#if DeliveryDetailsValue}}
<div class="order-comments-containers order-comments-freight-agreement">
  <label for="delivery-details-message" class="delivery-details-message">
      Freight Agreement
    </label>
    <p class="order-wizard-showpayments-module-payment-methods-summary delivery-details-text">
      {{profileDeliveryDetail}}
    </p>
</div>
{{/if}}
<!-- <div id="order-comments-container" class="order-comments-containers {{#if isReview}}Review_page{{/if}}">
	{{#unless isReview}}
	 <label for="instructions-message" class="instructionlabel">
      Delivery Details
    </label>
    {{/unless}}
    {{#if isReview}}
        {{#if model.options.custbody_delivery_details}}
            <label for="instructions-message" class="instructionlabel">
              Delivery Details
            </label>
            <p class="web_notes">{{model.options.custbody_delivery_details}}</p>
        {{else}}
            <label for="instructions-message" class="instructionlabel">
              Delivery Details
            </label>
            <p class="order-comments-input" type="text" name="custbody_special_instructions" > {{profileDeliveryDetail}}
        </p>
        {{/if}}
    {{else}}
        <p class="order-comments-input" type="text" name="custbody_special_instructions"> {{profileDeliveryDetail}}
        </p>
    {{/if}}
</div> -->

<!--
  Available helpers:
  {{ getExtensionAssetsPath "img/image.jpg"}} - reference assets in your extension
  
  {{ getExtensionAssetsPathWithDefault context_var "img/image.jpg"}} - use context_var value i.e. configuration variable. If it does not exist, fallback to an asset from the extension assets folder
  
  {{ getThemeAssetsPath context_var "img/image.jpg"}} - reference assets in the active theme
  
  {{ getThemeAssetsPathWithDefault context_var "img/theme-image.jpg"}} - use context_var value i.e. configuration variable. If it does not exist, fallback to an asset from the theme assets folder
-->