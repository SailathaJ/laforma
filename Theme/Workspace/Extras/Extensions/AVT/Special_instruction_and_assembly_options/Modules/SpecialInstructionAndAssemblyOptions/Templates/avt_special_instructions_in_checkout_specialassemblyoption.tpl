<!-- <h2 class="order-comments-title">{{translate 'Send Available'}}</h2> -->
<div id="order-comments-container" class="order-comments-containers available-section">
	{{#unless isReview}}
	 <label for="instructions-message" class="instructionlabel">
      
    </label>
    {{/unless}}
    {{#if isReview}}
        
            <input type="checkbox" name="custbody_avt_whiteglove" class="order-available-checkbox" value="{{model.options.custbody_avt_whiteglove_service}}" {{#if sendAvailable_Status}} checked {{/if}} data-action="availableClick" disabled="disabled"> <strong> Quote White Glove Delivery Service</strong>
        
    {{else}} 
        <input type="checkbox" name="custbody_avt_whiteglove" class="order-available-checkbox" value="{{model.options.custbody_avt_whiteglove_service}}" {{#if sendAvailable_Status}} checked {{/if}} data-action="availableClick"> <strong>Quote White Glove Delivery Service</strong>
    {{/if}}
</div>


<!--
  Available helpers:
  {{ getExtensionAssetsPath "img/image.jpg"}} - reference assets in your extension
  
  {{ getExtensionAssetsPathWithDefault context_var "img/image.jpg"}} - use context_var value i.e. configuration variable. If it does not exist, fallback to an asset from the extension assets folder
  
  {{ getThemeAssetsPath context_var "img/image.jpg"}} - reference assets in the active theme
  
  {{ getThemeAssetsPathWithDefault context_var "img/theme-image.jpg"}} - use context_var value i.e. configuration variable. If it does not exist, fallback to an asset from the theme assets folder
-->