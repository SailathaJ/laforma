<div id="order-comments-container" class="order-comments-containers {{#if isReview}}Review_page{{/if}}">
	{{#unless isReview}}
	 <label for="instructions-message" class="instructionlabel">
      Special Delivery Instructions
    </label>
    {{/unless}}
    {{#if isReview}}
        {{#if model.options.custbody_special_instructions}}
            <label for="instructions-message" class="instructionlabel">
              Special Delivery Instructions
            </label>
            <p class="web_notes">{{model.options.custbody_special_instructions}}</p>
        {{else}}
            <label for="instructions-message" class="instructionlabel">
              Special Delivery Instructions
            </label>
            <textarea class="order-comments-input" type="text" name="custbody_special_instructions"  value="{{model.options.custbody_special_instructions}}"> {{model.options.custbody_special_instructions}}
        </textarea>
        {{/if}}
    {{else}}
        <textarea class="order-comments-input" type="text" name="custbody_special_instructions"  value="{{model.options.custbody_special_instructions}}"> {{model.options.custbody_special_instructions}}
        </textarea>
    {{/if}}
</div>

<!--
  Available helpers:
  {{ getExtensionAssetsPath "img/image.jpg"}} - reference assets in your extension
  
  {{ getExtensionAssetsPathWithDefault context_var "img/image.jpg"}} - use context_var value i.e. configuration variable. If it does not exist, fallback to an asset from the extension assets folder
  
  {{ getThemeAssetsPath context_var "img/image.jpg"}} - reference assets in the active theme
  
  {{ getThemeAssetsPathWithDefault context_var "img/theme-image.jpg"}} - use context_var value i.e. configuration variable. If it does not exist, fallback to an asset from the theme assets folder
-->