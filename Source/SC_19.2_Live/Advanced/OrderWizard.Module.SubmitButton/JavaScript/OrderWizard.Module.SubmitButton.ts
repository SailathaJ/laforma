/// <amd-module name="OrderWizard.Module.SubmitButton"/>
/// <reference path="../../../Commons/Utilities/JavaScript/UnderscoreExtended.d.ts" />

import WizardModule = require('../../Wizard/JavaScript/Wizard.Module');
import Configuration = require('../../SCA/JavaScript/SC.Configuration');
import order_wizard_submitbutton_module_tpl = require('../../OrderWizard.Module.SubmitButton/Templates/order_wizard_submitbutton_module.tpl');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

export = WizardModule.extend({
    template: order_wizard_submitbutton_module_tpl,

    render: function() {
        if (
            this.wizard.currentStep === 'review' ||
            !this.options.is_below_summary ||
            (this.options.is_below_summary && Configuration.get('promocodes.allowMultiples', true))
        ) {
            this._render();
            this.trigger('ready', true);
        }
    },

    // @method getContinueButtonLabel @returns {String}
    getContinueButtonLabel: function() {
        const current_step = this.wizard.getCurrentStep();
        let label = Utils.translate('Place Order');

        if (current_step) {
            label = current_step.getContinueButtonLabel();
        }

        return label;
    },

    // @method getContext @return OrderWizard.Module.SubmitButton.Context
    getContext: function() {
        // @class OrderWizard.Module.SubmitButton.Context
        return {
            // @property {Boolean} showWrapper
            showWrapper: !!this.options.showWrapper,
            // @property {String} wrapperClass
            wrapperClass: this.options.wrapperClass,
            // @property {String} continueButtonLabel
            continueButtonLabel: this.getContinueButtonLabel() || ''
        };
    }
});
