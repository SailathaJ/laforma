/// <amd-module name="StoreLocator.Collection"/>
// @module StoreLocator

import LocationCollection = require('../../Location.SCA/JavaScript/Location.Collection');
import StoreLocatorModel = require('./StoreLocator.Model');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// @class StoreLocator.Collection @extend Backbone.Collection
const StoreLocatorCollection: any = LocationCollection.extend({
    // @property {StoreLocator.Model} model
    model: StoreLocatorModel,

    // @property {String} url
    url: Utils.getAbsoluteUrl('services/StoreLocator.Service.ss')
});

export = StoreLocatorCollection;
