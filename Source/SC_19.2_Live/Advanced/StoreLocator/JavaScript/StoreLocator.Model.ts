/// <amd-module name="StoreLocator.Model"/>
// @module StoreLocator

import LocationModel = require('../../Location.SCA/JavaScript/Location.Model');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

const StoreLocatorModel: any = LocationModel.extend({
    // @property {String} urlRoot
    urlRoot: Utils.getAbsoluteUrl('services/StoreLocator.Service.ss')
});

export = StoreLocatorModel;
