/// <amd-module name="StoreLocator.Upgrade.View"/>
// @module StoreLocator.Upgrade.View

import store_locator_upgrade = require('../Templates/store_locator_upgrade.tpl');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');

const StoreLocatorUpgradeView: any = BackboneView.extend({
    template: store_locator_upgrade,

    // @property {Object} attributes
    attributes: {
        id: 'StoreLocatorUpgrade',
        class: 'StoreLocatorUpgrade'
    },

    // @method initialize
    // @param {Object} options
    initialize: function initialize(options) {
        this.application = options.application;
    }
});

export = StoreLocatorUpgradeView;
