/// <amd-module name="StoreLocator.List.All.Store.View"/>
// @module StoreLocator.List.All.Store.View

import store_locator_list_all_store_tpl = require('../Templates/store_locator_list_all_store.tpl');
import Configuration = require('../../SCA/JavaScript/SC.Configuration');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');

const StoreLocatorListAllStoreView: any = BackboneView.extend({
    template: store_locator_list_all_store_tpl,

    // @method initialize
    // @param {Object} options
    initialize: function initialize(options) {
        this.index = options.index;
    },

    // @method getContext
    // @return StoreLocator.ListAll.Store.View.Context
    getContext: function getContext() {
        return {
            // @property {String} name
            name: this.model.get('name'),
            // @property {String} storeId
            storeId: this.model.get('internalid'),
            // @property {String} touchpoint
            touchpoint: Configuration.get('siteSettings.isHttpsSupported') ? 'home' : 'storelocator'
        };
    }
});

export = StoreLocatorListAllStoreView;
