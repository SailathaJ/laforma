/// <amd-module name="Overview.Shipping.View"/>
// Overview.Shipping.View.js
// -----------------------

import AddressDetailsView = require('../../../Commons/Address/JavaScript/Address.Details.View');
import overview_shipping_tpl = require('../Templates/overview_shipping.tpl');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');

const OverviewShippingView: any = BackboneView.extend({
    template: overview_shipping_tpl,

    initialize: function() {},
    childViews: {
        'Address.Details': function() {
            return new AddressDetailsView({
                hideDefaults: true,
                hideActions: true,
                model: this.model,
                hideSelector: true
            });
        }
    },
    // @method getContext @returns {Overview.Shipping.View.Context}
    getContext: function() {
        // @class Overview.Shipping.View.Context
        return {
            // @property {Boolean} hasDefaultShippingAddress
            hasDefaultShippingAddress: !!this.model,
            // @property {String} shippingAddressInternalid
            shippingAddressInternalid: this.model && this.model.get('internalid')
        };
    }
});

export = OverviewShippingView;
