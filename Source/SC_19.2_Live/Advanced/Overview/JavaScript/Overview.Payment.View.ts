/// <amd-module name="Overview.Payment.View"/>
// Overview.Payment.View.js
// -----------------------

import PaymentMethodHelper = require('../../PaymentMethod/JavaScript/PaymentMethod.Helper');
import overview_payment_tpl = require('../Templates/overview_payment.tpl');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');

// home page view
const OverviewPaymentView: any = BackboneView.extend({
    template: overview_payment_tpl,

    initialize: function() {},

    childViews: {
        'CreditCard.View': function() {
            const view = PaymentMethodHelper.getCreditCardView();
            return new view({
                model: this.model,
                hideSelector: true
            });
        }
    },

    // @method getContext @returns {Overview.Payment.View.Context}
    getContext: function() {
        // @class Overview.Payment.View.Context
        return {
            // @property {Boolean} hasDefaultCreditCard
            hasDefaultCreditCard: !!this.model,
            // @property {String} creditCardInternalid
            creditCardInternalid: this.model && this.model.get('internalid')
        };
    }
});

export = OverviewPaymentView;
