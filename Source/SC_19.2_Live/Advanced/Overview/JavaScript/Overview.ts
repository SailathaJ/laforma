/// <amd-module name="Overview"/>
// @module Overview
// Defines the Overview module (Router)

import * as _ from 'underscore';

import OverviewHomeView = require('./Overview.Home.View');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');
import MyAccountStandAlone = require('../../MyAccountApplication/JavaScript/MyAccount.StandAlone');
import OverviewHomeStandaloneView = require('./Overview.Home.Standalone.View');

// @class Overview @extends ApplicationModule
const Overview: any = {
    MenuItems: [
        function(application) {
            if (
                (!Utils.isPhoneDevice() &&
                    application.getConfig('siteSettings.sitetype') === 'STANDARD') ||
                application.getConfig('siteSettings.sitetype') !== 'STANDARD'
            ) {
                return {
                    id: 'home',
                    name: Utils.translate('Overview'),
                    url: 'overview',
                    index: 0
                };
            }
        }
    ],

    mountToApp: function(application) {
        const isStandalone: boolean = application.name === MyAccountStandAlone.getName();
        const pageType = application.getComponent('PageType');

        pageType.registerPageType({
            name: 'MyAccountOverview',
            routes: ['', '?*params', 'overview', 'overview?*params'],
            view: isStandalone ? OverviewHomeStandaloneView : OverviewHomeView,
            defaultTemplate: {
                name: 'overview_home.tpl',
                displayName: 'My Account Overview Default',
                thumbnail: Utils.getThemeAbsoluteUrlOfNonManagedResources('img/default-layout-myaccount-overview.png')
            }
        });
    }
};

export = Overview;
