/// <amd-module name="Overview.Profile.View"/>
// Overview.Profile.View.js
// -----------------------

import Configuration = require('../../SCA/JavaScript/SC.Configuration');
import overview_profile_tpl = require('../Templates/overview_profile.tpl');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

const OverviewProfileView: any = BackboneView.extend({
    template: overview_profile_tpl,

    initialize: function() {},
    // @method getContext @returns {Overview.Banner.View.Context}
    getContext: function() {
        const first_name = this.model.get('firstname') || '';
        const middle_name = this.model.get('middlename') || '';
        const last_name = this.model.get('lastname') || '';
        const company_name = this.model.get('companyname');

        // @class Overview.Banner.View.Context
        return {
            // @property {String} name
            name: first_name + ' ' + middle_name + ' ' + last_name,
            // @property {Boolean} isCompany
            isCompany: !!company_name,
            // @property {Boolean} isNameTitle
            isNameTitle: !company_name,
            // @property {String} companyName
            companyName: company_name || '',
            // @property {String} email
            email: this.model.get('email'),
            // @property {String} phone
            phone: Utils.formatPhone(
                this.model.get('phone') || '',
                Configuration.get('siteSettings.phoneformat')
            )
        };
    }
});

export = OverviewProfileView;
