/// <amd-module name="OrderHistory.Line.Model"/>

import '../../../Commons/Utilities/JavaScript/Utils';

import TransactionLineModel = require('../../../Commons/Transaction/JavaScript/Transaction.Line.Model');

const OrderHistoryLineModel: any = TransactionLineModel.extend();

export = OrderHistoryLineModel;
