/// <amd-module name="OrderHistory.Line.Collection"/>

import TransactionLineCollection = require('../../../Commons/Transaction/JavaScript/Transaction.Line.Collection');
import OrderHistoryLineModel = require('./OrderHistory.Line.Model');

const OrderHistoryLineCollection: any = TransactionLineCollection.extend({
    model: OrderHistoryLineModel
});

export = OrderHistoryLineCollection;
