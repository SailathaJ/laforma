/// <amd-module name="OrderHistory.Cancel.View"/>

import order_history_cancel_tpl = require('../Templates/order_history_cancel.tpl');

import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');

// @class ReturnAuthorization.Cancel.View @extend Backbone.Vie
const OrderHistoryCancelview: any = BackboneView.extend({
    template: order_history_cancel_tpl,

    events: {
        'click [data-action="delete"]': 'confirm'
    },

    initialize: function(options) {
        this.application = options.application;
    },

    confirm: function() {
        this.model.set('status', 'cancelled', { silent: false });
        this.model.save(
            // The only purpose of this parameter is to match the url of the cached resource
            // So that it matches the original request and removes it from the cache after
            // updating it
            { recordtype: 'salesorder' }
        );
    },

    // @method getContext @returns OrderHistory.Cancel.View.Context
    getContext: function() {
        // @class OrderHistory.Cancel.View.Context
        return {
            // @property {OrderHistory.Model} model
            model: this.model
        };
    }
});

export = OrderHistoryCancelview;
