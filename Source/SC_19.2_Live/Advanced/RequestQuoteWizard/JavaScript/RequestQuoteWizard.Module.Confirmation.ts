/// <amd-module name="RequestQuoteWizard.Module.Confirmation"/>

import WizardModule = require('../../Wizard/JavaScript//Wizard.Module');
import requestquote_wizard_module_confirmation_tpl = require('../Templates/requestquote_wizard_module_confirmation.tpl');
import Configuration = require('../../SCA/JavaScript/SC.Configuration');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');
import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');

// @class RequestQuoteWizard.Module.Confirmation @extends Wizard.Module
export = WizardModule.extend({
    // @property {Function} template
    template: requestquote_wizard_module_confirmation_tpl,

    // @method present Override default implementation so when the modules is being rendered we set the salesorderid parameter into the url
    // @return {Void}
    present: function() {
        const confirmation = this.model.get('confirmation') || {};
        // store current order id in the hash so it is available even when the checkout process ends.
        const newHash = Utils.addParamsToUrl(Backbone.history.fragment, {
            quoteid: confirmation.internalid
        });

        Backbone.history.navigate(newHash, {
            trigger: false
        });
    },

    // @method getContext
    // @return {RequestQuoteWizard.Module.Confirmation.Context}
    getContext: function() {
        // @class RequestQuoteWizard.Module.Confirmation.Context
        return {
            // property {Model.Model} model
            model: this.model,
            // @property {String} quoteId
            quoteId: this.model.get('confirmation').internalid,
            // @property {String} quoteTranId
            quoteTranId: this.model.get('confirmation').tranid,
            // @property {String} contactBusinessDaysMessage
            contactBusinessDaysMessage: Configuration.get('quote.contactBusinessDaysMessage', ''),
            // @property {String} disclaimer
            disclaimer: Configuration.get('quote.disclaimer', ''),
            // @property {String} confirmationMessage
            confirmationMessage:
                this.options.message || this.wizard.getCurrentStep().confirmationMessage
        };
        // @class QuoteToSalesOrderWizard.Module.Confirmation
    }
});
