/// <amd-module name="RequestQuoteWizard.Module.Items.Line.Actions.View"/>

import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import requestquote_wizard_module_items_line_actions_tpl = require('../Templates/requestquote_wizard_module_items_line_actions.tpl');

// @class RequestQuoteWizard.Module.Items.Line.Actions.View @extend Backbone.View
export = BackboneView.extend({
    // @property {Function} template
    template: requestquote_wizard_module_items_line_actions_tpl,

    // @property {Object} events
    events: {
        'click [data-action="remove"]': 'removeItem'
    },

    // @method removeItem Removed the current item from the list of items
    // @return {Void}
    removeItem: function() {
        this.options.model.collection.remove(this.options.model);
    },

    // @method getContext
    // @return {RequestQuoteWizard.Module.Items.Line.Actions.View.Context}
    getContext: function() {
        // @class RequestQuoteWizard.Module.Items.Line.Actions.View.Context
        return {};
        // @class RequestQuoteWizard.Module.Items.Line.Actions.View
    }
});
