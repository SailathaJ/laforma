/// <amd-module name="RequestQuoteWizard.Module.Message"/>

import * as _ from 'underscore';

import WizardModule = require('../../Wizard/JavaScript/Wizard.Module');
import requestquote_wizard_module_message_tpl = require('../Templates/requestquote_wizard_module_message.tpl');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// @class WizardModule.Module.Message @extend Wizard.Message
export = WizardModule.extend({
    // @property {Function} template
    template: requestquote_wizard_module_message_tpl,

    // @method getContext
    // @return {RequestQuoteWizard.Module.Message.Context}
    getContext: function() {
        // @class RequestQuoteWizard.Module.Message.Context
        return {
            // @property {String} pageHeader
            pageHeader: Utils.translate('Request a Quote'),
            // @property {String} message
            message: this.options.message || this.wizard.getCurrentStep().bottomMessage
        };
        // @class RequestQuoteWizard.Module.Message
    }
});
