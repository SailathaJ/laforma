/// <amd-module name="QuickOrderAccessPoints.HeaderLink.View"/>
// @module QuickOrderAccessPoints

import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import Configuration = require('../../SCA/JavaScript/SC.Configuration');
import quickorder_accesspoints_headerlink_tpl = require('./../Templates/quickorder_accesspoints_headerlink.tpl');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// @class QuickOrderAccessPoints.HeaderLink.View @extend Backbone.View
export = BackboneView.extend({
    // @property {Function} template
    template: quickorder_accesspoints_headerlink_tpl,

    // @method getContext
    // @return {QuickOrderAccessPoints.HeaderLink.View.Context}
    getContext: function() {
        // @class QuickOrderAccessPoints.HeaderLink.View.Context
        return {
            // @property {String} title
            title: Configuration.get('quickOrder.textHyperlink'),
            // @property {Boolean} showTitle
            showTitle: Configuration.get('quickOrder.showHyperlink'),
            // @property {Boolean} hasClass
            hasClass: !!this.options.className,
            // @property {String} className
            className: this.options.className,
            // @property {String} cartTouchPoint --We must provide a different touchpoint depending on where we are:
            // -if we are in shopping, the data-touchpoint should be 'home',
            // -if we are elsewhere, it should be 'viewcart'.
            // The latter case, when the NavigationHelper manages the navigation, the goToCart.ssp is activated, doing the appropiate redirection.
            cartTouchPoint: Utils.getPathFromObject(
                Configuration,
                'modulesConfig.Cart.startRouter',
                false
            )
                ? Configuration.currentTouchpoint
                : 'viewcart'
        };
        // @class QuickOrderAccessPoints.HeaderLink.View
    }
});
