/// <amd-module name="ItemsSearcher.Utils"/>

import * as _ from 'underscore';

import Configuration = require('../../SCA/JavaScript/SC.Configuration');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// @class ItemsSearcher.Utils
export = {
    // @method formatKeywords format a search query string according to configuration.js (searchPrefs)
    // @param {String} keywords @return {String} the formatted keywords
    // @static
    // @return {String}
    formatKeywords: function(keywords) {
        const keywordFormatter = Utils.getPathFromObject(
            Configuration,
            'searchPrefs.keywordsFormatter'
        );

        if (keywordFormatter && _.isFunction(keywordFormatter)) {
            keywords = keywordFormatter(keywords);
            const maxLength = Utils.getPathFromObject(
                Configuration,
                'searchPrefs.maxLength',
                99999
            );
            if (keywords.length > maxLength) {
                keywords = keywords.substring(0, maxLength);
            }
        }

        return keywords;
    }
};
