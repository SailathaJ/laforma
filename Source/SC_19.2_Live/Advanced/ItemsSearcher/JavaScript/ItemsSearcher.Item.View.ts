/// <amd-module name="ItemsSearcher.Item.View"/>

import * as _ from 'underscore';

import GlobalViewsStarRatingView = require('../../../Commons/GlobalViews/JavaScript/GlobalViews.StarRating.View');
import itemssearcher_item_tpl = require('../Templates/itemssearcher_item.tpl');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import BackboneCompositeView = require('../../../Commons/Backbone.CompositeView/JavaScript/Backbone.CompositeView');

// @class ItemsSearcher.Item.View @extends Backbone.View
export = BackboneView.extend({
    // @property {Function} template
    template: function() {},

    // @property {ItemsSearcher.View.Options} defaultOptions
    // @class ItemsSearcher.View.Options.Item.View.Option
    defaultOptions: {
        // @property {Function} template
        template: itemssearcher_item_tpl
    },
    // @class ItemsSearcher.Item.View

    // @method initialize
    // @param {ItemsSearcher.View.Options.Item.View.Option?} options
    // @return {Void}
    initialize: function(options) {
        this.options = _.defaults(options || {}, this.defaultOptions);
        this.template = this.options.template;
        BackboneCompositeView.add(this);
    },

    // @property {Object} childViews
    childViews: {
        'Global.StarRating': function() {
            return new GlobalViewsStarRatingView({
                model: this.model,
                showRatingCount: false
            });
        }
    },

    // @method getContext
    // @returns {ItemsSearcher.Item.View.Context}
    getContext: function() {
        // @class ItemsSearcher.Item.View.Context
        return {
            // @property {Item.Model} model
            model: this.model,
            // @property {String} currentQuery
            currentQuery: _(this.options.query).escape(),
            // @property {Boolea} isItemSelected
            isItemSelected: !!this.model,
            // @property {Boolean} hasResults
            hasResults: this.options.areResults,
            // @property {Boolean} isAjaxDone
            isAjaxDone: this.options.isAjaxDone
        };
        // @class ItemsSearcher.Item.View
    }
});
