declare var CMS: any;
interface ThemeSettings {
    skins: any;
    userSkins: any;
    formData: any;
    editedSettings: any;
    currentSettings: any;
    skinId?: number;
}

interface Skin {
    id: number;
}

interface CMSPromise {
    resolve(...args):void
    reject(error: {errors: string}):void
}
