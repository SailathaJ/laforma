/// <amd-module name="CMSadapter.Impl.Merchandising"/>

import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');

/*

@module CMSadapter

@class CMSadapter.Impl.Merchandising
*/

function AdapterMerchandising(application, CMS) {
    this.CMS = CMS;
    this.application = application;
    this.listenForCMS();
}

AdapterMerchandising.prototype.listenForCMS = jQuery.noop;

export = AdapterMerchandising;
