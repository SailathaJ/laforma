/// <amd-module name="CMSadapter.Page.Collection"/>

import CMSadapterPageModel = require('./CMSadapter.Page.Model');

import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');

// @module CMSadapter
// @class CMSadapter.Page.Collection @extends Backbone.Collection
export = Backbone.Collection.extend({
    model: CMSadapterPageModel
});
