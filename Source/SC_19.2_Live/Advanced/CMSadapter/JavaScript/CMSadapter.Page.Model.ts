/// <amd-module name="CMSadapter.Page.Model"/>

import BackboneModel = require('../../../Commons/BackboneExtras/JavaScript/Backbone.Model');

// @module CMSadapter

// @class CMSadapter.Page.Model Model for handling CMS landing and enhanced pages @extends Backbone.Model
export = BackboneModel.extend({});
