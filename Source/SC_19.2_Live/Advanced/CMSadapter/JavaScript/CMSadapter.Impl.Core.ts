/// <amd-module name="CMSadapter.Impl.Core"/>

import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');
import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

/*
@module CMSadapter
@class CMSadapter.Impl.Core the class that has the core integration using the CMS API.
*/

const AdapterCore = function(application, CMS) {
    this.CMS = CMS;
    this.application = application;
    this.listenForCMS();
    this.init();
};

AdapterCore.prototype.init = jQuery.noop;

AdapterCore.prototype.listenForCMS = jQuery.noop;

AdapterCore.prototype.getSetupOptions = jQuery.noop;

AdapterCore.prototype.getCmsContext = function getCmsContext() {
    const url = (<any>Backbone.history).fragment.split('?')[0];
    const path = Utils.correctURL(url);

    const context = {
        path: path,
        site_id: this.application.getConfig('siteSettings.siteid'),
        page_type: this.application.getLayout().currentView
            ? this.application.getLayout().currentView.el.id
            : ''
    };

    return context;
};

export = AdapterCore;
