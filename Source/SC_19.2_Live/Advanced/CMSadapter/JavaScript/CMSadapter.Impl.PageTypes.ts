/// <amd-module name="CMSadapter.Impl.PageTypes"/>

interface CmsPageType {
    name: string;
    layout: string;
}

export class CMSadapterPageTypes {
    CMS: any;

    application: any;

    constructor(application, CMS) {
        this.application = application;
        this.CMS = CMS;

        this.listenForCMS();
    }

    listenForCMS() {
        this.CMS.on('pagetype:update', (promise, page: CmsPageType) => {
            const pageTypeComponent = this.application.getComponent('PageType');
            const pageType = pageTypeComponent._getPageType(page.name);
            pageType.set('template', page.layout);
            promise.resolve();
        });
    }
}
