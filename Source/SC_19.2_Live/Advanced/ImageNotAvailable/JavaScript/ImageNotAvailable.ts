/// <amd-module name="ImageNotAvailable"/>

// --------------------
// Simple Module that will make sure that
// if an image files to load it will load an alternative image in it

import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// @class ImageNotAvailable @extends ApplicationModule
export = {
    // @method mountToApp
    // @param {ApplicationSkeleton} application
    // @return {Void}
    mountToApp: function(application) {
        // Every time a new view is rendered
        application.getLayout().on('afterAppendView', function(view) {
            // it will look at the img and bind the error event to it
            view.$('img').on('error', function() {
                // and haven't tried to changed it before, so we don't enter an infinite loop
                if (!this.errorCounter) {
                    const $this = jQuery(this);
                    const src = application.resizeImage(
                        Utils.getThemeAbsoluteUrlOfNonManagedResources(
                            'img/no_image_available.jpeg',
                            application.getConfig('imageNotAvailable')
                        ),
                        Utils.getViewportWidth() < 768 ? 'thumbnail' : 'zoom'
                    );

                    $this
                        // it will set the src of the img to the default image not available. You can set logic based on size or a class for displaying different urls if you need
                        .attr('src', src)

                        // ImageLoader compatibility
                        .attr('data-image-status', 'done');

                    $this.parent('[data-zoom]').length && (<any>$this.parent)('[data-zoom]').zoom();
                    this.errorCounter = true;
                }
            });
        });
    }
};
