/// <amd-module name="CustomerPayment.Model"/>
// @module CustomerPayment

import TransactionModel = require('../../../Commons/Transaction/JavaScript/Transaction.Model');

const CustomerPaymentModel: any = TransactionModel.extend({
    // @property {String} urlRoot
    urlRoot: 'services/CustomerPayment.Service.ss',

    cacheSupport: true
});

export = CustomerPaymentModel;
