/// <amd-module name="OrderWizard.Model"/>

import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');
import Singleton = require('../../../Commons/Main/JavaScript/Singleton');

const OrderWizardModel: any = Backbone.Model.extend({}, Singleton);

export = OrderWizardModel;
