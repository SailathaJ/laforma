/// <amd-module name="Receipt.Details.Item.Summary.View"/>

import * as _ from 'underscore';

import receipt_details_tiem_summary_tpl = require('../Templates/receipt_details_item_summary.tpl');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// @class Receipt.Details.Item.Summary.View @extend Backbone.View
const ReceiptDetailsItemSummaryView: any = BackboneView.extend({
    template: receipt_details_tiem_summary_tpl,

    // @method getContext @return Receipt.Details.Item.Summary.View.Context
    getContext: function() {
        const line = this.model;

        // @class Receipt.Details.Item.Summary.View.Context
        return {
            // @property {Model} line
            line: line,
            // @property {Boolean} isDiscount
            isDiscountType: line.get('type') === 'Discount',
            // @property {Number} quantity
            quantity: line.get('quantity') || 0,
            // @property {Boolean} showAmount
            showAmount: !!line.get('amount_formatted'),
            // @property {String} amountFormatted
            amountFormatted: Utils.formatCurrency(line.get('amount_formatted')),
            // @property {String} totalFormatted
            totalFormatted: Utils.formatCurrency(line.get('total')),
            // @property {Boolean} hasDiscount
            hasDiscount: !!line.get('discount'),
            // @property {Boolean} showAmountLabel
            showAmountLabel: !!line.get('amount_label')
        };
    }
});

export = ReceiptDetailsItemSummaryView;
