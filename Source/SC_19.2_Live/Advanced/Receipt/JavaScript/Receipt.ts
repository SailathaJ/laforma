/// <amd-module name="Receipt"/>

import ReceiptDetailsView = require('./Receipt.Details.View');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// Receipt.js
// -----------------
// Defines the Receipt module (Model, Collection, Views, Router)
const Receipt: any = {
    mountToApp: function(application) {
        const pageType = application.getComponent('PageType');

        pageType.registerPageType({
            name: 'OrderDetail',
            routes: ['receiptshistory/view/:id'],
            view: ReceiptDetailsView,
            defaultTemplate: {
                name: 'receipt_details.tpl',
                displayName: 'Order Detail Default',
                thumbnail: Utils.getThemeAbsoluteUrlOfNonManagedResources('img/default-layout-order-detail.png')
            }
        });
    }
};

export = Receipt;
