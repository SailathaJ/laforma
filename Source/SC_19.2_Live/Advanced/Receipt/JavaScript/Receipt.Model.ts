/// <amd-module name="Receipt.Model"/>

import TransactionModel = require('../../../Commons/Transaction/JavaScript/Transaction.Model');

// @class Receipt.Model Model for showing information about past receipts
const ReceiptModel: any = TransactionModel.extend({
    urlRoot: 'services/Receipt.Service.ss',
    // @property {Boolean} cacheSupport enable or disable the support for cache (Backbone.CachedModel)
    cacheSupport: true
});

export = ReceiptModel;
