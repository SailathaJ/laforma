/// <amd-module name="SC.Shopping.Layout"/>
/// <reference path="../../../Commons/Utilities/JavaScript/UnderscoreExtended.d.ts" />
import '../../../Commons/Utilities/JavaScript/Utils';

import shopping_layout_tpl = require('../Templates/shopping_layout.tpl');
import ApplicationSkeletonLayout = require('../../../Commons/ApplicationSkeleton/JavaScript/ApplicationSkeleton.Layout');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// @class SCA.Shopping.Layout @extends ApplicationSkeleton.Layout
const ShoppingLayout: any = ApplicationSkeletonLayout.extend({
    // @property {Function} template
    template: shopping_layout_tpl,
    // @property {String} className
    className: 'layout-container',

    // @property {Array} breadcrumbPrefix
    breadcrumbPrefix: [
        {
            href: '/',
            'data-touchpoint': 'home',
            'data-hashtag': '#',
            text: Utils.translate('Home')
        }
    ]
});

export = ShoppingLayout;
