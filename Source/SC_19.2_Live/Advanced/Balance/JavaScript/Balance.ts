/// <amd-module name="Balance"/>
// @module Balance

import BalanceView = require('./Balance.View');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

const Balance: any = {
    mountToApp: function(application) {
        const pageType = application.getComponent('PageType');
        pageType.registerPageType({
            name: 'AccountBalance',
            routes: ['balance', 'balance?*params'],
            view: BalanceView,
            defaultTemplate: {
                name: 'balance.tpl',
                displayName: 'Account Balance Default',
                thumbnail: Utils.getThemeAbsoluteUrlOfNonManagedResources('img/default-layout-account-balance.png')
            }
        });
    }
};
export = Balance;
