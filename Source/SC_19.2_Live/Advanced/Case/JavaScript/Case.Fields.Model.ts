///<amd-module name="Case.Fields.Model"/>

import Utils = require('../../../Commons/Utilities/JavaScript/Utils');
import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');

// @class Case.Fields.Model @extends Backbone.Model
const CaseFieldsModel: any = Backbone.Model.extend({
	//@property {String} urlRoot
    urlRoot: Utils.getAbsoluteUrl('SC/Case/Case.Fields.ss', true)
});

export = CaseFieldsModel;
