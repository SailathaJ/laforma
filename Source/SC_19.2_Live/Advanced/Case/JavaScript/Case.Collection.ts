/// <amd-module name="Case.Collection"/>

import Model = require('./Case.Model');

import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// @class Case.Collection @extends Backbone.Collection
const CaseCollection: any = Backbone.Collection.extend({
    url: Utils.getAbsoluteUrl('SC/Case/Case.ss', true),

    model: Model,

    initialize: function(): void {
        // The first time the collection is filled with data
        // we store a copy of the original collection
        this.once('sync reset', function() {
            if (!this.original) {
                this.original = this.clone();
            }
        });
    },

    parse: function(response) {
        this.totalRecordsFound = response.totalRecordsFound;
        this.recordsPerPage = response.recordsPerPage;

        return response.records;
    },

    update: function(options): void {
        const filter = options.filter || {};

        this.fetch({
            data: {
                filter: filter.value,
                sort: options.sort.value,
                order: options.order,
                page: options.page
            },
            reset: true,
            killerId: options.killerId
        });
    }
});

export = CaseCollection;
