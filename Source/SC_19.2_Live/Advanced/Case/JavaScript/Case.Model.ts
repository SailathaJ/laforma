/// <amd-module name="Case.Model"/>

import Utils = require('../../../Commons/Utilities/JavaScript/Utils');
import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');

// @class Case.Model Model for handling Support Cases (CRUD) @extends Backbone.Model

// @method validateLength. Validates message length. (0 < length <= 4000)
function validateLength(value, name: string): string {
    const max_length = 4000;

    if (value && value.length > max_length) {
        return Utils.translate('$(0) must be at most $(1) characters', name, max_length);
    }
}

// @method validateMessage. Validates message entered by the user. Checks length 0 < length <= 4000.
function validateMessage(value, name: string): string {
    /* jshint validthis:true */
    if (this.get('isNewCase')) {
        if (!value) {
            return Utils.translate('$(0) is required', name);
        }

        return validateLength(value, name);
    }
}

function validateReply(value, name: string) {
    /* jshint validthis:true */
    if (!this.get('isNewCase') && !value && !this.get('isClosing')) {
        return Utils.translate('$(0) is required', name);
    }
}

const CaseModel: any = Backbone.Model.extend({
    urlRoot: Utils.getAbsoluteUrl('SC/Case/Case.ss', true),

    validation: {
        title: {
            required: true,
            msg: Utils.translate('Subject is required')
        },

        message: {
            fn: validateMessage
        },

        reply: {
            fn: validateReply
        },

        email: {
            required: function(value, name: string, form) {
                return !!form.include_email;
            },
            pattern: 'email',
            msg: Utils.translate('Please provide a valid email')
        }
    }
});

export = CaseModel;
