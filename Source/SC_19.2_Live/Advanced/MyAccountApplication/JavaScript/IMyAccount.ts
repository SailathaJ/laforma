/// <amd-module name="IMyAccount"/>

interface IMyAccount {
    modulesToLoad(entryPointModules: any);
    clearLayout(layout: any);
    removeViews();
    replaceView();
}

export = IMyAccount;
