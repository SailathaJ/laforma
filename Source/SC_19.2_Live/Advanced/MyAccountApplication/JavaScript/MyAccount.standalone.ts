/// <amd-module name="MyAccount.standalone"/>

import MyAccountFull = require('./MyAccount.Full');
import IMyAccount = require('./IMyAccount');

import * as Layout from './SC.MyAccount.Layout';
import * as _ from 'underscore';

class MyAccountStandalone extends MyAccountFull implements IMyAccount {
    public isStandalone: boolean;
    static readonly _internalName: string = 'MyAccount.StandAlone';
    public static getName(): string {
        return MyAccountStandalone._internalName;
    }

    constructor(appName: string) {
        super(appName);
        this.Layout = Layout;
        this.isStandalone = true;
        this.on('afterModulesLoaded', this.onAfterModulesLoadedHandler);
    }

    onAfterModulesLoadedHandler = (): void => {
        this.off('afterModulesLoaded');
    };

    modulesToLoad = (entryPointModules: any) =>
        _.filter(entryPointModules, (module: any) => module && !module.excludeFromMyAccount);

    clearLayout = layout => {
        const applicationLayout = layout.application._layoutInstance;
        applicationLayout.afterAppendViewPromise.then(function() {
            //_.each(VIEW_TO_REMOVE_LIST, function(childViewName) {
            // SC._applications.MyAccount.getLayout().currentView
            // remove chhild views from VIEW_TO_REMOVE_LIST
            //});
        });
    };
}

export = MyAccountStandalone;
