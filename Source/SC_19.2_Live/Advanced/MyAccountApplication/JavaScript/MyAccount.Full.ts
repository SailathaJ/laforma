/// <amd-module name="MyAccount.Full"/>

import '../../../Commons/Main/JavaScript/Application';
import '../../../Commons/Utilities/JavaScript/backbone.custom';
import '../../../Commons/BackboneExtras/JavaScript/Backbone.Model';
import '../../../Commons/BackboneExtras/JavaScript/Backbone.Sync';
import '../../../Commons/BackboneExtras/JavaScript/Backbone.Validation.callbacks';
import '../../../Commons/BackboneExtras/JavaScript/Backbone.Validation.patterns';
import '../../../Commons/BackboneExtras/JavaScript/Backbone.View';
import '../../../Commons/BackboneExtras/JavaScript/Backbone.View.render';
import '../../../Commons/BackboneExtras/JavaScript/Backbone.View.saveForm';
import '../../../Commons/BackboneExtras/JavaScript/Backbone.View.toggleReset';
import '../../../Commons/BootstrapExtras/JavaScript/Bootstrap.Rate';
import '../../../Commons/BootstrapExtras/JavaScript/Bootstrap.Slider';
import '../../../Commons/jQueryExtras/JavaScript/jQuery.ajaxSetup';
import '../../../Commons/jQueryExtras/JavaScript/jQuery.serializeObject';
import '../../../Commons/NativesExtras/JavaScript/String.format';

import IMyAccount = require('./IMyAccount');

import MyAccountConfiguration = require('../../MyAccountApplication.SCA/JavaScript/MyAccount.Configuration');

import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');
import * as Layout from './SC.MyAccount.Layout';

class MyAccountFull extends SC.Application implements IMyAccount {

    static readonly _internalName: string = 'MyAccount.Full';
    public static getName():string {
        return MyAccountFull._internalName;
    }

    public Configuration;
    public Layout;
    constructor(appName: string) {
        super(appName);
        this.Configuration = MyAccountConfiguration.getInstance().getConfiguration();
        this.Layout = Layout;
        jQuery.ajaxSetup( { cache:false } );
        this.on('afterModulesLoaded', this.onAfterModulesLoadedHandler);
    }

    onAfterModulesLoadedHandler = (): void => {
        this.off('afterModulesLoaded');
    };

    modulesToLoad = (entryPointModules: any) => entryPointModules;

    clearLayout = (layout: any) => {};

    removeViews = () => {};

    replaceView = () => {};

}

export = MyAccountFull;