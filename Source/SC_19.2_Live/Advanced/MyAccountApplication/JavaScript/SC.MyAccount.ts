/// <amd-module name="SC.MyAccount"/>

import IMyAccount = require('./IMyAccount');
import MyAccountStandAlone = require('./MyAccount.StandAlone');
import MyAccountFull = require('./MyAccount.Full');

class SCMyAccount {
  static _instance:SCMyAccount;
  static getInstance(): IMyAccount {
    if (SC.ENVIRONMENT.standalone) {
      return new MyAccountStandAlone('MyAccount.StandAlone');
    }
    return new MyAccountFull('MyAccount.Full');
  }
}

export = SCMyAccount;
