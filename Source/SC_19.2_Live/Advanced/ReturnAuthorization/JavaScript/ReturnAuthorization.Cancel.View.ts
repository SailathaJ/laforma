/// <amd-module name="ReturnAuthorization.Cancel.View"/>
// @module ReturnAuthorization

import return_authorization_cancel_tpl = require('../Templates/return_authorization_cancel.tpl');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// @class ReturnAuthorization.Cancel.View @extend Backbone.View
const ReturnAuthorizationCancelView: any = BackboneView.extend({
    template: return_authorization_cancel_tpl,

    title: Utils.translate('Cancel Return Request'),

    page_header: Utils.translate('Cancel Return Request'),

    events: {
        'click [data-action="delete"]': 'confirm'
    },

    initialize: function(options) {
        this.application = options.application;
    },

    confirm: function() {
        this.model
            .save({
                status: 'cancelled'
            })
            .then(jQuery.proxy(this, 'dismiss'));
    },

    dismiss: function() {
        this.$containerModal &&
            this.$containerModal
                .removeClass('fade')
                .modal('hide')
                .data('bs.modal', null);
    }
});

export = ReturnAuthorizationCancelView;
