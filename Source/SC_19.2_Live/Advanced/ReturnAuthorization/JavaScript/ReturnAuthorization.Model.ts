/// <amd-module name="ReturnAuthorization.Model"/>
// @module ReturnAuthorization.Model

import TransactionCollection = require('../../../Commons/Transaction/JavaScript/Transaction.Collection');
import TransactionModel = require('../../../Commons/Transaction/JavaScript/Transaction.Model');

// @class ReturnAuthorization.Model @extend Backbone.Model
const ReturnAuthorizationModel: any = TransactionModel.extend({
    // @property {String} urlRoot
    urlRoot: 'services/ReturnAuthorization.Service.ss',

    // @property {Boolean} cacheSupport enable or disable the support for cache (Backbone.CachedModel)
    cacheSupport: true,
    // @method initialize
    // @param {Object} attributes
    initialize: function(attributes) {
        // call the initialize of the parent object, equivalent to super()
        TransactionModel.prototype.initialize.apply(this, arguments);

        this.on('change:applies', function(model, applies) {
            model.set('applies', new TransactionCollection(applies), { silent: true });
        });

        this.trigger('change:applies', this, (attributes && attributes.lines) || []);
    }
});

export = ReturnAuthorizationModel;
