/// <amd-module name="ReturnAuthorization.Collection"/>
// @module ReturnAuthorization.Collection

import Model = require('./ReturnAuthorization.Model');
import TransactionCollection = require('../../../Commons/Transaction/JavaScript/Transaction.Collection');

// @class ReturnAuthorization.Collection @extend Transaction.Collection
const ReturnAuthorizationCollection: any = TransactionCollection.extend({
    model: Model,
    // @property {Boolean} cacheSupport enable or disable the support for cache (Backbone.CachedModel)
    cacheSupport: true,

    url: 'services/ReturnAuthorization.Service.ss'
});

export = ReturnAuthorizationCollection;
