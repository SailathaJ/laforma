/// <amd-module name="ReturnAuthorization.Form.Item.Summary.View"/>
// @module ReturnAuthorization.Form.Item.Summary.View

import return_authorization_form_item_summary_tpl = require('../Templates/return_authorization_form_item_summary.tpl');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');

// @class ReturnAuthorization.Form.Item.Summary.View @extend Backbone.View
const ReturnAuthorizationFormItemSummaryView: any = BackboneView.extend({
    template: return_authorization_form_item_summary_tpl,

    // @method getContext @return ReturnAuthorization.Form.Item.Summary.View.Context
    getContext: function() {
        // @class ReturnAuthorization.Form.Item.Summary.View.Context
        return {
            // @property {Model} line
            line: this.model,
            // @property {Boolean} isLineActive
            isLineActive: this.model.get('checked'),
            // @property {Number} selectedQuantity
            selectedQuantity: this.model.get('returnQty') || this.model.get('quantity'),
            // @property {Number} selectedQuantity
            maxQuantity: this.model.get('quantity'),
            // @property {Boolean} isQuantityGreaterThan1
            isQuantityGreaterThan1: this.model.get('quantity') > 1
        };
    }
});

export = ReturnAuthorizationFormItemSummaryView;
