/// <amd-module name="ReferenceMap.Configuration"/>

// @module StoreLocatorReferenceMapsImplementation

const configuration = {
    // @method mapOptions
    // @returns {Object} with default center location, zoom, mapType and mapTypeControl
    mapOptions: function() {},
    // @method mapOptions
    // @returns {Object} with Store icon, position icon and autocomplete input
    iconOptions: function() {},
    // @method zoomInDetails
    // @return {Number} zoom to be applied in Store.Locator.Details.View
    zoomInDetails: function() {},
    // @method title
    // @retrun {String} to be applied in Main and Header
    title: function() {},
    // @method isEnabled
    // @return {Boolean} define if application is enabled or disabled
    isEnabled: <any>function() {},
    // @method getUrl
    // @return {String} url to be loaded
    getUrl: function() {},
    // @method getApiKey
    // @return {String} map provider api key
    getApiKey: function() {},
    // @method getExtraData
    // @return {Array} with {Object} _internalid, name, servicehours
    getExtraData: function() {},
    // @method getRadius
    // @return {Number} radius used in search
    getRadius: function() {},
    // @method openPopupOnMouseOver define if popup appears when scroll on the list
    // @return {Boolean}
    openPopupOnMouseOver: function() {},
    // @method showLocalizationMap
    // @return {Boolean} map appears when search in mobile
    showLocalizationMap: function() {},
    // @method showAllStoresRecordsPerPage
    // @return {Number} total of records per page in StoreLocator.List.View.All
    showAllStoresRecordsPerPage: function() {}
};

export = configuration;
