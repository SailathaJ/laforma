/// <amd-module name="QuickOrder.EmptyCart.View"/>

import quick_order_empty_cart_tpl = require('../Templates/quick_order_empty_cart.tpl');

import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');

// @class QuickOrder.EmptyCart.View @extend Backbone.View
const QuickOrderEmptyCartView: any = BackboneView.extend({
    // @property {Function} template
    template: quick_order_empty_cart_tpl
});

export = QuickOrderEmptyCartView;
