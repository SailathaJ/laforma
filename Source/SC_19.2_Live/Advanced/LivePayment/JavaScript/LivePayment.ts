/// <amd-module name="LivePayment"/>

import LivePaymentModel = require('./LivePayment.Model');

const LivePayment: any = {
    mountToApp: function() {
        if (SC.ENVIRONMENT.LIVEPAYMENT) {
            LivePaymentModel.getInstance().set(SC.ENVIRONMENT.LIVEPAYMENT);
        }
    }
};

export = LivePayment;
