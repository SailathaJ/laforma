/// <amd-module name="GoogleTagManager.Model"/>

import BackboneModel = require('../../../Commons/BackboneExtras/JavaScript/Backbone.Model');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');
import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');

export = BackboneModel.extend({
    urlRoot: Utils.getAbsoluteUrl('services/GoogleTagManager.Service.ss'),

    getDataLayer: function(data) {
        const deferred = jQuery.Deferred();

        const self = this;

        self.set('id', data.id);
        self.set('events', data.events);
        this.save().done(function(result) {
            self.set('events', result.events);
            self.set('internalid', result.internalid);
            deferred.resolve();
        });

        return deferred.promise();
    },
    saveEvent: function(data) {
        const deferred = jQuery.Deferred();
        const self = this;

        this.set('events', data);
        self.save().done(function() {
            deferred.resolve();
        });

        return deferred.promise();
    }
});
