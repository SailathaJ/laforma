/// <amd-module name="SC.Checkout.Layout"/>

import * as _ from 'underscore';

import ConfigurationPromise = require('./SC.Checkout.Configuration');
import ApplicationSkeletonLayout = require('../../../Commons/ApplicationSkeleton/JavaScript/ApplicationSkeleton.Layout');
import checkout_layout_tpl = require('../Templates/checkout_layout.tpl');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// @class SCA.Checkout.Layout @extend ApplicationSkeletonLayout
const SCCheckoutLayout: any = ApplicationSkeletonLayout.extend({
    // @propery {Function} template
    template: checkout_layout_tpl,

    // @propery {String} className
    className: 'layout-container',
    // @property {Array} breadcrumbPrefix
    breadcrumbPrefix: [
        {
            href: '#',
            'data-touchpoint': 'home',
            'data-hashtag': '#',
            text: Utils.translate('Home')
        },
        {
            href: '#',
            'data-touchpoint': 'checkout',
            'data-hashtag': '#',
            text: Utils.translate('Checkout')
        }
    ]
});

export = SCCheckoutLayout;
