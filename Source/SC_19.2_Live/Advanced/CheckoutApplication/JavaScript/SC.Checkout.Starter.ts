/// <amd-module name="SC.Checkout.Starter"/>

import applicationPromise = require('./SC.Checkout');
import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');
import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');
import entryPointModule = require('./SC.Checkout.Starter.Dependencies'); // Auto generated at build time using configuration from distro.json
import extensionsPromise = require('../../../Commons/SC.Extensions/JavaScript/SC.Extensions');

jQuery(function() {
    applicationPromise.then(function(application) {
        extensionsPromise.then(function(...entryPointExtensionsModules) {
            // At starting time all the modules Array is initialized
            const entryPointModules = entryPointModule.concat(entryPointExtensionsModules);

            application.getConfig().siteSettings = SC.ENVIRONMENT.siteSettings || {};

            if (SC.ENVIRONMENT.CHECKOUT.skipLogin) {
                application.Configuration.checkout = application.Configuration.checkout || {};
                application.Configuration.checkout.skipLogin = SC.ENVIRONMENT.CHECKOUT.skipLogin;
                delete SC.ENVIRONMENT.CHECKOUT.skipLogin;
            }

            application.start(entryPointModules, function() {
                // Checks for errors in the context
                if (SC.ENVIRONMENT.contextError) {
                    // Shows the error.
                    if (SC.ENVIRONMENT.contextError.errorCode === 'ERR_WS_EXPIRED_LINK') {
                        application
                            .getLayout()
                            .expiredLink(SC.ENVIRONMENT.contextError.errorMessage);
                    } else {
                        application
                            .getLayout()
                            .internalError(
                                SC.ENVIRONMENT.contextError.errorMessage,
                                'Error ' +
                                    SC.ENVIRONMENT.contextError.errorStatusCode +
                                    ': ' +
                                    SC.ENVIRONMENT.contextError.errorCode
                            );
                    }
                } else {
                    const { fragment } = Utils.parseUrlOptions(location.search);

                    if (fragment && !location.hash) {
                        location.hash = decodeURIComponent(fragment);
                    }

                    Backbone.history.start();
                }

                application.getLayout().appendToDom();
            });
        });
    });
});
