/// <amd-module name="PrintStatement"/>

import PrintStatementView = require('./PrintStatement.View');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// @class PrintStatement
const PrintStatement: any = {
    // @method mountToApp
    mountToApp: function(application) {
        const pageType = application.getComponent('PageType');
        pageType.registerPageType({
            name: 'PrintStatement',
            routes: ['printstatement', 'printstatement?*params'],
            view: PrintStatementView,
            defaultTemplate: {
                name: 'print_statement.tpl',
                displayName: 'PrintStatement Default',
                thumbnail: Utils.getThemeAbsoluteUrlOfNonManagedResources('img/default-layout-printStatement.png')
            }
        });
    }
};

export = PrintStatement;
