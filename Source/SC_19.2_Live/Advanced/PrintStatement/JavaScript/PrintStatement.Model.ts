/// <amd-module name="PrintStatement.Model"/>

import * as _ from 'underscore';
import '../../../Commons/BackboneExtras/JavaScript/Backbone.Sync';

import BackboneModel = require('../../../Commons/BackboneExtras/JavaScript/Backbone.Model');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

function parseDate(value) {
    if (value && value.replace) {
        return new Date(value.replace(/-/g, '/')).getTime();
    }
    return new Date(value).getTime();
}

function validateStatementDate(value) {
    if (!value || isNaN(parseDate(value))) {
        return Utils.translate('Invalid Statement date');
    }
}

function validateStartDate(value) {
    if (value) {
        if (isNaN(parseDate(value))) {
            return Utils.translate('Invalid Start date');
        }
    }
}

// @class PrintStatement.Model @extends Backbone.Model
const PrintStatementModel: any = BackboneModel.extend({
    validation: {
        statementDate: { fn: validateStatementDate },

        startDate: { fn: validateStartDate }
    },

    // @property {String} urlRoot
    urlRoot: 'services/PrintStatement.Service.ss',

    // @method parse
    parse: function(result) {
        return result;
    }
});

export = PrintStatementModel;
