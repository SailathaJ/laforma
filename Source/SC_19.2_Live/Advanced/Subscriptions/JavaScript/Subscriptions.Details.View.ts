/// <amd-module name="Subscriptions.Details.View"/>

import * as _ from 'underscore';

import Configuration = require('../../SCA/JavaScript/SC.Configuration');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import BackboneFormView = require('../../../Commons/Backbone.FormView/JavaScript/Backbone.FormView');
import subscriptions_details_tpl = require('../Templates/subscriptions_details.tpl');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');
import SubscriptionsModel = require('./Subscriptions.Model');
import SubscriptionLinesView = require('./Subscriptions.Line.View');
import SubscriptionAddonListView = require('./Subscriptions.AddOn.List.View');
import AjaxRequestsKiller = require('../../../Commons/AjaxRequestsKiller/JavaScript/AjaxRequestsKiller');
import GlobalViewsConfirmationView = require('../../../Commons/GlobalViews/JavaScript/GlobalViews.Confirmation.View');
import BackboneCollectionView = require('../../../Commons/Backbone.CollectionView/JavaScript/Backbone.CollectionView');
import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');

const SubscriptionsDetailsView: any = BackboneView.extend({
    template: subscriptions_details_tpl,

    title: Utils.translate('My Subscriptions'),

    page_header: Utils.translate('My Subscriptions'),

    events: {
        'click [data-action="goToAddOnsMarket"]': 'goToAddOnsMarket',
        'click [data-action="cancel-subscription"]': 'cancel',
        'click [data-action="reactivate-subscription"]': 'reactivate'
    },

    attributes: {
        id: 'Subscriptions',
        class: 'Subscriptions'
    },

    initialize: function(options) {
        this.application = options.application;
        this.options = options;
    },

    beforeShowContent: function beforeShowContent() {
        this.model = new SubscriptionsModel({ internalid: this.options.routerArguments[0] });
        return this.model.fetch({
            killerId: AjaxRequestsKiller.getKillerId()
        });
    },
    // @method getSelectedMenu @return {String}
    getSelectedMenu: function() {
        return 'subscriptions';
    },
    // @method getBreadcrumbPages
    getBreadcrumbPages: function() {
        return {
            text: this.title,
            href: '/Subscriptions'
        };
    },

    goToAddOnsMarket: function() {
        const view = new SubscriptionAddonListView(
            _.extend(this.options, {
                subscription: this.model
            })
        );

        view.showContent();
    },
    // @property {Object} childViews
    childViews: {
        'Optional.Lines.Collection': function() {
            return new BackboneCollectionView({
                childView: SubscriptionLinesView,
                viewsPerRow: 1,
                collection: this.model.get('optionalSubscriptionLines'),
                childViewOptions: _.extend(this.options,{
                    subscription: this.model
                })
            });
        },
        'Required.Lines.Collection': function() {
            return new BackboneCollectionView({
                childView: SubscriptionLinesView,
                viewsPerRow: 1,
                collection: this.model.get('requiredSubscriptionLines'),
                childViewOptions: _.extend(this.options,{
                    subscription: this.model
                })
            });
        }
    },
    cancel: function(e) {
        e.preventDefault();
        const deleteConfirmationView = new GlobalViewsConfirmationView({
            callBack: this._cancelSubscription,
            callBackParameters: {
                context: this
            },
            title: Utils.translate('Cancel Subscription'),
            body: Utils.translate('Please, confirm you want to cancel this subscription'),
            autohide: true
        });
        return this.application.getLayout().showInModal(deleteConfirmationView);
    },
    _cancelSubscription: function(options) {
        options.context.model.destroy().done(function() {
            Backbone.history.navigate('subscription/'+options.context.model.get('internalid'), true );            
        });
    },
    reactivate: function(e) {
        e.preventDefault();
        const deleteConfirmationView = new GlobalViewsConfirmationView({
            callBack: this._reactivateSubscription,
            callBackParameters: {
                context: this
            },
            title: Utils.translate('Reactivate Subscription'),
            body: Utils.translate('Please, confirm you want to reactivate this subscription'),
            autohide: true
        });
        return this.application.getLayout().showInModal(deleteConfirmationView);
    },
    _reactivateSubscription: function(options) {
        options.context.model.save().done(function() {
            Backbone.history.navigate('subscription/'+options.context.model.get('internalid'), true );            
        });
    },

    // @method getContext @return {Subscriptions.Details.View.Context}
    getContext: function() {
        // @class Subscriptions.List.View.Context
        const subscriptionplan_name = this.model.get('name') || '';
        const has_subscription_status = !_.isUndefined(this.model.get('status'));
        const subscription_status_lower_case = has_subscription_status ? this.model.get('status').toLowerCase() : '';
        const end_date = this.model.get('endDate') || Utils.translate('N/A');

        return {
            // @property {String} name
            name: subscriptionplan_name,
            // @property {Boolean} hasSubscriptionStatus
            hasSubscriptionStatus: has_subscription_status,
            // @property {String} status
            status: this.model.getStatusLabel(),
            // @property {String} statusLowerCase
            statusLowerCase: subscription_status_lower_case,
            // @property {Boolean} isOptionalLinesCountGreaterThan0
            isOptionalLinesCountGreaterThan0: this.model.get('optionalSubscriptionLines').length > 0,
            // @property {Boolean} isRequiredLinesCountGreaterThan0
            isRequiredLinesCountGreaterThan0: this.model.get('requiredSubscriptionLines').length > 0,
            // @property {Boolean} isNonIncludedLinesCountGreaterThan0
            isNonIncludedLinesCountGreaterThan0: this.model.get('nonIncludedLinesCollection').length > 0,
            // @property {String} internalId
            internalId: this.model.get('internalid'),
            // @property {String} nextBillCycleDate
            nextBillCycleDate: this.model.get('nextBillCycleDate') || '-',
            // @property {String} lastBillDate
            lastBillDate: this.model.get('lastBillDate') || '-',
            // @property {String} endDate
            endDate: end_date,
            // @property {Boolean} canBeSuspended
            canBeSuspended: this.model.get('canBeSuspended'),
            // @property {Boolean} canBeReactivated
            canBeReactivated: this.model.get('canBeReactivated')
        };
    }
});

export = SubscriptionsDetailsView;
