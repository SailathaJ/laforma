/// <amd-module name="Subscriptions.List.View"/>
// @module Subscriptions

import * as _ from 'underscore';

import SubscriptionsCollection = require('./Subscriptions.Collection');
import ListHeaderView = require('../../../Commons/ListHeader/JavaScript/ListHeader.View');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');
import BackboneCollectionView = require('../../../Commons/Backbone.CollectionView/JavaScript/Backbone.CollectionView');
import Configuration = require('../../SCA/JavaScript/SC.Configuration');
import RecordViewsView = require('../../RecordViews/JavaScript/RecordViews.View');

import GlobalViewsPaginationView = require('../../../Commons/GlobalViews/JavaScript/GlobalViews.Pagination.View');
import subscriptions_list_tpl = require('../Templates/subscriptions_list.tpl');

import Handlebars = require('../../../Commons/Utilities/JavaScript/Handlebars');
import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');

import ProfileModel = require('../../../Commons/Profile/JavaScript/Profile.Model');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

const SubscriptionsListView = BackboneView.extend({
    template: subscriptions_list_tpl,

    title: Utils.translate('My Subscriptions'),

    page_header: Utils.translate('My Subscriptions'),

    attributes: {
        id: 'Subscriptions',
        class: 'Subscriptions'
    },

    initialize: function(options) {
        this.application = options.application;
        this.collection = new SubscriptionsCollection();
        this.profileModel = ProfileModel.getInstance();

        this.listenCollection();
        this.collection.on('reset', this.showContent, this);

        var page = 1;

        if (options.routerArguments && options.routerArguments[0])
        {
            var params = Utils.parseUrlOptions(options.routerArguments[0]);

            if (params.page)
            {
                page = params.page;
            }
        }
        this.options.page = page;

        this.listHeader = new ListHeaderView({
            view: this,
            application: this.application,
            collection: this.collection,
            filters: this.filterOptions,
            sorts: this.sortOptions,
            rangeFilter: 'date',
            rangeFilterLabel: Utils.translate('From'),
            hidePagination: false,
            allowEmptyBoundaries: true
        });
    },

    beforeShowContent: function beforeShowContent() {
        const promise = jQuery.Deferred();

        this.collection
            .fetch({
                data: {
                    page: this.options.page,
                    order: 1,
                    sort: 'trandate,internalid',
                    results_per_page: 20,
                },
                error: function(model, jqXhr) {
                    // this will stop the ErrorManagment module to process this error
                    // as we are taking care of it
                    jqXhr.preventDefault = true;
                }
            })
            .always(function() {
                promise.resolve();
            });

        return promise;
    },

    // @method listenCollection
    listenCollection: function() {
        this.setLoading(true);
        this.collection.on({
            request: jQuery.proxy(this, 'setLoading', true),
            reset: jQuery.proxy(this, 'setLoading', false)
        });
        this.collection.on('reset', this.render, this);
    },

    // @method setLoading @param {Boolean} bool
    setLoading: function(bool) {
        this.isLoading = bool;
    },

    // @method getSelectedMenu @return {String}
    getSelectedMenu: function() {
        return 'subscriptions';
    },
    // @method getBreadcrumbPages
    getBreadcrumbPages: function() {
        return {
            text: this.title,
            href: '/Subscriptions'
        };
    },

    childViews: {
        'ListHeader.View': function() {
            return this.listHeader;
        },
        'Records.Collection': function() {


            const records_collection = new Backbone.Collection(
                this.collection.map(function(subscription) {
                    const model = new Backbone.Model({
                        touchpoint: 'customercenter',
                        title: new Handlebars.SafeString(
                            Utils.translate(
                                '<span class="subscriptionid">$(0)</span>',
                                subscription.get('name')
                            )
                        ),
                        detailsURL: subscription.getTypeUrl(),

                        columns: [
                            {
                                label: Utils.translate('Last Bill:'),
                                type: 'date',
                                name: 'lastbill',
                                value: !subscription.get('lastBillDate') ? '-' : subscription.get('lastBillDate')
                            },
                            {
                                label: Utils.translate('Next Bill:'),
                                type: 'date',
                                name: 'nextbill',
                                value: subscription.get('status') === 'ACTIVE' && subscription.get('nextBillCycleDate') ? subscription.get('nextBillCycleDate') : '-'
                            },
                            {
                                label: Utils.translate('Activation Date:'),
                                type: 'date',
                                name: 'datecreated',
                                value: subscription.get('startDate')
                            },
                            {
                                label: Utils.translate('Status'),
                                type: 'status',
                                name: 'status',
                                value: Utils.translate(subscription.getStatusLabel())
                            }
                        ]
                    });

                    model.id = subscription.get('internalId');

                    return model;
                })
            );

            return new BackboneCollectionView({
                childView: RecordViewsView,
                collection: records_collection,
                viewsPerRow: 1
            });
        },
        'GlobalViews.Pagination': function() {
            return new GlobalViewsPaginationView(
                _.extend(
                    {
                        totalPages: Math.ceil(
                            this.collection.totalRecordsFound / this.collection.recordsPerPage
                        )
                    },
                    Configuration.defaultPaginationSettings
                )
            );
        }
    },

    // @method getContext @return {Subscriptions.List.View.Context}
    getContext: function() {
        // @class Subscriptions.List.View.Context
        return {
            // @property {String} pageHeader
            pageHeader: this.page_header,
            // @property {Boolean} showNoTermMessage
            hasTerms: !!this.profileModel.get('paymentterms'),
            // @property {Boolean} isThereAnyResult
            isThereAnyResult: !!this.collection.length,
            // @property {Boolean} isLoading
            isLoading: this.isLoading,
            // @property {Boolean} showPagination
            showPagination: !!(this.collection.totalRecordsFound && this.collection.recordsPerPage),
            // @property {Boolean} showCurrentPage
            showCurrentPage: this.options.showCurrentPage,
            // @property {Boolean} showBackToAccount
            showBackToAccount: Configuration.get('siteSettings.sitetype') === 'STANDARD'
        };
    }
});

export = SubscriptionsListView;
