/// <amd-module name="Subscriptions.Line.Collection"/>
import Model = require('./Subscriptions.Line.Model');
import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');

const SubscriptionsLineCollection: any = Backbone.Collection.extend({
    model: Model
});

export = SubscriptionsLineCollection;
