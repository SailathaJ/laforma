/// <amd-module name="Subscriptions.Line.Model"/>
/// <reference path="../../../Commons/Utilities/JavaScript/GlobalDeclarations.d.ts" />

import * as _ from 'underscore';

import Utils = require('../../../Commons/Utilities/JavaScript/Utils');
import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');
import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');

const SubscriptionsLineModel: any = Backbone.Model.extend({

    initialize: function() {
        const item = this.get('item');
        this.set('item', new Backbone.Model(item));

        this.setPeriodicity();

        this.setPricePlan();

        this.setLinePrices();

        this.setLineCharge();

        this.on('change:quantity', this.setRecurringAmount, this);

        this.setRecurringAmount();
    },

    getStatusLabel: function(){
        if(this.get('isProcessing') || this.get('status') === 'PENDING_ACTIVATION')
        {
            return 'PROCESSING';
        }
        else
        {
            return this.get('status');
        }
    },

    setPeriodicity: function () {
        if(!this.get('priceIntervals').length) return;
        const price_intervals = this.get('priceIntervals');
        const frequency = price_intervals[0].frequency;
        switch (frequency) {
            case 'ANNUALLY':
                this.set('frequency', 'Year');
                break;
            case 'WEEKLY':
                this.set('frequency', 'Week');
                break;
            case 'MONTHLY':
                this.set('frequency', 'Month');
                break;
            default:
        }
    },
    setLineCharge: function () {
        const line_type = this.get('subscriptionLineType');

        switch (line_type.toString()) {
            case '1':
                this.set('chargeType', 'One Time');
                break;
            case '2':
                this.set('chargeType', 'Recurring');
                break;
            case '3':
                this.set('chargeType', 'Usage');
                break;
            default:
                this.set('chargeType', line_type);
        }
    },
    setPricePlan: function () {
        if(!this.get('priceIntervals').length) return;
        const price_plan = this.get('priceIntervals')[0].pricePlan;

        this.set('recurringAmount', this.get('priceIntervals')[0].recurringAmount)
        this.set('recurringAmount_formatted', Utils.formatCurrency(this.get('priceIntervals')[0].recurringAmount));
        this.set('pricePlanId', price_plan.pricePlanId)

        switch(price_plan.pricePlanType.toString()) {
            case '2':
                this.set('pricePlanType', Utils.translate('Tiered'));
                break;
            case '4':
                this.set('pricePlanType', Utils.translate('Volume'));
                break;
            default:
        }
    },

    setRecurringAmount: function() {
        let url =
            '/app/accounting/subscription/subscriptionrecurringamount.nl';

        const params: object = {
            'jrmethod': 'remoteObject.getRecurringAmount',
            'jrparams': '['+ this.get('quantity') +', ' + this.get('pricePlanId') + ', ' +this.get('discount')+ ', true]'
        };

        url = Utils.addParamsToUrl(url, params);
    const self = this;
        jQuery
            .get(
                url
            )
            .then(function(result)  {
                let amount: any = JSON.parse(result)
                if(parseInt(self.get('recurringAmount')) !== parseInt(amount.result)){
                    self.set('recurringAmount', parseInt(amount.result));
                    self.set('recurringAmount_formatted', Utils.formatCurrency(amount.result));
                }
            })
    },

    getPrice: function() {
        var price_details = this.get('item').get('_priceDetails');
        if( !price_details ||
            !price_details.priceschedule ||
            !price_details.priceschedule.length) return;

        let price_intervals = price_details.priceschedule;
        const first_price_interval = price_intervals[0];
        const quantity = this.get('quantity') || 1;
        price_intervals = _.sortBy(price_intervals, 'minimumquantity').reverse();


        let matching_price_interval = _.find(price_intervals, function(price: any) {
            return (
                (!price.minimumquantity || price.minimumquantity <= quantity) &&
                (!price.maximumquantity || price.maximumquantity >= quantity)
            );
        });

        if(this.get('pricePlanType').toLowerCase() === 'volume'){

        }

        if (!matching_price_interval) matching_price_interval = first_price_interval;

        return {
            //maximumquantity: matching_price_interval.maximumquantity,
            //minimumquantity: matching_price_interval.minimumquantity,
            price_by_unit: matching_price_interval.price,
            price: matching_price_interval.price * quantity,
            price_formatted: Utils.formatCurrency(
                matching_price_interval.price * quantity,
                SC.ENVIRONMENT.currentCurrency.symbol
            ),
            frequency: matching_price_interval.frequency
        };
    },

    getStockInfo: function() {
        return {
            isInStock: true
        };
    },

    isReadOnly: function () {
        return this.get('status') !== 'NOT_INCLUDED' && (this.get('subscriptionLineType') === 1 || this.get('subscriptionLineType') === 3);
    },

    setLinePrices: function() {
        const price_intervals = this.get('priceIntervals');
        if(!price_intervals.length) return;
        const newest_price_interval = price_intervals[price_intervals.length - 1];

        const price_tiers_collection = newest_price_interval.pricePlan.priceTiers;

        const price_schedule = _.map(price_tiers_collection, (price_interval: any) =>{

            return {
                //maximumquantity: price_interval.maxamount,
                minimumquantity: price_interval.fromVal,
                price: price_interval.value,
                price_formatted: Utils.formatCurrency(price_interval.valueFormatted),
                showSingleValue: price_interval.showSingleValue,
                frequency: this.get('frequency'),
                is_range: false
            };
        });
        const item = this.get('item');

        item.set('_priceDetails', { priceschedule: price_schedule });
    }
});

export = SubscriptionsLineModel;
