/// <amd-module name="Subscriptions.Collection"/>

import * as _ from 'underscore';

import SubscriptionsModel = require('./Subscriptions.Model');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');
import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');

// @class Subscriptions.Collection @extend Backbone.Collection
const SubscriptionsCollection: any = Backbone.Collection.extend({
    // @property {Subscriptions.Model} model
    model: SubscriptionsModel,
    // @property {Boolean} cacheSupport enable or disable the support for cache (Backbone.CachedModel)
    cacheSupport: false,
    // @property {String} url
    url: Utils.getAbsoluteUrl('SC/Subscriptions/Subscriptions.ss', true),

    initialize: function(models, options) {
        this.customFilters = options && options.filters;
    },

    // @method parse Handle the response from the back-end to properly manage total records found value
    // @param {Object} response JSON Response from the back-end service
    // @return {Array<Object>} List of returned records from the back-end
    parse: function(response) {
        const original_items = _.compact(response);
        const items = _.sortBy(original_items, function(o: any) {
            return o.isActive;
        });

        if (!_.isUndefined(response.totalRecordsFound))
        {
            this.totalRecordsFound = response.totalRecordsFound;
            this.recordsPerPage = response.recordsPerPage;
        }

        return _.toArray(response.subscriptions);
    },

    // @method update Method called by ListHeader.View when applying new filters and constrains
    // @param {Collection.Filter} options
    // @return {Void}
    update: function(subscriptions) {
        const data = {};

        this.fetch({
            data: data,
            reset: true,
            killerId: subscriptions.killerId
        });
    }
});

export = SubscriptionsCollection;
