/// <amd-module name="Subscriptions.AddOn.Summary.View"/>
// Subscriptions.AddOn.Summary.View.js
// -----------------------

import subscription_addon_summary = require('../Templates/subscriptions_addon_summary.tpl') ;
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

const SubscriptionsAddOnSummaryView: any = BackboneView.extend({
    template: subscription_addon_summary,

    initialize: function(options) {
        this.model = options.model;
        this.model.on('change:recurringAmount_formatted',()=>{
            this.render();
        });
    },
    // @method getContext @returns {Overview.Banner.View.Context}
    getContext: function() {
        //var price_object = this.model.getPrice();

        // @class Overview.Banner.View.Context
        return {
            // @property {number} quantity
            itemQuantity: this.model.get('quantity'),
            // @property {bool} hasItemPrice
            hasItemPrice: !!this.model.get('recurringAmount'),
            // @property {String} itemPrice
            itemPrice: this.model.get('recurringAmount_formatted'),
            // @property {bool} itemPriceTotal
            itemPriceTotal: this.model.get('recurringAmount_formatted')
        };
    }
});

export = SubscriptionsAddOnSummaryView;
