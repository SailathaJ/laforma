/// <amd-module name="Subscriptions.AddOn.Details.View"/>
// @module SubscriptionsAddOnDetailsView

import * as _ from 'underscore';

import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import subscriptions_addon_details = require('../Templates/subscriptions_addon_details.tpl');

import AjaxRequestsKiller = require('../../../Commons/AjaxRequestsKiller/JavaScript/AjaxRequestsKiller');
import SubscriptionsModel = require('./Subscriptions.Model');
import SubscriptionsAddOnReviewView = require('./Subscriptions.AddOn.Review.View');
import QuantityPricingView = require('../../QuantityPricing/JavaScript/QuantityPricing.View');
import SummaryView = require('./Subscriptions.AddOn.Summary.View');
import resizeImage = require('../../../Commons/Utilities/JavaScript/Utilities.ResizeImage');

import QuantityAmount = require('./Subscriptions.AddOn.Quantity.Amount.View');

import Utils = require('../../../Commons/Utilities/JavaScript/Utils');
import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');
import GlobalViewsConfirmationView = require('../../../Commons/GlobalViews/JavaScript/GlobalViews.Confirmation.View');
import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');


const SubscriptionsAddOnDetailsView: any = BackboneView.extend({
    template: subscriptions_addon_details,

    title: Utils.translate('Add-On Details'),

    events: {
        'click [data-action="continue"]': 'goToReview',
        'click [data-action="cancel"]': 'cancelAddOn'
    },

    initialize: function(options) {
        this.application = options.application;
        this.options = options;

        this.model = this.options.model;
        this.subscription = this.options.subscription;

        this.model.on('change:recurringAmount_formatted', this.render, this );
    },

    beforeShowContent: function beforeShowContent() {
        if (!this.subscription || !this.model) {
            this.subscription = new SubscriptionsModel({
                internalid: this.options.routerArguments[0]
            });
            const fetch = this.subscription.fetch({ killerId: AjaxRequestsKiller.getKillerId()
            }).done(()=>{
                this.model = this.subscription.findLineInUnifiedCollection(parseInt(this.options.routerArguments[1], 10));
            });

            return fetch;
        }else{
            return jQuery.Deferred().resolve();
        }
    },

    childViews: {
      'Quantity.Amount' : function() {
          if(this.model.get('chargeType').toLowerCase() !== 'usage'){
            return new QuantityAmount({ model: this.model } );
          }
        },
        'Quantity.Pricing': function() {
          if(!this.quantityPricingView){
              const prices = this.model.get('item').get('_priceDetails') &&
                  this.model.get('item').get('_priceDetails').priceschedule;
              if(prices && prices.length > 1) {
                  this.quantityPricingView = new QuantityPricingView({
                      notUseAccordion: false,
                      model: this.model,
                      title: this.model.get('pricePlanType') + Utils.translate(' Pricing')
                  });
              }
          }
          return this.quantityPricingView;
        },
        'Summary.View': function() {
            return new SummaryView({
                model: this.model
            });
        }
    },

    cancelAddOn: function(e)
    {
        e.preventDefault();
        const deleteConfirmationView = new GlobalViewsConfirmationView({
            callBack: this._cancelAddon,
            callBackParameters: {
                context: this
            },
            title: Utils.translate('Cancel Line from subscription'),
            body: Utils.translate('Please, confirm you want to cancel this item from your subscription'),
            autohide: true
        });
        return this.application.getLayout().showInModal(deleteConfirmationView);
    },

    _cancelAddon: function(options)
    {
        options.context.subscription.save({
            lineNumber:options.context.model.get('lineNumber'),
            action:'delete'
        }).done(()=>{
            Backbone.history.navigate('subscription/'+options.context.subscription.get('internalid'), true );
        });
    },

    goToReview: function() {
        const subscriptions_addon_review_view = new SubscriptionsAddOnReviewView({
            application: this.options.application,
            subscription: this.subscription,
            model: this.model
        });

        subscriptions_addon_review_view.showContent();
    },

    // @method getSelectedMenu @return {String}
    getSelectedMenu: function() {
        return 'subscriptions';
    },
    // @method getBreadcrumbPages
    getBreadcrumbPages: function() {
        return {
            text: this.title,
            href: '/Subscriptions'
        };
    },

    getContext: function() {
        const item = this.model.get('item');
        const name = item.get('storeDisplayName') || item.get('itemId') || '';
        let price_total = this.model.get('recurringAmount_formatted') || Utils.translate('N/A');

        if (!!this.model.get('frequency')) {
            price_total = price_total + ' / ' + this.model.get('frequency');
        }
        // @class Overview.Banner.View.Context
        return {
            // @property {String} internalId
            internalId: this.model.get('internalId'),
            // @property {String} nextBillCycleDate
            nextBillCycleDate: this.model.get('nextbillcycledate'),
            // @property {String} lastBillDate
            lastBillDate: this.model.get('lastbilldate'),
            // @property {String} subscriptionPlanId
            subscriptionPlanId: this.model.get('subscriptionplan'),
            // @property {String} imageUrl
            imageUrl: resizeImage(item.get('imageUrl'), 'main'),
            // @property {String} displayName
            displayName: name,
            // @property {bool} hasDescription
            hasDescription: !!item.get('storedetailedDescription'),
            // @property {String} description
            description: item.get('storedetailedDescription'),
            // @property {String} itemPrice
            itemPrice: price_total,
            // @property {bool} hasItemPrice
            hasItemPrice: !!price_total,
            // @property {number} quantity
            quantity: this.model.get('quantity'),
            // @property {bool} setMinusDisabled
            setMinusDisabled: this.setMinusDisabled,
            // @property {bool} showInfoMessage
            showInfoMessage: this.model.get('isProrated') === true,
            // @property {bool} showContinueButton
            showContinueButton: !this.model.isReadOnly(),
            // @property {bool} showCancelButton
            showCancelButton: this.model.get('canBeSuspended')
        };
    }
});

export = SubscriptionsAddOnDetailsView;
