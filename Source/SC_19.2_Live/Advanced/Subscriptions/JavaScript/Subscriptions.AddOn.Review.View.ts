/// <amd-module name="Subscriptions.AddOn.Review.View"/>
import subscriptions_addon_review = require('../Templates/subscriptions_addon_review.tpl');
import SubscriptionModel = require('./Subscriptions.Model');
import SummaryView = require('./Subscriptions.AddOn.Summary.View');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');
import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');

const SubscriptionsAddOnReviewView: any = BackboneView.extend({

    template: subscriptions_addon_review,

    events: {
        'click [data-action="submit"]': 'submit'
    },

    title: Utils.translate('Add Subscription Review'),

    page_header: Utils.translate('Add Subscription Review'),

    initialize: function(options) {
        this.subscription = options.subscription;
        this.model = options.model;
    },

    childViews: {
        'Summary.View': function () {
            return new SummaryView({
                model: this.model
            });
        }
    },

    submit: function(e) {

        e.preventDefault();
        const updated_model = new SubscriptionModel();
        updated_model.set('internalid',this.subscription.get('internalid'));
        updated_model.set('quantity', this.model.get('quantity'));
        updated_model.set('lineNumber',this.model.get('lineNumber'));
        updated_model.save().done(()=>{
            Backbone.history.navigate('subscription/'+this.subscription.get('internalid'), true );
        });
    },

    // @method getSelectedMenu @return {String}
    getSelectedMenu: function() {
        return 'subscriptions';
    },
    // @method getBreadcrumbPages
    getBreadcrumbPages: function() {
        return {
            text: this.title,
            href: '/Subscriptions'
        };
    },

    getContext: function() {
        const item = this.model.get('item');

        return {
            itemName: item.get('itemId'),
            itemQuantity: this.model.get('quantity'),
            itemPrice: this.model.get('recurringAmount_formatted'),
            itemImage: item.get('imageUrl'),
            itemDescription: item.get('storedetailedDescription'),
            showInfoMessage: this.model.get('isProrated') === true
        };
    }
});

export = SubscriptionsAddOnReviewView;
