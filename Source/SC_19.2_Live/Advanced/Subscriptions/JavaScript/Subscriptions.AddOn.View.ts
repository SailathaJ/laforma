/// <amd-module name="Subscriptions.AddOn.View"/>
import subscriptions_addon_item = require('../Templates/subscriptions_addon_item_cell.tpl');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');
import UtilitiesResizeImage = require('../../../Commons/Utilities/JavaScript/Utilities.ResizeImage');
import SubscriptionsAddOnDetailsView = require('./Subscriptions.AddOn.Details.View');

const SubscriptionsAddOnView: any = BackboneView.extend({
    template: subscriptions_addon_item,

    title: Utils.translate('My Subscriptions'),

    page_header: Utils.translate('My Subscriptions'),

    events: {
        'click [data-action="add"]': 'addLine'
    },

    initialize: function(options) {
        this.subscription = options.subscription;
    },

    addLine: function(e) {
        e.preventDefault();
        const view = new SubscriptionsAddOnDetailsView({
            application: this.options.application,
            model: this.model,
            subscription: this.subscription
        });
        view.showContent();
    },

    getContext: function() {
        const PERIODS = {
            MONTHLY: Utils.translate('/month'),
            YEARLY: Utils.translate('/year'),
            WEEKLY: Utils.translate('/week')
        };
        const intervals = this.model.get('priceIntervals');
        const item = this.model.get('item');
        let price;
        if(    intervals.length &&
               intervals[0].pricePlan.priceTiers.length &&
               intervals[0].pricePlan.priceTiers[0].valueFormatted)
        {
            var length = intervals[0].pricePlan.priceTiers.length;
            price = intervals[0].pricePlan.priceTiers[0].valueFormatted;
            if(length>1)
            {
                price += ' - '+
                intervals[0].pricePlan.priceTiers[length-1].valueFormatted;
            }
            if(PERIODS[intervals[0].frequency])
            {
                price += ' ' + PERIODS[intervals[0].frequency];
            }
        }
        if(!price){
            price = Utils.translate('N/A');
        }
        return {
            description: item.get('storedetailedDescription'),
            end_date: Utils.isDateValid(new Date(this.model.get('EndDate'))) ? new Date(this.model.get('EndDate')).toDateString() : '',
            image: UtilitiesResizeImage(item.get('imageUrl'), 'thumbnail'),
            title: item.get('storeDisplayName') || item.get('itemId') || '',
            item_id: item.get('itemId') || '',
            lineNumber: this.model.get('lineNumber'),
            price: price,
            quantity: this.model.get('quantity'),
            start_date: new Date(this.model.get('startdate'))
        };
    }
});

export = SubscriptionsAddOnView;
