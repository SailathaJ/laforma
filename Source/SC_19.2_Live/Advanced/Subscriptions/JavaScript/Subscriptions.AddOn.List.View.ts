/// <amd-module name="Subscriptions.AddOn.List.View"/>
// @module Subscriptions

import * as _ from 'underscore';

import SubscriptionsModel = require('./Subscriptions.Model');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import BackboneCollectionView = require('../../../Commons/Backbone.CollectionView/JavaScript/Backbone.CollectionView');
import subscriptions_addons = require('../Templates/subscriptions_addons.tpl');
import backbone_collection_view_row_tpl = require('../../../Commons/Backbone.CollectionView/Templates/backbone_collection_view_row.tpl');
import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

import SubscriptionsAddOnView = require('./Subscriptions.AddOn.View');
import AjaxRequestsKiller = require('../../../Commons/AjaxRequestsKiller/JavaScript/AjaxRequestsKiller');

const SubscriptionsAddOnListView = BackboneView.extend({
    template: subscriptions_addons,

    title: Utils.translate('My Subscriptions'),

    page_header: Utils.translate('My Subscriptions'),

    attributes: {
        id: 'Subscriptions',
        class: 'Subscriptions'
    },

    initialize: function(options) {},
    beforeShowContent: function beforeShowContent() {
        if (!this.options.subscription) {
            this.subscription = new SubscriptionsModel({
                internalid: this.options.routerArguments[0]
            });
            const fetch = this.subscription
                .fetch({ killerId: AjaxRequestsKiller.getKillerId() })
                .done(()=>{
                    this.collection = this.subscription.get('nonIncludedLinesCollection');
                });

            return fetch;
        }
        this.subscription = this.options.subscription;
        this.collection = this.subscription.get('nonIncludedLinesCollection');
        return jQuery.Deferred().resolve();
    },

    // @method getSelectedMenu @return {String}
    getSelectedMenu: function() {
        return 'subscriptions';
    },
    // @method getBreadcrumbPages
    getBreadcrumbPages: function() {
        return {
            text: this.title,
            href: '/Subscriptions'
        };
    },

    childViews: {
        'Facets.Items': function() {
            return new BackboneCollectionView({
                childView: SubscriptionsAddOnView,
                childViewOptions: {
                    application: this.options.application,
                    subscription: this.subscription
                },
                viewsPerRow: (Utils.isPhoneDevice()) ? 2 : 3,
                collection: this.collection,
                rowTemplate: backbone_collection_view_row_tpl
            });
        }
    },

    // @method getContext @return {Subscriptions.AddOn.List.View.Context}
    getContext: function() {
        // @class Subscriptions.AddOn.List.View.Context
        return {
            //  @property {String} pageHeader
            pageHeader: this.subscription.get('name')
        };
    }
});

export = SubscriptionsAddOnListView;
