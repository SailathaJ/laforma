/// <amd-module name="Subscriptions.Line.View"/>
// Subscription.Line.View.js
// -----------------------

import subscriptions_Line_tpl = require('../Templates/subscriptions_line.tpl');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import SubscriptionAddOnDetailView = require('./Subscriptions.AddOn.Details.View');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

const SubscriptionLineView: any = BackboneView.extend({
    template: subscriptions_Line_tpl,

    events: {
        'click [data-action="change"]': 'goToPDP'
    },

    initialize: function(options) {
        this.subscription = options.subscription;
    },

    goToPDP: function() {
        const view = new SubscriptionAddOnDetailView({
            subscription: this.subscription,
            model: this.model,
            application: this.options.application
        });

        view.showContent();
    },

    // @method getContext @returns {Overview.Banner.View.Context}
    getContext: function() {
        const item = this.model.get('item');
        const name = item.get('storeDisplayName') || item.get('itemId') || '';
        const status = this.model.get('status') || '';
        const quantity = this.model.get('quantity') || Utils.translate('N/A');
        const start_date = this.model.get('startDate');
        const price = this.model.getPrice();
        const rate_formatted = this.model.get('recurringAmount_formatted') || Utils.translate('N/A');


        return {
            name: name,
            // @property {String} status
            status: this.model.getStatusLabel(),
            // @property {String} statusLowerCase
            statusLowerCase: this.model.getStatusLabel().toLowerCase(),
            // @property {Boolean} quantity
            quantity: quantity,
            // @property {Boolean} start_date
            startDate: start_date,
            // @property {String} subscriptionLineNumber
            subscriptionLineNumber: this.model.get('lineNumber'),
            // @property {hasPrice}
            hasPrice: !!(price && price.price_formatted),
            // @property {hasPeriodicity}
            hasPeriodicity: !!this.model.get('frequency'),
            // @property {amount} price
            price: rate_formatted,
            // @property {String} periodicity
            frequency: this.model.get('frequency'),
            // @property {number} type
            type: this.model.get('chargeType')
        };
    }
});

export = SubscriptionLineView;
