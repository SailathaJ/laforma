/// <amd-module name="SC.MyAccount.Starter"/>
/// <reference path="../../../Commons/Utilities/JavaScript/GlobalDeclarations.d.ts" />

import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');

import entryPointModules = require('./SC.MyAccount.Starter.Dependencies');
import extensionsPromise = require('../../../Commons/SC.Extensions/JavaScript/SC.Extensions');

import SCMyAccount = require('../../MyAccountApplication/JavaScript/SC.MyAccount');

class MyAccountStarter {
    private myAccount;

    constructor() {
        this.myAccount = SCMyAccount.getInstance();
        this.myAccount.getConfig().siteSettings = SC.ENVIRONMENT.siteSettings || {};
        const self = this;
        jQuery(function() {
            extensionsPromise.then((...entryPointExtensionsModules) =>
                self.init(entryPointModules.concat(entryPointExtensionsModules))
            );
        });
    }

    private init(allModules): void {
        const modulesList = this.myAccount.modulesToLoad(allModules);
        this.myAccount.start(modulesList, () => {
            this.checkForErrors();
            this.myAccount.getLayout().appendToDom();
        });
    }

    private checkForErrors(): void {
        if (SC.ENVIRONMENT.contextError) {
            this.myAccount
                .getLayout()
                .$('#site-header')
                .hide();
            this.myAccount
                .getLayout()
                .$('#site-footer')
                .hide();
            this.myAccount
                .getLayout()
                .internalError(
                    SC.ENVIRONMENT.contextError.errorMessage,
                    'Error ' +
                        SC.ENVIRONMENT.contextError.errorStatusCode +
                        ': ' +
                        SC.ENVIRONMENT.contextError.errorCode
                );
        } else {
            const { fragment } = Utils.parseUrlOptions(location.search);
            if (fragment && !location.hash) {
                location.hash = decodeURIComponent(fragment);
            }
            Backbone.history.start();
        }
    }
}

export = new MyAccountStarter();
