/// <amd-module name="OrderWizard.Module.PaymentMethod.ThreeDSecure"/>

import '../../../Commons/Transaction/JavaScript/Transaction.Model';
import * as _ from 'underscore';

import WizardModule = require('../../Wizard/JavaScript/Wizard.Module');
import order_wizard_paymentmethod_threedsecure_module_tpl = require('../Templates/order_wizard_paymentmethod_threedsecure_module.tpl');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

const OrderWizardModulePaymentMEthodThreeDSecure: any = WizardModule.extend({
    // @property {Function} template
    template: order_wizard_paymentmethod_threedsecure_module_tpl,

    // @property {String} title
    title: Utils.translate('Credit Card Authentication'),

    // @method initialize
    // @param {Object} options
    initialize: function(options) {
        this.application = options.application;
        this.deferred = options.deferred;

        WizardModule.prototype.initialize.apply(this, arguments);
    },

    // @method render
    // @return {Backbone.View} result
    render: function() {
        const result = WizardModule.prototype.render.apply(this, arguments);
        return result;
    },

    // @method showInModal
    // @param {Object} options
    // @return {jQuery.Deferred}
    showInModal: function(options) {
        const self = this;
        this.render();
        const promise = this.application
            .getLayout()
            .showInModal(this, _.extend({ keyboard: false, backdrop: 'static' }, options));
        promise.done(function() {
            self.listenForCallback();
        });

        this.on('modal-close', function() {
            this.model.fetch().done(function() {
                self.model.set('internalid', 'cart');
                self.wizard.getCurrentStep().enableNavButtons();
            });
        });

        return promise;
    },

    // @method process3DSecure. Called from ssp 3D Secure file (threedsecure.ssp)
    // @param {Json} confirmation Order submit answer.
    process3DSecure: function(order_info) {
        if (order_info.confirmation && order_info.confirmation.confirmationnumber) {
            this.error = null;
            this.model.set(order_info);
            this.success();
        } else {
            this.fail();
            if (order_info && order_info.confirmation && order_info.confirmation.errorMessage) {
                this.error = order_info.confirmation.errorMessage;
                this.wizard.manageError(order_info.confirmation);
                this.model.set('3dsecure_error', order_info.confirmation);
                this.showInModal();
            }
        }
    },

    // @method listenForCallback. Turns callback widely available, emptying it after use.
    listenForCallback: function() {
        const self = this;
        (<any>window).process3DSecure = function(data) {
            self.process3DSecure(data);
            (<any>window).process3DSecure = function() {};
        };
    },

    // @method fail Called if confirmation coming from 3D Secure ssp file fails.
    // @return {jQuery.Deferred} Rejected promise.
    fail: function() {
        return this.deferred.reject();
    },

    // @method success Called if confirmation coming from 3D Secure ssp file succeeded.
    // @return {jQuery.Deferred} Resolved promise.
    success: function() {
        this.$containerModal
            .removeClass('fade')
            .modal('hide')
            .data('bs.modal', null);
        return this.deferred.resolve();
    },

    // @method getContext
    // @return {OrderWizard.Module.PaymentMethod.ThreeDSecure.Context}
    getContext: function() {
        return {
            threeDSecureError: this.model.get('3dsecure_error'),
            iframe: this.model.get('confirmation').get('paymentauthorization').servicehtml
        };
    }
});

export = OrderWizardModulePaymentMEthodThreeDSecure;
