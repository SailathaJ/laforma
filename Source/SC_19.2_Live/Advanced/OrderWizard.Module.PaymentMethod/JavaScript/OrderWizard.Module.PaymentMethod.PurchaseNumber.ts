/// <amd-module name="OrderWizard.Module.PaymentMethod.PurchaseNumber"/>

import WizardModule = require('../../Wizard/JavaScript/Wizard.Module');
import Configuration = require('../../SCA/JavaScript/SC.Configuration');
import order_wizard_paymentmethod_purchasenumber_module_tpl = require('../Templates/order_wizard_paymentmethod_purchasenumber_module.tpl');
import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');

const OrderWizardModulePaymentMethodPurchaseNumber: any = WizardModule.extend({
    // @property {Function} template
    template: order_wizard_paymentmethod_purchasenumber_module_tpl,

    // @property {String} className
    className: 'OrderWizard.Module.PaymentMethod.PurchaseNumber',

    isActive: function(): boolean {
        return Configuration.get('siteSettings.checkout.showpofieldonpayment', 'T') === 'T';
    },

    submit: function() {
        const purchase_order_number = this.$('[name=purchase-order-number]').val() || '';
        this.wizard.model.set('purchasenumber', purchase_order_number);
        return jQuery.Deferred().resolve();
    },

    // @method getContext
    // @returns {OrderWizard.Module.PaymentMethod.Creditcard.Context}
    getContext: function() {
        return {
            // @property {String} purchaseNumber
            purchaseNumber: this.wizard.model.get('purchasenumber')
        };
    }
});

export = OrderWizardModulePaymentMethodPurchaseNumber;
