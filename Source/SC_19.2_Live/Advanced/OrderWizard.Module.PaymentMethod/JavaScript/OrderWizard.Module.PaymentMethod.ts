/// <amd-module name="OrderWizard.Module.PaymentMethod"/>

import WizardModule = require('../../Wizard/JavaScript/Wizard.Module');

const OrderWizardModulePaymentMethod: any = WizardModule.extend({
    submit: function() {
        // Gets the payment method for this object
        const payment_method = this.paymentMethod;
        return this.model.addPayment(payment_method);
    }
});

export = OrderWizardModulePaymentMethod;
