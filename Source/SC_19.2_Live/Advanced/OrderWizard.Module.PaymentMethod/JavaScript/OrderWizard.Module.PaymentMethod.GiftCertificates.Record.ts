/// <amd-module name="OrderWizard.Module.PaymentMethod.GiftCertificates.Record"/>

import GlobalViewsFormatPaymentMethodView = require('../../../Commons/GlobalViews/JavaScript/GlobalViews.FormatPaymentMethod.View');
import order_wizard_paymentmethod_giftcertificates_module_record_tpl = require('../Templates/order_wizard_paymentmethod_giftcertificates_module_record.tpl');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import BackboneCompositeView = require('../../../Commons/Backbone.CompositeView/JavaScript/Backbone.CompositeView');

// @class OrderWizard.Module.PaymentMethod.GiftCertificates.Record @extends Backbone.View
const OrderWizardModulePaymentMethodGiftCertificatesRecord: any = BackboneView.extend({
    template: order_wizard_paymentmethod_giftcertificates_module_record_tpl,

    initialize: function() {
        BackboneCompositeView.add(this);
    },

    // @property {Object} childViews
    childViews: {
        GiftCertificates: function() {
            return new GlobalViewsFormatPaymentMethodView({
                model: this.model
            });
        }
    },

    // @method getContext @return OrderWizard.Module.PaymentMethod.GiftCertificates.Record.Context
    getContext: function() {
        // @class OrderWizard.Module.PaymentMethod.GiftCertificates.Record.Context
        return {
            // @property {Quote.Model} collection
            giftcertificate: this.model.get('giftcertificate')
        };
    }
});

export = OrderWizardModulePaymentMethodGiftCertificatesRecord;
