/// <amd-module name="QuoteToSalesOrderWizard.Module.PaymentMethod.Selector"/>

import OrderWizardModulePaymentMethodSelector = require('../../OrderWizard.Module.PaymentMethod/JavaScript/OrderWizard.Module.PaymentMethod.Selector');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// @class PaymentWizard.Module.PaymentMethod.Creditcard @extend OrderWizard.Module.PaymentMethod.Creditcard
export = OrderWizardModulePaymentMethodSelector.extend({
    className: 'QuoteToSalesOrderWizard.Module.PaymentMethod.Selector',

    render: function() {
        if (this.wizard.hidePayment()) {
            this.$el.empty();
        } else {
            OrderWizardModulePaymentMethodSelector.prototype.render.apply(this, arguments);
        }

        if (this.selectedModule && !!~this.selectedModule.type.indexOf('external_checkout')) {
            this.trigger('change_label_continue', Utils.translate('Continue to External Payment'));
        } else {
            this.trigger('change_label_continue', Utils.translate('Submit'));
        }
    }
});
