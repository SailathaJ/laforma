/// <amd-module name="Home"/>
import * as home_cms_tpl from 'home_cms.tpl';

import HomeView = require('./Home.View');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// @class Home @extends ApplicationModule
export = {
    mountToApp: function(application) {
        const homeCMSTemplate = home_cms_tpl;
        const pageType = application.getComponent('PageType');

        pageType.registerPageType({
            name: 'home-page',
            routes: ['', '?*params'],
            view: HomeView,
            defaultTemplate: {
                name: 'home.tpl',
                displayName: 'Home Default',
                thumbnail: Utils.getThemeAbsoluteUrlOfNonManagedResources('img/default-layout-home.png')
            }
        });

        pageType.registerTemplate({
            pageTypes: ['home-page'],
            template: {
                name: 'home_cms.tpl',
                displayName: 'Home CMS',
                thumbnail: Utils.getThemeAbsoluteUrlOfNonManagedResources('img/cms-layout-home.png')
            }
        });
    }
};
