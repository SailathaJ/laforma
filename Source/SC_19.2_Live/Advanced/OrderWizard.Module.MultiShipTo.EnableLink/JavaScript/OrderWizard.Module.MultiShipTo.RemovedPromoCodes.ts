/// <amd-module name="OrderWizard.Module.MultiShipTo.RemovedPromoCodes"/>

import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import order_wizard_msr_removed_promocodes_tpl = require('../Templates/order_wizard_msr_removed_promocodes.tpl');
// @class OrderWizard.Module.MultiShipTo.RemovedPromoCodes @extend Backbone.View

export = BackboneView.extend({
    // @property {Function} template
    template: order_wizard_msr_removed_promocodes_tpl,

    // @method getContet
    // @return {OrderWizard.Module.MultiShipTo.RemovedPromoCodes.Context}
    getContext: function getContext() {
        // @class OrderWizard.Module.MultiShipTo.RemovedPromoCodes.Context
        return {
            invalidPromocodes: this.options.invalidPromocodes
        };
        // @class OrderWizard.Module.MultiShipTo.RemovedPromoCodes
    }
});
