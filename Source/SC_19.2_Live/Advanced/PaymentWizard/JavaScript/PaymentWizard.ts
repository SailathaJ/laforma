/// <amd-module name="PaymentWizard"/>

import PaymentWizardRouter = require('./PaymentWizard.Router');
import ProfileModel = require('../../../Commons/Profile/JavaScript/Profile.Model');
import LivePaymentModel = require('../../LivePayment/JavaScript/LivePayment.Model');
import PaymentWizardView = require('./PaymentWizard.View');
import PaymentWizardConfiguration = require('./PaymentWizard.Configuration');

// @class PaymentWizard @extends ApplicationModule
const PaymentWizard: any = {
    mountToApp: function(application) {
        const paymentRouter = new PaymentWizardRouter(application, {
            profile: ProfileModel.getInstance(),
            model: LivePaymentModel.getInstance(),
            steps: PaymentWizardConfiguration
        });

        PaymentWizardView.wizard = paymentRouter;

        return paymentRouter;
    }
};

export = PaymentWizard;
