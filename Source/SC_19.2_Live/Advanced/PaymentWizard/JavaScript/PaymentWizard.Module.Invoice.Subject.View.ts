/// <amd-module name="PaymentWizard.Module.Invoice.Subject.View"/>

import payment_wizard_invoice_subject_tpl = require('../Templates/payment_wizard_invoice_subject.tpl');

import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');

// @class PaymentWizard.Module.Invoice.Subject.View @extend BackboneView
const PaymentWizardModuleInvoiceSubjectView: any = BackboneView.extend({
    template: payment_wizard_invoice_subject_tpl,

    // @method getContext @return {PaymentWizard.Module.Invoice.Subject.View.Context}
    getContext: function() {
        // @class PaymentWizard.Module.Invoice.Subject.View.Context
        return {
            // @property {Boolean} isOverdue
            isOverdue: !!this.model.get('isoverdue'),
            // @property {String} dueDate
            dueDate: this.model.get('duedate') || ' ',
            // @property {Boolean} isDiscountApplied
            isDiscountApplied: !!this.model.get('discountapplies'),
            // @property {String} discountFormatted
            discountFormatted: this.model.get('discount_formatted'),
            // @property {String} discDate
            discDate: this.model.get('discdate'),
            // @property {Boolean} isPaid
            isPaid: !!this.model.get('ispaid')
        };
    }
});

export = PaymentWizardModuleInvoiceSubjectView;
