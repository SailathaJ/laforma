/// <amd-module name="PaymentWizard.CreditTransaction.Collection"/>

import Model = require('./PaymentWizard.CreditTransaction.Model');
import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');

// @class PaymentWizard.CreditTransaction.Collection @extend Backbone.Collection
const PaymentWizardCreditTransactionCollection: any = Backbone.Collection.extend({
    model: Model
});

export = PaymentWizardCreditTransactionCollection;
