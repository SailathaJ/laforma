/// <amd-module name="PaymentWizard.Module.CreditTransaction.Edit.Action.View"/>

import payment_wizard_credit_transaction_edit_action_tpl = require('../Templates/payment_wizard_credit_transaction_edit_action.tpl');

import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');

// @class PaymentWizard.Module.CreditTransaction.Edit.Action.View @extend BackboneView
const PaymentWizardModuleCreditTransactionEditActionView: any = BackboneView.extend({
    template: payment_wizard_credit_transaction_edit_action_tpl,

    // @method getContext @return {PaymentWizard.Module.CreditTransaction.Edit.Action.View.Context}
    getContext: function() {
        // @class PaymentWizard.Module.CreditTransaction.Edit.Action.View.Context
        return {
            // @property {Boolean} showEditAction
            showEditAction: !!this.model.get('check')
        };
    }
});

export = PaymentWizardModuleCreditTransactionEditActionView;
