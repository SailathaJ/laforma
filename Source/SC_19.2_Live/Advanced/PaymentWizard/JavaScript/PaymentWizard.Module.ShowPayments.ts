/// <amd-module name="PaymentWizard.Module.ShowPayments"/>

import * as _ from 'underscore';

import OrderWizardModuleShowPayments = require('../../OrderWizard.Module.PaymentMethod/JavaScript/OrderWizard.Module.ShowPayments');
import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');

// @class PaymentWizard.Module.ShowPayments @extend OrderWizard.Module.ShowPayments
const PaymentWizardModuleShowPayments: any = OrderWizardModuleShowPayments.extend({
    className: 'PaymentWizard.Module.ShowPayments',

    render: function() {
        this.options.hideBilling = true;

        if (this.wizard.hidePayment()) {
            this.$el.empty();
        } else {
            OrderWizardModuleShowPayments.prototype.render.apply(this, arguments);
        }
    },

    // @method getPaymentmethods
    getPaymentmethods: function() {
        return _.reject(
            this.model.get('confirmation').paymentmethods ||
                this.model.get('paymentmethods').models,
            function(paymentmethod: any): boolean {
                return paymentmethod.type === 'giftcertificate';
            }
        );
    },

    totalChange: jQuery.noop
});

export = PaymentWizardModuleShowPayments;
