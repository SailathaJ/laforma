/// <amd-module name="PaymentWizard.Module.ConfirmationSummary"/>

import * as _ from 'underscore';

import WizardModule = require('../../Wizard/JavaScript/Wizard.Module');
import payment_wizard_confirmation_summary_module_tpl = require('../Templates/payment_wizard_confirmation_summary_module.tpl');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// @class PaymentWizard.Module.ConfirmationSummary @extend Wizard.Module
const PaymentWizardModuleConfirmationSummary: any = WizardModule.extend({
    template: payment_wizard_confirmation_summary_module_tpl,

    className: 'PaymentWizard.Module.ConfirmationSummary',

    // @method getContext @return {PaymentWizard.Module.ConfirmationSummary.Context}
    getContext: function() {
        const is_confirmation_created = !!this.model.get('confirmation');
        const model = is_confirmation_created ? this.model.get('confirmation') : {};
        const selectedInvoicesLength = _.filter(model.invoices, function(obj: any) {
            return obj.apply === true;
        }).length;

        // @class PaymentWizard.Module.ConfirmationSummary.Context
        return {
            // @property {Number} selectedInvoicesLength
            selectedInvoicesLength: is_confirmation_created ? selectedInvoicesLength : 0,
            // @property {String} invoiceTotalFormatted
            invoiceTotalFormatted: model.invoices_total_formatted,
            // @property {String} paymentFormatted
            paymentFormatted: model.payment_formatted,
            // @property {String} depositTotalFormatted
            depositTotalFormatted: model.deposits_total_formatted,
            // @property {Boolean} hasDeposit
            hasDeposit: !!model.deposits_total,
            // @property {Boolean} hasCredit
            hasCredit: !!model.creditTotal,
            // @property {String} creditTotalFormatted
            creditTotalFormatted: model.credits_total_formatted,
            // @property {Boolean} showTotalLabel
            showTotalLabel: !!this.options.total_label,
            // @property {String} totalLabel
            totalLabel: this.options.total_label
                ? this.options.total_label
                : Utils.translate('Estimated payment')
        };
    }
});

export = PaymentWizardModuleConfirmationSummary;
