/// <amd-module name="PaymentWizard.CreditTransaction.Model"/>

import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

function validateAmountRemaining(value, name, form) {
    if (isNaN(parseFloat(value))) {
        return Utils.translate('The amount to apply is not a valid number');
    }

    if (value <= 0) {
        return Utils.translate('The amount to apply has to be positive');
    }

    if (value > form.remaining) {
        return Utils.translate('The amount to apply cannot exceed the remaining amount');
    }

    if (form.orderTotal < 0) {
        return Utils.translate('The amount to apply cannot exceed the remaining payment total');
    }
}

// @class PaymentWizard.CreditTransaction.Model @extend Backbone.Model
const PaymentWizardCreditTransactionModel: any = Backbone.Model.extend({
    validation: {
        amount: {
            fn: validateAmountRemaining
        }
    },

    initialize: function() {
        if (!this.get('type')) {
            this.set('type', 'Deposit');
        }
    }
});

export = PaymentWizardCreditTransactionModel;
