/// <amd-module name="PaymentWizard.Module.Invoice.Action.View"/>

import payment_wizard_invoice_action_tpl = require('../Templates/payment_wizard_invoice_action.tpl');

import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');

// @class PaymentWizard.Module.Invoice.Action.View @extend BackboneView
const PaymentWizardModuleInvoiceActionView: any = BackboneView.extend({
    template: payment_wizard_invoice_action_tpl,

    // @method getContext @return {PaymentWizard.Module.Invoice.Action.View.Context}
    getContext: function() {
        // @class PaymentWizard.Module.Invoice.Action.View.Context
        return {
            // @property {Boolean} isPayfull
            isPayfull: !!this.model.get('isPayFull'),
            // @property {Boolean} showAction
            showAction: !!this.model.get('check')
        };
    }
});

export = PaymentWizardModuleInvoiceActionView;
