/// <amd-module name="SC.Application"/>
// only used in test cases, consider remove

import SCApplicationSkeleton = require('./SC.Application.Skeleton');

// Application Creation:
// Applications will provide by default: Layout (So your views can talk to)
// and a Router (so you can extend them with some nice defaults)
// If you like to create extensions to the Skeleton you should extend SC.Application.Skeleton
const _applications = {};

export = function Application(application_name: string) {
    _applications[application_name] =
        _applications[application_name] || new SCApplicationSkeleton(application_name);
    return _applications[application_name];
};
