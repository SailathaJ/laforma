/// <amd-module name="Merchandising.jQueryPlugin">
import MerchandisingZone = require('./Merchandising.Zone');
import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');

/*
@module Merchandising
#Merchandizing jQuery plugin
Implements a jQuery plugin 'merchandisingZone' to handle the Merchandising Zone's intialization. Usage example:

	jQuery('my-custom-selector').merchandisingZone(options)

options MUST include the application its running id of the Zone to be rendered is optional IF it is on the element's data-id
*/

// [jQuery.fn](http://learn.jquery.com/plugins/basic-plugin-creation/)
(<any>jQuery.fn).merchandisingZone = function(options) {
    return this.each(function() {
        new MerchandisingZone(this, options);
    });
};
