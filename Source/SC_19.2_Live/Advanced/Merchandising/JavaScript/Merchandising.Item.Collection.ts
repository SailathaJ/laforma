/// <amd-module name="Merchandising.Item.Collection">
import * as _ from 'underscore';

import ItemCollection = require('../../../Commons/Item/JavaScript/Item.Collection');
import Session = require('../../../Commons/Session/JavaScript/Session');
import ProfileModel = require('../../../Commons/Profile/JavaScript/Profile.Model');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// @module Merchandising

// @class Merchandising.ItemCollection Item collection used for the merchandising zone
// we declare a new version of the ItemCollection
// to make sure the urlRoot doesn't get overridden
// @extends Item.Collection
export = ItemCollection.extend({
    url: function() {
        const profile = ProfileModel.getInstance();
        return Utils.addParamsToUrl(
            profile.getSearchApiUrl(),
            _.extend({}, this.searchApiMasterOptions, Session.getSearchApiParams()),
            profile.isAvoidingDoubleRedirect()
        );
    }
});
