/// <amd-module name="TransactionHistory.Collection"/>
// @module TransactionHistory

import Model = require('./TransactionHistory.Model');
import TransactionCollection = require('../../../Commons/Transaction/JavaScript/Transaction.Collection');

const TransactionHistoryCollection: any = TransactionCollection.extend({
    model: Model,

    // @property {Boolean} cacheSupport enable or disable the support for cache (Backbone.CachedModel)
    cacheSupport: false,

    url: 'services/TransactionHistory.Service.ss'
});

export = TransactionHistoryCollection;
