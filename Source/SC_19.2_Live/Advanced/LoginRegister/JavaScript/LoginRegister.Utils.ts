/// <amd-module name="LoginRegister.Utils"/>

import * as _ from 'underscore';

import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');
import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');

import ErrorManagement = require('../../../Commons/ErrorManagement/JavaScript/ErrorManagement');
import Configuration = require('../../SCA/JavaScript/SC.Configuration');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');
import jQueryExtras = require('../../../Commons/jQueryExtras/JavaScript/jQuery.serializeObject');

const LoginRegisterUtils: any = {
    skipLoginCloseModal: function() {
        if (this.$containerModal && Configuration.get('checkoutApp.skipLogin')) {
            this.$containerModal
                .removeClass('fade')
                .modal('hide')
                .data('bs.modal', null);
        }
    }
};

export = LoginRegisterUtils;
