/// <amd-module name="Location.Model"/>
import BackboneCachedModel = require('../../../Commons/BackboneExtras/JavaScript/Backbone.CachedModel');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

const LocationModel: any = BackboneCachedModel.extend({
    // @property {String} urlRoot
    urlRoot: Utils.getAbsoluteUrl('services/Location.Service.ss'),

    toString: function toString() {
        return this.get('internalid') || '';
    }
});

export = LocationModel;
