/// <amd-module name="ReorderItems"/>

import ReorderItemsListView = require('./ReorderItems.List.View');

import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

const ReorderItems: any = {
    mountToApp: function(application) {
        const pageType = application.getComponent('PageType');
        pageType.registerPageType({
            name: 'ReorderHistory',
            routes: ['reorderItems', 'reorderItems?:options'],
            view: ReorderItemsListView,
            defaultTemplate: {
                name: 'reorder_items_list.tpl',
                displayName: 'Reorder history default',
                thumbnail: Utils.getThemeAbsoluteUrlOfNonManagedResources('img/default-layout-transaction-list.png')
            }
        });
    }
};

export = ReorderItems;
