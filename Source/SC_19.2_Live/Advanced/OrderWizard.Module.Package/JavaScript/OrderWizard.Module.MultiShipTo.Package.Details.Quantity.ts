/// <amd-module name="OrderWizard.Module.MultiShipTo.Package.Details.Quantity"/>

import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import order_wizard_msr_package_details_quantity_tpl = require('../Templates/order_wizard_msr_package_details_quantity.tpl');

// @class OrderWizard.Module.MultiShipTo.Package.Details.Quantity @extend Backbone.View
export = BackboneView.extend({
    template: order_wizard_msr_package_details_quantity_tpl,

    // @method getContext @return {OrderWizard.Module.MultiShipTo.Package.Details.Quantity.Context}
    getContext: function() {
        // @class OrderWizard.Module.MultiShipTo.Package.Details.Quantity.Context
        return {
            // @property {Number} lineQuantity
            lineQuantity: this.model.get('quantity'),
            // @property {String} lineTotalFormatted
            lineTotalFormatted: this.model.get('total_formatted'),
            // @property {String} lineAmountFormatted
            lineAmountFormatted: this.model.get('amount_formatted'),
            // @property {Boolean} isAmountGreaterThanTotal
            isAmountGreaterThanTotal: this.model.get('amount') > this.model.get('total')
        };
    }
});
