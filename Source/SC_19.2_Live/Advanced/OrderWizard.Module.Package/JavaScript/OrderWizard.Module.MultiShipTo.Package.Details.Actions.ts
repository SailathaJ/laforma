/// <amd-module name="OrderWizard.Module.MultiShipTo.Package.Details.Actions"/>

import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import order_wizard_msr_package_details_actions_tpl = require('../Templates/order_wizard_msr_package_details_actions.tpl');

// @class OrderWizard.Module.MultiShipTo.Package.Details.Quantity @extend Backbone.View
export = BackboneView.extend({
    template: order_wizard_msr_package_details_actions_tpl,

    // @method getContext @return {OrderWizard.Module.MultiShipTo.Package.Details.Actions.Context}
    getContext: function() {
        // @class OrderWizard.Module.MultiShipTo.Package.Details.Actions.Context
        return {
            // @property {String} lineId
            lineId: this.model.id
        };
    }
});
