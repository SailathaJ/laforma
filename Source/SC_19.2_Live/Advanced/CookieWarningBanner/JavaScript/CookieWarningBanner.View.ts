/// <amd-module name="CookieWarningBanner.View"/>
// @module CookieWarningBanner.View

import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import Configuration = require('../../SCA/JavaScript/SC.Configuration');
import cookie_warning_banner_view_tpl = require('../Templates/cookie_warning_banner_view.tpl');
import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');
import cookie = require('../../../Commons/Utilities/JavaScript/jQuery.cookie');

// @class CookieWarningBanner.View @extend Backbone.View
const CookieWarningBannerView: any = BackboneView.extend({
    template: cookie_warning_banner_view_tpl,

    events: {
        'click [data-action="close-message"]': 'closeMessage'
    },

    // @method initialize Override default method to initialize the jQuery cookie JSON property
    // @return {Void}
    initialize: function initialize() {
        BackboneView.prototype.initialize.apply(this, arguments);
        jQuery.cookie.json = true;
    },

    // @method closeMessage Event handle for the close action
    closeMessage: function closeMessage() {
        jQuery.cookie('isCookieWarningClosed', true, { expires: 365 });
    },

    // @method showBanner Indicate if this current message should be shown or not
    // @return {Boolean}
    showBanner: function showBanner() {
        return (
            Configuration.get('siteSettings.showcookieconsentbanner') === 'T' &&
            !(
                Configuration.get('cookieWarningBanner.saveInCookie', false) &&
                jQuery.cookie('isCookieWarningClosed')
            )
        );
    },

    // @method getContext
    // @return {CookieWarningBanner.View.Context}
    getContext: function getContext() {
        // @class CookieWarningBanner.View.Context
        return {
            // @property {Boolean} showBanner
            showBanner: this.showBanner(),
            // @property {String} cookieMessage
            cookieMessage: Configuration.get('cookieWarningBanner.message', ''),
            // @property {Boolean} showLink
            showLink: !!Configuration.get('siteSettings.cookiepolicy', false),
            // @property {String} linkHref
            linkHref: Configuration.get('siteSettings.cookiepolicy', false),
            // @property {String} linkContent
            linkContent: Configuration.get('cookieWarningBanner.anchorText', ''),
            // @property {Boolean} showLink
            closable: Configuration.get('cookieWarningBanner.closable', true)
        };
        // @class CookieWarningBanner.View
    }
});

export = CookieWarningBannerView;
