/// <amd-module name="Account.ForgotPassword.Model"/>
// @module Account.ForgotPassword.Model

import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// @class Account.ForgotPassword.Model
// Sends user input data to the forgot password service
// validating email before is sent
// [Backbone.validation](https://github.com/thedersen/backbone.validation)
// @extend Backbone.Model
const AccountForgotPasswordModel: any = Backbone.Model.extend({
    // @property {String} urlRoot
    urlRoot: Utils.getAbsoluteUrl('services/Account.ForgotPassword.Service.ss'),

    // @property validation. Backbone.Validation attribute used for validating the form before submit.
    validation: {
        email: {
            required: true,
            pattern: 'email',
            msg: Utils.translate('Valid Email is required')
        }
    }
});

export = AccountForgotPasswordModel;
