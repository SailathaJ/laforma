/// <amd-module name="Account.Register.Model"/>
/// <reference path="../../../Commons/Utilities/JavaScript/GlobalDeclarations.d.ts"/>
// @module Account.Register.Model

import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// @class Account.Register.Model
// Sends user input data to the register service
// validating fields before they are sent
// [Backbone.validation](https://github.com/thedersen/backbone.validation)
// @extend Backbone.Model
const AccountRegisterModel: any = Backbone.Model.extend({
    // @property {String} urlRoot
    urlRoot: Utils.getAbsoluteUrl('services/Account.Register.Service.ss'),

    // @property {Object} validation. Backbone.Validation attribute used for validating the form before submit.
    validation: {
        firstname: {
            required: true,
            msg: Utils.translate('First Name is required')
        },
        lastname: {
            required: true,
            msg: Utils.translate('Last Name is required')
        },
        email: {
            required: true,
            pattern: 'email',
            msg: Utils.translate('Valid Email is required')
        },
        company: {
            required: SC.ENVIRONMENT.siteSettings.registration.companyfieldmandatory === 'T',
            msg: Utils.translate('Company Name is required')
        },
        password: {
            required: true,
            msg: Utils.translate('Please enter a valid password')
        },
        password2: [
            {
                required: true,
                msg: Utils.translate('Confirm password is required')
            },
            {
                equalTo: 'password',
                msg: Utils.translate('New Password and Confirm Password do not match')
            }
        ]
    }
});

export = AccountRegisterModel;
