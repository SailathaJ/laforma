/// <amd-module name="Account.Login.Model"/>
/// <reference path="../../../Commons/Utilities/JavaScript/GlobalDeclarations.d.ts"/>
// @module Account.Login.Model

import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// @class Account.Login.Model
// Sends user input data to the login service
// validating email and password before they are sent
// [Backbone.validation](https://github.com/thedersen/backbone.validation)
// @extend Backbone.Model
const AccountLoginModel: any = Backbone.Model.extend({
    // @property {String} urlRoot
    urlRoot:
        Utils.getAbsoluteUrl('services/Account.Login.Service.ss') +
        '?n=' +
        SC.ENVIRONMENT.siteSettings.siteid,

    // @property validation. Backbone.Validation attribute used for validating the form before submit.
    validation: {
        email: {
            required: true,
            pattern: 'email',
            msg: Utils.translate('Valid Email is required')
        },
        password: {
            required: true,
            msg: Utils.translate('Please enter a valid password')
        }
    }
});

export = AccountLoginModel;
