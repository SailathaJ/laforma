/// <amd-module name="Account.ResetPassword.Model"/>
// @module Account.ResetPassword.Model

import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// @class Account.ResetPassword.Model
// Sends user input data to the reset password service
// validating passwords before they are sent
// [Backbone.validation](https://github.com/thedersen/backbone.validation)
// @extend Backbone.Mode
const AccountResetPasswordModel: any = Backbone.Model.extend({
    // @property {String} urlRoot
    urlRoot: Utils.getAbsoluteUrl('services/Account.ResetPassword.Service.ss'),

    // @property {Object} validation. Backbone.Validation attribute used for validating the form before submit.
    validation: {
        confirm_password: [
            {
                required: true,
                msg: Utils.translate('Confirm password is required')
            },
            {
                equalTo: 'password',
                msg: Utils.translate('New Password and Confirm Password do not match')
            }
        ],

        password: {
            required: true,
            msg: Utils.translate('New  password is required')
        }
    }
});

export = AccountResetPasswordModel;
