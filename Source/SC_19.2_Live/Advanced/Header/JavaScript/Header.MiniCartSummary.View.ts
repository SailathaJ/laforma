/// <amd-module name="Header.MiniCartSummary.View"/>

import * as _ from 'underscore';

import LiveOrderModel = require('../../../Commons/LiveOrder/JavaScript/LiveOrder.Model');
import header_mini_cart_summary_tpl = require('../Templates/header_mini_cart_summary.tpl');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// @module Header

// @class Header.MiniCartSummary.View @extends Backbone.View
export = BackboneView.extend({
    template: header_mini_cart_summary_tpl,

    initialize: function() {
        const self = this;

        this.itemsInCart = 0;
        this.showPluraLabel = true;
        this.isLoading = true;

        LiveOrderModel.loadCart().done(function() {
            const cart = LiveOrderModel.getInstance();
            self.itemsInCart = _.reduce(
                cart.get('lines').pluck('quantity'),
                function(memo, quantity: string) {
                    return memo + (parseFloat(quantity) || 1);
                },
                0
            );

            self.showPluraLabel = self.itemsInCart !== 1;
            self.isLoading = false;
            self.render();

            cart.on('change', function() {
                self.render();
            });
        });

        this.on('afterViewRender', function() {
            self.isLoading && Utils.ellipsis('.header-mini-cart-summary-cart-ellipsis');
        });
    },

    // @method getContext @return {Header.MiniCart.View.Context}
    getContext: function() {
        // @class Header.MiniCartSummary.View.Context
        return {
            // @property {Number} itemsInCart
            itemsInCart: this.itemsInCart,
            // @property {Boolean} showPluraLabel
            showPluraLabel: this.showPluraLabel,
            // @property {Boolean} isLoading
            isLoading: this.isLoading
        };
    }
});
