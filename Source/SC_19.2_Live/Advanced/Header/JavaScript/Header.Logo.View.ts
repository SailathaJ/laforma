/// <amd-module name="Header.Logo.View"/>

import Configuration = require('../../../Commons/Utilities/JavaScript/SC.Configuration');
import header_logo_tpl = require('../Templates/header_logo.tpl');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// @module Header
// @class Header.Logo.View @extends BackboneView
export = BackboneView.extend({
    // @property {Function} template
    template: header_logo_tpl,

    // @method initialize
    // @param {Header.LogoView.Initialize.Options} options
    // @return {Void}

    // @method getContext @return {Header.Logo.View.Context}
    getContext: function() {
        // @class Header.Logo.View.Context
        return {
            // @property {String} logoUrl
            logoUrl: Utils.getAbsoluteUrlOfNonManagedResources(Configuration.get('header.logoUrl')),
            // @property {String} headerLinkHref
            headerLinkHref: this.options.headerLinkHref || '/',
            // @property {String} headerLinkTouchPoint
            headerLinkTouchPoint: this.options.headerLinkTouchPoint || 'home',
            // @property {String} headerLinkHashtag
            headerLinkHashtag: this.options.headerLinkHashtag || '#',
            // @property {String} headerLinkTitle
            headerLinkTitle: this.options.headerLinkTitle || SC.ENVIRONMENT.siteSettings.displayname
        };
    }
});
// @class Header.LogoView.Initialize.Options
// @property {String} headerLinkHref This is the URL the header uses
// @property {String} headerLinkTouchPoint
// @property {String} headerLinkHashtag
// @property {String} headerLinkTitle
