/// <amd-module name="Header.Simplified.View"/>

import HeaderLogoView = require('./Header.Logo.View');
import header_simplified_tpl = require('../Templates/header_simplified.tpl');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import BackboneCompositeView = require('../../../Commons/Backbone.CompositeView/JavaScript/Backbone.CompositeView');

// @class Header.Simplified.View @extends Backbone.View
export = BackboneView.extend({
    // @property {Function} template
    template: header_simplified_tpl,
    // @method initialize
    initialize: function() {
        BackboneCompositeView.add(this);
    },
    // @property {Object} childViews
    childViews: {
        'Header.Logo': function() {
            return new HeaderLogoView(this.options);
        }
    }
});
