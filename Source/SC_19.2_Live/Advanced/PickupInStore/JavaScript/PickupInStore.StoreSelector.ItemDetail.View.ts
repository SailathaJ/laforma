/// <amd-module name="PickupInStore.StoreSelector.ItemDetail.View"/>

import TransactionLineViewsPriceView = require('../../../Commons/Transaction.Line.Views/JavaScript/Transaction.Line.Views.Price.View');
import ProductLineSkuView = require('../../../Commons/ProductLine/JavaScript/ProductLine.Sku.View');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import transaction_line_views_cell_navigable_tpl = require('../../../Commons/Transaction.Line.Views/Templates/transaction_line_views_cell_navigable.tpl');

const PickupInStoreStoreSelectorItemDetailView: any = BackboneView.extend({
    // @property {Function} template
    template: transaction_line_views_cell_navigable_tpl,

    // @method initialize
    // @param {PickupInStore.StoreSelector.ItemDetail.View.InitializeParameters} options
    initialize: function(options) {
        this.options = options;
        this.application = options.application;
        this.model = options.model;
    },

    // @property {ChildViews} childViews
    childViews: {
        'Item.Price': function() {
            return new TransactionLineViewsPriceView({
                model: this.model
            });
        },
        'Item.Sku': function() {
            return new ProductLineSkuView({
                model: this.model
            });
        }
    },

    // @method getContext
    // @return {PickupInStore.StoreSelector.ItemDetail.View.Context}
    getContext: function() {
        const item = this.model.get('item');

        // @class PickupInStore.StoreSelector.ItemDetail.View.Context
        return {
            // @property {String} itemName
            itemName: item.get('_name'),
            // @property {ImageContainer} thumbnail
            thumbnail: this.model.getThumbnail()
        };
    }
});

export = PickupInStoreStoreSelectorItemDetailView;
