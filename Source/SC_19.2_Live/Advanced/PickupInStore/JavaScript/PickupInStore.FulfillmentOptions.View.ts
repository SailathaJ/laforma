/// <amd-module name="PickupInStore.FulfillmentOptions.View"/>

import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import pickup_in_store_fulfillment_options_tpl = require('../Templates/pickup_in_store_fulfillment_options.tpl');

const PickupInStoreFulfillmentOptionsView: any = BackboneView.extend({
    // @property {Function} template
    template: pickup_in_store_fulfillment_options_tpl,

    // @method initialize
    initialize: function initialize(options) {
        this.model = options.model;
    },

    // @method getContext
    getContext: function getContext() {
        this.item = this.model;

        const stock_information = this.model.getStockInfo();
        const is_available_for_ship =
            this.item.get('_isBackorderable') || this.item.get('_isInStock');

        return {
            // @property {Boolean} isAvailableForShip
            isAvailableForShip: is_available_for_ship,
            // @property {Boolean} isAvailableForPickup
            isAvailableForPickup: stock_information.isAvailableForPickup
        };
    }
});

export = PickupInStoreFulfillmentOptionsView;
