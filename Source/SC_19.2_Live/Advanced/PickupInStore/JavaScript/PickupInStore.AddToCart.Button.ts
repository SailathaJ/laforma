/// <amd-module name="PickupInStore.AddToCart.Button"/>

import Configuration = require('../../SCA/JavaScript/SC.Configuration');
import CartAddToCartButtonView = require('./PickupInStore.AddToCart.Button');

const PickupInStoreAddToCartButton: any = function() {
    if (Configuration.get('siteSettings.isPickupInStoreEnabled')) {
        const original_add_to_cart_fn = (<any>CartAddToCartButtonView).prototype.addToCart;

        (<any>CartAddToCartButtonView).prototype.addToCart = function() {
            if (
                !this.model.getItem().get('_isstorepickupallowed') &&
                this.model.get('fulfillmentChoice') === 'pickup'
            ) {
                this.model.set('fulfillmentChoice', 'ship');
            }

            return original_add_to_cart_fn.apply(this, arguments);
        };
    }
};

export = PickupInStoreAddToCartButton;
