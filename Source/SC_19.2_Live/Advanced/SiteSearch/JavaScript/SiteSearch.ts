/// <amd-module name="SiteSearch"/>

import SiteSearchView = require('./SiteSearch.View');
import SiteSearchButtonView = require('./SiteSearch.Button.View');

export = {
    mountToApp: function(application) {
        const layout = application.getComponent('Layout');
        layout.registerView('SiteSearch', function() {
            return new SiteSearchView({ application: application });
        });
        layout.registerView('SiteSearch.Button', function() {
            return new SiteSearchButtonView({ application: application });
        });
    }
};
