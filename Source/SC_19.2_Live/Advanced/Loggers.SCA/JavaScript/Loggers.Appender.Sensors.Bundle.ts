/// <amd-module name="Loggers.Appender.Sensors.Bundle"/>
/// <reference path="../../../Commons/Utilities/JavaScript/GlobalDeclarations.d.ts" />

import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');

const loggersAppenderSensorsBundle = {
    extract: () => {
        const metadata = SC.ENVIRONMENT.RELEASE_METADATA || {};

        const data = {
            bundleId: metadata.prodbundle_id || '',
            bundleName: metadata.name || '',
            bundleVersion: metadata.version || '',
            buildNo: metadata.buildno || '',
            dateLabel: metadata.datelabel || '',
            baseLabel: metadata.baselabel || ''
        };

        return jQuery.Deferred().resolve(data);
    }
};

export { loggersAppenderSensorsBundle };
