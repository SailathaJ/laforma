/// <amd-module name="Loggers.Appender.Sensors.Customer"/>

import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');
import ProfileModel = require('../../../Commons/Profile/JavaScript/Profile.Model');

const loggersAppenderSensorsCustomer = {
    extract: () => {
        const profile_model = ProfileModel.getInstance();
        const isGuest = profile_model.get('isGuest') === 'T';
        const isLoggedIn = !isGuest && profile_model.get('isLoggedIn') === 'T';
        const isRecognized = !isGuest && profile_model.get('isRecognized') === 'T';
        const isReturning = !isGuest && isLoggedIn;
        const isNew = !isGuest && !isRecognized && !isLoggedIn;
        const data: any = {};

        data.customerSessionStatus = isNew
            ? 'New'
            : isReturning
            ? 'Returning'
            : isGuest
            ? 'Guest'
            : isRecognized
            ? 'Recognized'
            : '';

        const regex = new RegExp('[; ]NLVisitorId=([^\\s;]*)');
        const sMatch = (' ' + document.cookie).match(regex);
        data.visitorId = sMatch ? unescape(sMatch[1]) : '';

        return jQuery.Deferred().resolve(data);
    }
};

export { loggersAppenderSensorsCustomer };
