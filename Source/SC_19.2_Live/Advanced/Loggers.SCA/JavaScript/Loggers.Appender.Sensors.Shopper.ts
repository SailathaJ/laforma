/// <amd-module name="Loggers.Appender.Sensors.Shopper"/>

import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');
import ProfileModel = require('../../../Commons/Profile/JavaScript/Profile.Model');
import Session = require('../../../Commons/Session/JavaScript/Session');

const loggersAppenderSensorsShopper = {
    extract: () => {
        const data = { shopperInternalId: '', currencyCode: '' };
        const profile_instance = ProfileModel.getInstance();

        const shopper_id = profile_instance.get('internalid');
        if (shopper_id && shopper_id !== '0') {
            data.shopperInternalId = shopper_id;
        }

        data.currencyCode = Session.get('currency.code') || '';

        return jQuery.Deferred().resolve(data);
    }
};

export { loggersAppenderSensorsShopper };
