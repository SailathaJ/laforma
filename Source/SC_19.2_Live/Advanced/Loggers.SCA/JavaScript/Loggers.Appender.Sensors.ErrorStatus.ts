/// <amd-module name="Loggers.Appender.Sensors.ErrorStatus"/>

import ComponentContainer = require('../../../Commons/SC/JavaScript/SC.ComponentContainer');
import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');

const loggersAppenderSensorsErrorStatus = {
    extract: () => {
        const { application } = ComponentContainer.getComponent('Layout');

        const current_view = application.getLayout().getCurrentView();
        const errorData =
            current_view.isErrorView && (current_view.getPageDescription() || 'error');

        return jQuery.Deferred().resolve({ errorStatus: errorData });
    }
};

export { loggersAppenderSensorsErrorStatus };
