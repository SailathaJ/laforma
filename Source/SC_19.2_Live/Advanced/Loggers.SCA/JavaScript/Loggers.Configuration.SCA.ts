/// <amd-module name="Loggers.Configuration.SCA"/>

import { LoggersAppenderDummy } from '../../../Commons/Loggers/JavaScript/Loggers.Appender.Dummy';
import { LoggersAppenderSensors } from './Loggers.Appender.Sensors';
import { LoggersAppenderElasticLogger } from '../../../Commons/Loggers/JavaScript/Loggers.Appender.ElasticLogger';
import {
    ConfigurationLoggers,
    dev,
    prod
} from '../../../Commons/Loggers/JavaScript/Loggers.Configuration';

// const dummy = LoggersAppenderDummy.getInstance();
const sensors = LoggersAppenderSensors.getInstance();
const elasticlogger = LoggersAppenderElasticLogger.getInstance();

export const configuration: ConfigurationLoggers = {
    root: {
        log: [{ profile: prod, appenders: [elasticlogger] }, { profile: dev, appenders: [] }],
        actions: {
            Navigation: [{ profile: prod, appenders: [sensors] }, { profile: dev, appenders: [] }],
            'Place Order': [
                { profile: prod, appenders: [sensors] },
                { profile: dev, appenders: [] }
            ],
            'Add to Cart': [
                { profile: prod, appenders: [sensors] },
                { profile: dev, appenders: [] }
            ],
            'Save Credit Card': [
                { profile: prod, appenders: [sensors] },
                { profile: dev, appenders: [] }
            ],
            'Save Address': [
                { profile: prod, appenders: [sensors] },
                { profile: dev, appenders: [] }
            ],
            'Customer Registration': [
                { profile: prod, appenders: [sensors] },
                { profile: dev, appenders: [] }
            ]
        },
        loggers: {}
    }
};
