/// <amd-module name="Loggers.Appender.Sensors.SiteData"/>

import ComponentContainer = require('../../../Commons/SC/JavaScript/SC.ComponentContainer');
import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');

const loggersAppenderSensorsSiteData = {
    extract: () => {
        const pageType = ComponentContainer.getComponent('PageType');
        const context = pageType.getContext();

        const data = {
            sitePage: context.page_type,
            siteFragment: context.path,
            sitePageDisplayName: context.page_type_display_name,
            siteUrl: SC.ENVIRONMENT.shoppingDomain
        };

        return jQuery.Deferred().resolve(data);
    }
};

export { loggersAppenderSensorsSiteData };
