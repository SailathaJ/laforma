/// <amd-module name="Loggers.Appender.Sensors.Device"/>

import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

const loggersAppenderSensorsDevice = {
    extract: () => {
        const data = {
            device: Utils.getDeviceType()
        };

        return jQuery.Deferred().resolve(data);
    }
};

export { loggersAppenderSensorsDevice };
