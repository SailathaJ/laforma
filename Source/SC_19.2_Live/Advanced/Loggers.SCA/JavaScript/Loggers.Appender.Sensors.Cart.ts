/// <amd-module name="Loggers.Appender.Sensors.Cart"/>

import ComponentContainer = require('../../../Commons/SC/JavaScript/SC.ComponentContainer');
import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');

const loggersAppenderSensorsCart = {
    extract: () => {
        const cartPromise = jQuery.Deferred();
        const cart = ComponentContainer.getComponent('Cart');

        cart.getLines().done(lines => {
            const data = {
                cartLines: lines.length + ''
            };

            cartPromise.resolve(data);
        });

        return cartPromise;
    }
};

export { loggersAppenderSensorsCart };
