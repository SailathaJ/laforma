/// <amd-module name="Loggers.Appender.Sensors"/>
/// <reference path="../../../Commons/Utilities/JavaScript/GlobalDeclarations.d.ts" />

import * as _ from 'underscore';
import { LoggersAppender } from '../../../Commons/Loggers/JavaScript/Loggers.Appender';

import { loggersAppenderSensorsSiteData } from './Loggers.Appender.Sensors.SiteData';
import { loggersAppenderSensorsBundle } from './Loggers.Appender.Sensors.Bundle';
import { loggersAppenderSensorsCart } from './Loggers.Appender.Sensors.Cart';
import { loggersAppenderSensorsShopper } from './Loggers.Appender.Sensors.Shopper';
import { loggersAppenderSensorsCustomer } from './Loggers.Appender.Sensors.Customer';
import { loggersAppenderSensorsDevice } from './Loggers.Appender.Sensors.Device';
import { loggersAppenderSensorsErrorStatus } from './Loggers.Appender.Sensors.ErrorStatus';

import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');

interface LoggersOptions {
    baseLabel: string;
    buildNo: string;
    bundleId: string;
    bundleName: string;
    bundleVersion: string;
    cartLines: string;
    currencyCode: string;
    customerSessionStatus: string;
    dateLabel: string;
    device: string;
    shopperInternalId: string;
    siteUrl: string;
    visitorId: string;
}

interface NavigationLoggersOptions extends LoggersOptions {
    errorStatus: undefined | string;
    linkType: string;
    siteFragment: string;
    sitePage: string;
    sitePageDisplayName: string;
    showContentTime: number;
}

interface StandardLoggersOptions extends LoggersOptions {
    actionId: string;
    cartLines: string;
    operationIds: string[];
    status: string;
}

export class LoggersAppenderSensors implements LoggersAppender {
    private static instance: LoggersAppenderSensors;

    private dataExtractorNavigation = [
        loggersAppenderSensorsSiteData,
        loggersAppenderSensorsBundle,
        loggersAppenderSensorsCart,
        loggersAppenderSensorsCustomer,
        loggersAppenderSensorsShopper,
        loggersAppenderSensorsDevice,
        loggersAppenderSensorsErrorStatus
    ];

    private dataExtractor = [
        loggersAppenderSensorsSiteData,
        loggersAppenderSensorsBundle,
        loggersAppenderSensorsCustomer,
        loggersAppenderSensorsShopper,
        loggersAppenderSensorsDevice
    ];

    private NLRUM: any;

    private firstTime: boolean = true;

    private applicationStartTime: number;

    private paramsMap = {};

    private enabled = !SC.isPageGenerator() && SC.ENVIRONMENT.SENSORS_ENABLED;

    private registerMap() {
        this.paramsMap = {
            status: {
                key: 'status',
                values: {
                    success: this.NLRUM.status.completed,
                    fail: this.NLRUM.status.incomplete,
                    cancelled: this.NLRUM.status.cancelled
                }
            }
        };
    }

    private mapParams(params: StandardLoggersOptions) {
        const mapped = {};

        _.mapObject(params, (value, key) => {
            if (this.paramsMap[key]) {
                mapped[this.paramsMap[key].key] =
                    this.paramsMap[key].values && this.paramsMap[key].values[value]
                        ? this.paramsMap[key].values[value]
                        : value;
            } else {
                mapped[key] = value;
            }
        });

        return mapped;
    }

    protected constructor() {
        // don't execute in Page Generator
        if (this.enabled) {
            this.NLRUM = (<any>window).NLRUM;

            if (this.NLRUM && this.NLRUM.addSCData) {
                this.applicationStartTime = (<any>window).applicationStartTime;
                this.registerMap();
            } else {
                this.enabled = false;
                console.log('nlRUM.js failed to load');
            }
        }
    }

    ready() {
        return this.enabled;
    }

    info() {}

    error() {}

    private startNavigation() {
        if (this.applicationStartTime) {
            const { firstTime } = this;

            if (!firstTime) {
                this.NLRUM.markIndirectStart && this.NLRUM.markIndirectStart();
            }

            const options: any = { action: 'Navigation' };

            if (this.firstTime) {
                this.firstTime = false;
                options.startTime = this.applicationStartTime;
                options.linkType = 'direct';
            } else {
                options.linkType = 'indirect';
                options.startTime = Date.now();
            }

            return options;
        }
    }

    start(action: string, _options: object) {
        if (action === 'Navigation') {
            return this.startNavigation();
        }
        const promise = jQuery.Deferred();

        loggersAppenderSensorsCart.extract().done((cart, _nrlum) => {
            const data: any = {};

            data.actionId = this.NLRUM.actionStart(action);
            data.cartLines = cart.cartLines;

            promise.resolve(data);
        });

        return { promise: promise };
    }

    private endNavigation(options) {
        if (this.applicationStartTime) {
            const self = this;
            const data: NavigationLoggersOptions = {
                baseLabel: '',
                buildNo: '',
                bundleId: '',
                bundleName: '',
                bundleVersion: '',
                cartLines: '',
                currencyCode: '',
                customerSessionStatus: '',
                dateLabel: '',
                device: '',
                shopperInternalId: '',
                siteUrl: '',
                visitorId: '',
                siteFragment: '',
                sitePage: '',
                sitePageDisplayName: '',
                errorStatus: undefined,
                linkType: '',
                showContentTime: 0
            };

            data.linkType = options.linkType;
            data.showContentTime = Date.now() - options.startTime;

            jQuery.when
                .apply(jQuery, _.invoke(this.dataExtractorNavigation, 'extract'))
                .done((...results) => {
                    self.NLRUM.addSCData(
                        LoggersAppenderSensors.loggerData(results, data, 'Navigation')
                    );
                });
        }
    }

    private static loggerData(
        loggerOptionsItem: LoggersOptions[],
        data: LoggersOptions,
        loggerType: string = 'Not Navigation'
    ): LoggersOptions {
        const navigationProperties = ['sitePage', 'siteFragment', 'sitePageDisplayName'];

        _.each(loggerOptionsItem, argument => {
            Object.getOwnPropertyNames(argument).forEach((key: string) => {
                let addItem = false;
                if (loggerType === 'Navigation' || navigationProperties.indexOf(key) === -1) {
                    addItem = true;
                }
                if (addItem) {
                    data[key] = argument[key];
                }
            });
        });
        return data;
    }

    private extractOperationIds(options) {
        if (_.isString(options.operationIds)) {
            return [options.operationIds];
        }
        if (_.isArray(options.operationIds)) {
            return options.operationIds.slice(0);
        }
        return [];
    }

    end(dataStart, options: StandardLoggersOptions) {
        if (dataStart.action === 'Navigation') {
            this.endNavigation(dataStart);
        } else {
            dataStart.promise.done(data => {
                options.operationIds = this.extractOperationIds(options);
                jQuery.when
                    .apply(jQuery, _.invoke(this.dataExtractor, 'extract'))
                    .done((...results) => {
                        this.NLRUM.actionEnd(
                            data.actionId,
                            this.mapParams(
                                _.extend(
                                    {},
                                    data,
                                    LoggersAppenderSensors.loggerData(results, options)
                                )
                            )
                        );
                    });
            });
        }
    }

    static getInstance() {
        if (!LoggersAppenderSensors.instance) {
            LoggersAppenderSensors.instance = new LoggersAppenderSensors();
        }

        return LoggersAppenderSensors.instance;
    }
}
