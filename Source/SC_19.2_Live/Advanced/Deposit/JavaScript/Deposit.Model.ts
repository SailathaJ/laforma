/// <amd-module name="Deposit.Model"/>

import TransactionModel = require('../../../Commons/Transaction/JavaScript/Transaction.Model');
import InvoiceCollection = require('../../Invoice/JavaScript/Invoice.Collection');

// @class Deposit.Model @extend Transaction.Model
const DepositModel: any = TransactionModel.extend({
    urlRoot: 'services/Deposit.Service.ss',

    // @property {Boolean} cacheSupport enable or disable the support for cache (Backbone.CachedModel)
    cacheSupport: true,

    initialize: function initialize(attributes) {
        TransactionModel.prototype.initialize.apply(this, arguments);

        this.on('change:invoices', function(model, invoices) {
            model.set('invoices', new InvoiceCollection(invoices), { silent: true });
        });

        this.trigger('change:invoices', this, (attributes && attributes.invoices) || []);
    }
});

export = DepositModel;
