/// <amd-module name="Deposit.Details.DepositApplication.Link.View"/>

import deposit_details_deposit_application_link_tpl = require('../Templates/deposit_details_deposit_application_link.tpl');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');

// @class Deposit.Details.DepositApplication.Link.View @extend Backbone.View
const DepositDetailsDepositApplicationLinkView: any = BackboneView.extend({
    template: deposit_details_deposit_application_link_tpl,

    events: {
        'click [data-action="go-to-deposit-application"]': 'goToDepositApplication'
    },

    goToDepositApplication: function(e) {
        e.stopPropagation();
    },
    // @method getContext @return Deposit.Details.DepositApplication.Link.View.Context
    getContext: function() {
        // @class Deposit.Details.DepositApplication.Link.View.Context
        return {
            // @property {String} depositApplicationId
            depositApplicationId: this.model.get('depositId'),
            // @property {String} depositApplicationTitle
            depositApplicationDate: this.model.get('depositDate')
        };
    }
});

export = DepositDetailsDepositApplicationLinkView;
