/// <amd-module name="RequestQuoteAccessPoints.HeaderLink.View"/>

import Configuration = require('../../SCA/JavaScript/SC.Configuration');
import requestquote_accesspoints_headerlink_tpl = require('../Templates/requestquote_accesspoints_headerlink.tpl');

import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');

// @class RequestQuoteAccessPoints.HeaderLink.View @extend Backbone.View
const RequestQuoteAccessPointsHeaderLinkView: any = BackboneView.extend({
    // @property {Function} template
    template: requestquote_accesspoints_headerlink_tpl,

    // @method getContext
    // @return {RequestQuoteAccessPoints.HeaderLink.View.Context}
    getContext: function() {
        // @class RequestQuoteAccessPoints.HeaderLink.View.Context
        return {
            // @property {Boolean} hasClass
            hasClass: !!this.options.className,
            // @property {String} className
            className: this.options.className,
            // @property {Boolean} showTitle
            showTitle: Configuration.get('quote.showHyperlink'),
            // @property {String} title
            title: Configuration.get('quote.textHyperlink')
        };
        // @class RequestQuoteAccessPoints.HeaderLink.View
    }
});

export = RequestQuoteAccessPointsHeaderLinkView;
