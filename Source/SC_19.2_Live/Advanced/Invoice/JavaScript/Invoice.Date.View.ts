/// <amd-module name="Invoice.Date.View"/>

import invoice_date_tpl = require('../Templates/invoice_date.tpl');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');

// @class Invoice.Date.View @extends Backbone.View
const InvoiceDateView: any = BackboneView.extend({
    // @property {Function} template
    template: invoice_date_tpl,

    // @method getContext @returns {Invoice.Date.View.Context}
    getContext: function() {
        const payment = this.model.get('payment');
        // @class Invoice.Date.View.Context
        return {
            // @property {String} dueDate
            dueDate: this.model.get('dueDate') || '',
            // @property {Boolean} showOverdueFlag
            showOverdueFlag: !!this.model.get('isOverdue'),
            // @property {Boolean} showPartiallyPaid
            showPartiallyPaid: !!this.model.get('isPartiallyPaid'),
            // @property {Boolean} showUnapprovedPayment
            showUnapprovedPayment: payment && payment.status === 'unapprovedPayment'
        };
    }
});

export = InvoiceDateView;
