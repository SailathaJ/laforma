/// <amd-module name="StoreLocatorAccessPoints.HeaderLink.View"/>

import ReferenceConfiguration = require('../../StoreLocatorReferenceMapImplementation/JavaScript/ReferenceMap.Configuration');
import storelocator_accesspoints_headerlink_tpl = require('../Templates/storelocator_accesspoints_headerlink.tpl');

import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import Configuration = require('../../SCA/JavaScript/SC.Configuration');

// @class StoreLocatorAccessPoints.HeaderLink.View @extend Backbone.View
const StoreLocatorAccessPointsHeaderLinkView: any = BackboneView.extend({
    // @property {Function} template
    template: storelocator_accesspoints_headerlink_tpl,

    // @method getContext
    // @return {StoreLocatorAccessPoints.HeaderLink.View.Context}
    getContext: function() {
        // @class StoreLocatorAccessPoints.HeaderLink.View.Context
        return {
            // @Property {String} title
            title: ReferenceConfiguration.title(),
            // @property {Boolean} hasClass
            hasClass: !!this.options.className,
            // @property {String} className
            className: this.options.className,
            // @property {String} touchpoint
            touchpoint: Configuration.get('siteSettings.isHttpsSupported') ? 'home' : 'storelocator'
        };
        // @class StoreLocatorAccessPoints.HeaderLink.View
    }
});

export = StoreLocatorAccessPointsHeaderLinkView;
