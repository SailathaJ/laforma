/// <amd-module name="SC.CCT.Html"/>

import SCCCTHtmlView = require('./SC.CCT.Html.View');

// @module SC.CCT.Html
export = {
    mountToApp: function mountToApp(application) {
        const component = application.getComponent('CMS');

        if (!component) {
            return;
        }

        component.registerCustomContentType({
            id: 'CMS',
            view: SCCCTHtmlView
        });
    }
};
