/// <amd-module name="SC.CCT.Html.View"/>

import CustomContentTypeBaseView = require('../../CustomContentType/JavaScript/CustomContentType.Base.View');
import sc_cct_html_tpl = require('../Templates/sc_cct_html.tpl');

// @module SC.CCT.Html
// @class SC.CCT.Html.View @extend CustomContentType.Base.View
export = CustomContentTypeBaseView.extend({
    template: sc_cct_html_tpl,

    getContext: function getContext() {
        // @class SC.CCT.Html.View.Context
        return {
            // @property {String} htmlString
            htmlString: this.settings.html_string,
            // @property {Boolean} hasHtmlString
            hasHtmlString: !!this.settings.html_string
        };
        // @class SC.CCT.Html.View
    }
});
