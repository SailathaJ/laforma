if(typeof SCM !== 'undefined')
{
	Object.keys(SCM).forEach(function(module_name)
	{
		define(module_name, function()
		{
			return SCM[module_name];
		});
	});
}