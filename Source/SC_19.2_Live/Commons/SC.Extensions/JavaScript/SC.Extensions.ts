/// <amd-module name="SC.Extensions"/>
/// <reference path="../../Utilities/JavaScript/GlobalDeclarations.d.ts" />

import * as _ from 'underscore';

import jQuery = require('../../Utilities/JavaScript/jQuery');

let ext_promise = jQuery.Deferred().resolve();
if (SC && SC.extensionModules) {
    ext_promise = jQuery.when.apply(
        jQuery,
        _.map(SC.extensionModules, function(appModuleName) {
            const promise = jQuery.Deferred();
            try {
                require([appModuleName], promise.resolve, function(error) {
                    console.error(error);
                    promise.resolve();
                });
            } catch (error) {
                console.error(error);
                promise.resolve();
            }
            return promise;
        })
    );
}
export = ext_promise;
