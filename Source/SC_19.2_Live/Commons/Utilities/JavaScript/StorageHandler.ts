/// <amd-module name="StorageHandler"/>
const StorageHandler: any = {
    // @method setLocalStorage
    // Set/update a value for the "key" in local storage
    // @param {Object} value Value of the attribute "key" in local storage
    // @param {Object} key Attribute name of a key in local storage
    set: function(key, value) {
        value = value && JSON.stringify(value);
        localStorage.setItem(key, value);
    },

    // @method getFromLocalStorage
    // Get the value of key in local storage
    // @param {Object} key Attribute name of a key in local storage
    // @return {Object} Data retrieved form localstorage parsed in JSON format
    get: function(key) {
        const data = localStorage.getItem(key);
        return data && JSON.parse(data);
    },

    remove: function(key) {
        localStorage.removeItem(key);
    }
};
export = StorageHandler;
