/// <amd-module name="Utilities.Interfaces"/>

export interface UrlParams {
    param: string;
    value: string;
}
