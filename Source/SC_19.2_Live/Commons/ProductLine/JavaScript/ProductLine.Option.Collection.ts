/// <amd-module name="ProductLine.Option.Collection"/>

import Backbone = require('../../Utilities/JavaScript/backbone.custom');
import ProductLineOptionModel = require('./ProductLine.Option.Model');

// @class ProdctLine.Option.Collection Base collection class used to manage the common parts of Product, Item and Transaction.Line options
// @extend Backbone.Collection
export = Backbone.Collection.extend({
    // @property {ProdctLine.Option.Model}
    model: ProductLineOptionModel
});
