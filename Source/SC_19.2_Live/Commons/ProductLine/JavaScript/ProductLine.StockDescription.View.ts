/// <amd-module name="ProductLine.StockDescription.View"/>

import product_line_stock_description_tpl = require('../Templates/product_line_stock_description.tpl');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');

// @class ProductLine.StockDescription.View @extends Backbone.View
export = BackboneView.extend({
    template: product_line_stock_description_tpl,

    // @method initialize Override default method to attach model's change event to re-render
    initialize: function() {
        this.model.on('change', this.render, this);
    },

    // @method getContext
    // @return {ProductLine.StockDescription.View.Context}
    getContext: function() {
        this.stock_info = this.model.getStockInfo();

        // @class ProductLine.Stock.View.Context
        return {
            // @property {Boolean} showStockDescription
            showStockDescription: !!(
                this.stock_info.showStockDescription && this.stock_info.stockDescription
            ),
            // @property {Item.Model.StockInfo} stockInfo
            stockInfo: this.stock_info
        };
        // @class ProductLine.StockDescription.View
    }
});

// @class ProductLine.StockDescription.View.Initialize.options
