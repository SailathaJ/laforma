/// <amd-module name="ItemRelations" />

import ItemRelationsRelatedView = require('./ItemRelations.Related.View');
import ItemRelationsCorrelatedView = require('./ItemRelations.Correlated.View');
import CartDetailedView = require('../../Cart/JavaScript/Cart.Detailed.View');

const ItemRelations = {
    // @method mountToApp
    // @param {ApplicationSkeleton} application
    // @return {Void}
    mountToApp: function(application) {
        CartDetailedView.addChildViews({
            'Correlated.Items': function wrapperFunction(options) {
                return function() {
                    return new ItemRelationsCorrelatedView({
                        itemsIds: options.model.getItemsIds(),
                        application: application
                    });
                };
            },
            'Related.Items': function wrapperFunction(options) {
                return function() {
                    return new ItemRelationsRelatedView({
                        itemsIds: options.model.getItemsIds(),
                        application: application
                    });
                };
            }
        });
    }
};

export = ItemRelations;
