/// <amd-module name="Notifications.View"/>
// @module Notifications
import NotificationsOrderView = require('./Notifications.Order.View');
import NotificationsProfileView = require('./Notifications.Profile.View');
import notifications_tpl = require('../Templates/notifications.tpl');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');

// @class Notifications.View @extends Backbone.View
const NotificationsView: any = BackboneView.extend({
    template: notifications_tpl,

    initialize: function() {},

    // @property {ChildViews} childViews
    childViews: {
        'Order.Notifications': function() {
            return new NotificationsOrderView();
        },
        'Profile.Notifications': function() {
            return new NotificationsProfileView();
        }
    },

    // @method getContext @return Notifications.View.Context
    getContext: function() {
        // @class Notifications.View.Context
        return {};
    }
});

export = NotificationsView;
