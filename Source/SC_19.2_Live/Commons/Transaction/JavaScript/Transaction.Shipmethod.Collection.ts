/// <amd-module name="Transaction.Shipmethod.Collection"/>

import TransactionShipmethodModel = require('./Transaction.Shipmethod.Model');
import Backbone = require('../../Utilities/JavaScript/backbone.custom');

const TransactionShipmethodCollection: any = Backbone.Collection.extend({
    // @property {Transaction.Shipmethod.Model} model
    model: TransactionShipmethodModel,

    // @property {String} comparator
    comparator: 'name'
});

export = TransactionShipmethodCollection;
