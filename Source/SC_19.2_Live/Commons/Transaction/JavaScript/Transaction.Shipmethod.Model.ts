/// <amd-module name="Transaction.Shipmethod.Model"/>

import Backbone = require('../../Utilities/JavaScript/backbone.custom');

const TransactionShipmethodModel: any = Backbone.Model.extend({
    // @method getFormattedShipmethod
    // @return {String}
    getFormattedShipmethod: function() {
        return this.get('name');
    }
});

export = TransactionShipmethodModel;
