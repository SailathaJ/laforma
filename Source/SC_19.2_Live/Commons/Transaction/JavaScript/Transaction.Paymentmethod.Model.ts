/// <amd-module name="Transaction.Paymentmethod.Model"/>

import Backbone = require('../../Utilities/JavaScript/backbone.custom');

const TransactionPaymentmethodModel: any = Backbone.Model.extend({
    // @method getFormattedPaymentmethod Returns the current model's type
    // @return {String}
    getFormattedPaymentmethod: function() {
        return this.get('type');
    }
});

export = TransactionPaymentmethodModel;
