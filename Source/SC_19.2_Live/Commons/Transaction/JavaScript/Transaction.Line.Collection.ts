/// <amd-module name="Transaction.Line.Collection"/>

import TransactionLineModel = require('./Transaction.Line.Model');
import Backbone = require('../../Utilities/JavaScript/backbone.custom');

const TransactionLineCollection: any = Backbone.Collection.extend({
    // @property {Transaction.Line.Model} model
    model: TransactionLineModel
});

export = TransactionLineCollection;
