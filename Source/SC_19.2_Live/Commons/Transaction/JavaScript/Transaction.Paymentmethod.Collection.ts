/// <amd-module name="Transaction.Paymentmethod.Collection"/>

import TransactionPaymentmethodModel = require('./Transaction.Paymentmethod.Model');
import Backbone = require('../../Utilities/JavaScript/backbone.custom');

const TransactionPaymentmethodCollection: any = Backbone.Collection.extend({
    // @property {Transaction.Paymentmethod.Model} model
    model: TransactionPaymentmethodModel
});

export = TransactionPaymentmethodCollection;
