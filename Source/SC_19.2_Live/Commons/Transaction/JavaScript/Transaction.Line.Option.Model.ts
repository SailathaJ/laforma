/// <amd-module name="Transaction.Line.Option.Model"/>

import ProductLineOptionModel = require('../../ProductLine/JavaScript/ProductLine.Option.Model');

const TransactionLineOptionModel: any = ProductLineOptionModel.extend({});

export = TransactionLineOptionModel;
