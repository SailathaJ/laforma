/// <amd-module name="Transaction.Line.Option.Collection"/>

import TransactionLineOptionModel = require('./Transaction.Line.Option.Model');
import ProductLineOptionCollection = require('../../ProductLine/JavaScript/ProductLine.Option.Collection');

const TransactionLineOptionCollection: any = ProductLineOptionCollection.extend({
    // @property {Transaction.Line.Model} model
    model: TransactionLineOptionModel
});

export = TransactionLineOptionCollection;
