/// <amd-module name="Facets.Empty.View"/>

import facets_empty_tpl = require('../Templates/facets_empty.tpl');

import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');

// @module Facets
// @class Facets.Empty.View @extends Backbone.View
export = BackboneView.extend({
    template: facets_empty_tpl,

    // @method getContext @returns {Facets.Empty.View.Context}
    getContext: function() {
        // @class Facets.Empty.View.Context
        return {
            // @property {String} keywords
            keywords: this.options.keywords
        };
        // @classFacets.Empty.View.View
    }
});
