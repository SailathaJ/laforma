/// <amd-module name="Facets.Collection"/>

import FacetsModel = require('./Facets.Model');
import ProfileModel = require('../../../Commons/Profile/JavaScript/Profile.Model');

import BackboneCachedCollection = require('../../../Commons/BackboneExtras/JavaScript/Backbone.CachedCollection');

// @module Facets
export = BackboneCachedCollection.extend({
    url: ProfileModel.getInstance().getSearchApiUrl(),

    model: FacetsModel,

    parse: function(response) {
        return [response];
    }
});
