/// <amd-module name="Facets.Browse.CategoryHeading.View"/>

import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import CategoriesUtils = require('../../../Commons/Categories/JavaScript/Categories.Utils');

import facetsBrowseCategoryHeadingTpl = require('../Templates/facets_browse_category_heading.tpl');

export = BackboneView.extend({
    template: facetsBrowseCategoryHeadingTpl,

    getContext: function() {
        const additionalFields = CategoriesUtils.getAdditionalFields(
            this.model.attributes,
            'categories.category.fields'
        );

        return {
            // @property {String} name
            name: this.model.get('name'),
            // @property {String} banner
            banner: this.model.get('pagebannerurl'),
            // @property {String} description
            description: this.model.get('description'),
            // @property {String} pageheading
            pageheading: this.model.get('pageheading') || this.model.get('name'),
            // @property {Boolean} hasBanner
            hasBanner: !!this.model.get('pagebannerurl'),
            // @property {Object} additionalFields
            additionalFields: additionalFields,
            // @property {Boolean} showDescription
            showDescription: !!this.options.showDescription
        };
    }
});
