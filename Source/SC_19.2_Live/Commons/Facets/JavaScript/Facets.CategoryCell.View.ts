/// <amd-module name="Facets.CategoryCell.View"/>

import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import CategoriesUtils = require('../../../Commons/Categories/JavaScript/Categories.Utils');

import facets_category_cell_tpl = require('../Templates/facets_category_cell.tpl');

export = BackboneView.extend({
    template: facets_category_cell_tpl,

    // @method getContext @return Facets.CategoryCell.View.Context
    getContext: function() {
        const additionalFields = CategoriesUtils.getAdditionalFields(
            this.model.attributes,
            'categories.subCategories.fields'
        );

        return {
            // @property {String} label
            name: this.model.get('name'),
            // @property {String} name
            url: this.model.get('fullurl'),
            // @property {String} image
            image: this.model.get('thumbnailurl'),
            // @property {Boolean} hasImage
            hasImage: !!this.model.get('thumbnailurl'),
            // @property {Object} additionalFields
            additionalFields: additionalFields
        };
    }
});
