/// <amd-module name="Facets.Helper"/>

import Translator = require('./Facets.Translator');

// @module Facets
export = {
    settings_stack: [],

    // @method parseUrl Returns a Facet.Translator for the passed url and configuration @static
    parseUrl: function(fullurl, configuration, isCategoryPage?: any) {
        return new Translator(fullurl, null, configuration, isCategoryPage);
    },

    // @method setCurrent @param {Object} settings @static
    setCurrent: function(settings) {
        this.settings_stack.push(settings);
    },

    // @method getCurrent @returns {Object} @static
    getCurrent: function() {
        return this.settings_stack[this.settings_stack.length - 1];
    },

    // @method getPrevious @returns {Object} @static
    getPrevious: function() {
        return this.settings_stack[this.settings_stack.length - 2];
    }
};
