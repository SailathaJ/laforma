/// <amd-module name="Facets.ItemListSummary.View"/>

import facets_item_list_summary_tpl = require('../Templates/facets_item_list_summary.tpl');

import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');

// @module Facets
export = BackboneView.extend({
    template: facets_item_list_summary_tpl,

    // @method getContext @returns {Facets.ItemListSummary.View.Context}
    getContext: function() {
        const { configuration } = this.options;
        const range_end = configuration.currentPage * configuration.itemPerPage;
        const total = configuration.totalItems;

        // @class Facets.ItemListSummary.View.Context
        return {
            // @property {Number} rangeEnd
            rangeEnd: Math.min(range_end, total),

            // @property {Number} rangeStart
            rangeStart: range_end - configuration.itemsPerPage + 1,

            // @property {total} rangeStart
            total: total
        };
    }
});
