// PageType.ServiceController.js
// ----------------
// Service to get CMS PageTypes
define('PageType.ServiceController', ['ServiceController', 'PageType.Model'], function(
    ServiceController,
    PageTypeModel
) {
    return ServiceController.extend({
        name: 'PageType.ServiceController',

        get: function() {
            return PageTypeModel.getPageTypes();
        }
    });
});
