/// <amd-module name="PageType.Component"/>

import * as _ from 'underscore';

import { Loggers } from '../../Loggers/JavaScript/Loggers';
import { PageTypeCollection } from './PageType.Collection';

import SCBaseComponent = require('../../SC/JavaScript/SC.BaseComponent');
import PageTypeRouter = require('./PageType.Router');
import Configuration = require('../../Utilities/JavaScript/SC.Configuration');
import Utils = require('../../Utilities/JavaScript/Utils');

import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');
import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');
// import { PageTypeModel } from './PageType.Model';

// @module PageType.Component
/* interface PageType {
	name: string
	displayName: string
	template: string
}*/
interface PageContext {
    context: any;
    data: { promiseCMS: any; rendered: boolean; timeoutId: number };
}

export = function PageTypeComponentGenerator(application) {
    const Router = new PageTypeRouter(application);
    const CMSReady = jQuery.Deferred();
    let CMS;
    const routes = {};
    const pages = {};
    const allPageTypes = {};

    const templates = {};
    const types = {};
    let lastContext: PageContext = {
        context: {},
        data: {
            promiseCMS: jQuery.Deferred(),
            rendered: false,
            timeoutId: 0
        }
    };

    Backbone.on('cms:loaded', function(cms) {
        CMS = cms;
        CMSReady.resolve();
    });

    CMSReady.done(function() {
        CMS.on('page:content:set', function(_promise, _ccts, contentContext) {
            const fullContext = PageTypeComponent._validateCurrentContext(contentContext);

            if (fullContext) {
                var { data } = fullContext;

                if (data.rendered) {
                    const cctComponent = PageTypeComponent.application.getComponent('CMS');

                    cctComponent.addContents();
                } else if (data.timeoutId) {
                    clearTimeout(data.timeoutId);
                }

                data.promiseCMS.resolve();
            } else {
                data.promiseCMS.reject();
            }
        });
    });

    var PageTypeComponent = SCBaseComponent.extend({
        componentName: 'PageType',

        application: application,

        pageTypes: new PageTypeCollection(
            (SC.ENVIRONMENT.PageTypes && SC.ENVIRONMENT.PageTypes.pageTypes) || []
        ),

        _validateCurrentContext: function _validateCurrentContext(context) {
            const contextLast: any = lastContext.context;

            if (contextLast.path === context.path) {
                return lastContext;
            }
            return null;
        },

        _getPage: function _getPage(url) {
            return pages[url];
        },

        _addPage: function _addPage(page, type, url) {
            if (type && url) {
                if (!routes[type]) {
                    routes[type] = [];
                }

                if (_.indexOf(routes[type], url) === -1) {
                    routes[type].push(url);
                }

                pages[url] = page;
            }
        },

        _addRoute: function _addRoute(options) {
            _.each(options.routes, function(route: string) {
                if (route[0] === '/') {
                    route = route.substr(1);
                }

                Router.route(route, options.type);
            });
        },
        _getPageType: function _getPageType(name) {
            return this.pageTypes.find(pageType => pageType.get('name') === name);
        },

        _updatePageTypes: function _updatePageTypes() {
            this.pageTypes.fetch().done(() => {
                const { context } = lastContext;
                Backbone.history.navigate(context.path, { trigger: false });
                Backbone.history.loadUrl(context.path);
            });
        },

        getPageTypes: function getPageTypes() {
            const data = {
                layouts: templates,
                types: {}
            };

            _.each(types, function(value: any, key) {
                if (value.registered) {
                    data.types[key] = value;
                }
            });

            return data;
        },

        getContext: function getContext() {
            return Utils.deepCopy(lastContext.context);
        },

        registerTemplate: function registerTemplate(templateData) {
            const { template } = templateData;

            if (!templates[template.name]) {
                templates[template.name] = {
                    name: template.displayName,
                    thumbnail: template.thumbnail
                };
            }

            _.each(templateData.pageTypes, function(pageType: any) {
                if (!types[pageType]) {
                    types[pageType] = {
                        supportedLayoutIds: []
                    };
                }

                if (_.indexOf(types[pageType].supportedLayoutIds, template.name) === -1) {
                    types[pageType].supportedLayoutIds.push(template.name);
                }
            });
        },

        _CmsViewPromises: function _CmsViewPromises(options) {
            const url =
                (Backbone.history.fragment && Backbone.history.fragment.split('?')[0]) ||
                Backbone.history.location.hash;
            const path = Utils.correctURL(url);

            const context = {
                path: options.path || path,
                site_id: options.site_id || Configuration.get('siteSettings.siteid'),
                page_type: options.page_type || '',
                page_type_display_name: options.page_type || ''
            };
            const pageTypeInfo = this._getPageType(options.page_type);

            if (pageTypeInfo) {
                context.page_type_display_name = pageTypeInfo.get('displayName');
            }

            const data = {
                promiseCMS: jQuery.Deferred(),
                rendered: false,
                timeoutId: 0
            };

            lastContext = {
                context: context,
                data: data
            };

            const self = this;
            const { view } = options;

            const promise = jQuery.Deferred();

            let promiseView = view ? jQuery.Deferred().resolve() : jQuery.Deferred().reject();

            if (view && view.beforeShowContent) {
                promiseView = view.beforeShowContent();
            }

            if (!Configuration.get('cms.useCMS')) {
                data.promiseCMS.resolve();
            } else {
                CMSReady.done(function() {
                    self.application.getLayout().once('beforeAppendView', function() {
                        if (self._validateCurrentContext(context) && !data.rendered) {
                            const cctComponent = self.application.getComponent('CMS');

                            cctComponent.addContents();
                        }
                    });

                    CMS.once('context:get', function(
                        promise // TODO: remove this event, is for test only
                    ) {
                        if (self._validateCurrentContext(context)) {
                            promise.resolve(context);
                        }
                    });

                    CMS.trigger('app:page:changed'); // Add context again as parameter and remove 'context:get' when issue is fixed

                    promiseView.done(function() {
                        data.timeoutId = setTimeout(function() {
                            if (self._validateCurrentContext(context)) {
                                data.rendered = true;
                                data.promiseCMS.resolve();
                            } else {
                                data.promiseCMS.reject();
                            }
                        }, Configuration.get('cms.contentWait', 200));
                    });
                });
            }

            jQuery
                .when(data.promiseCMS, promiseView)
                .done(function() {
                    promise.resolve();
                })
                .fail(function(promiseError) {
                    promise.reject(promiseError);
                });

            return promise;
        },

        registerPageType: function registerPageType(pagetype) {
            if (pagetype.name && pagetype.view) {
                allPageTypes[pagetype.name] = pagetype;
            }

            if (pagetype.name && pagetype.routes && !pagetype.view && allPageTypes[pagetype.name]) {
                pagetype.view = allPageTypes[pagetype.name].view;
            }

            const callback = function() {
                const loggers = Loggers.getLogger();
                loggers.start('Navigation');

                let view;

                if (pagetype.view) {
                    let url = Backbone.history.fragment;
                    url = url.split('?')[0]; // remove options
                    const page = pages[url];

                    const options: any = _.extend({}, pagetype.options || {}, {
                        application: application,
                        container: application,
                        routerArguments: arguments
                    });

                    if (page) {
                        options.pageInfo = page;
                    }

                    view = new pagetype.view(options);
                } else if (pagetype.callback) {
                    view = pagetype.callback.apply(arguments);
                }

                const data = {
                    view: view,
                    page_type: pagetype.name
                };

                PageTypeComponent._CmsViewPromises(data).done(function() {
                    view._pagetype = true;

                    const showContentPromise = view.showContent();

                    showContentPromise &&
                        showContentPromise.done(function() {
                            loggers.endLast('Navigation');

                            if (Configuration.get('cms.useCMS')) {
                                CMS.trigger('app:page:rendered');
                            }
                        });
                });
            };

            if (pagetype.routes) {
                _.each(pagetype.routes, function(route: string) {
                    if (route[0] === '/') {
                        route = route.substr(1);
                    }

                    Router.route(route, callback);
                });
            } else {
                Router[pagetype.name] = callback;

                if (routes[pagetype.name]) {
                    let route: string;

                    while ((route = routes[pagetype.name].pop())) {
                        if (route[0] === '/') {
                            route = route.substr(1);
                        }

                        Router.route(route, pagetype.name);
                    }
                }
            }

            if (!types[pagetype.name]) {
                types[pagetype.name] = {
                    supportedLayoutIds: []
                };
            }

            types[pagetype.name].registered = true;

            if (pagetype.defaultTemplate) {
                templates[pagetype.defaultTemplate.name] = {
                    name: pagetype.defaultTemplate.displayName,
                    thumbnail: pagetype.defaultTemplate.thumbnail
                };

                types[pagetype.name].defaultLayoutId = pagetype.defaultTemplate.name;
                types[pagetype.name].supportedLayoutIds.push(pagetype.defaultTemplate.name);
            }
        }
    });

    return PageTypeComponent;
};
