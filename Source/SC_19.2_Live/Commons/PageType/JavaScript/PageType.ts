/// <amd-module name="PageType"/>

import 'PageType.Base.View';

import PageTypeComponent = require('./PageType.Component');

// @module PageType
// @class PageType @extends ApplicationModule
export = {
    mountToApp: function(application) {
        return PageTypeComponent(application);
    }
};
