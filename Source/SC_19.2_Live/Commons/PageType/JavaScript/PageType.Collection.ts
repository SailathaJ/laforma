/// <amd-module name="PageType.Collection"/>

import { PageTypeModel } from './PageType.Model';

import Utils = require('../../Utilities/JavaScript/Utils');
import Backbone = require('../../Utilities/JavaScript/backbone.custom');

export const PageTypeCollection = Backbone.Collection.extend({
    cacheSupport: false,
    url: Utils.getAbsoluteUrl('services/PageType.Service.ss'),
    model: PageTypeModel,
    parse: function(data) {
        return data.pageTypes;
    }
});
