/// <amd-module name="PageType.Router"/>

import Backbone = require('../../Utilities/JavaScript/backbone.custom');

// @module Home
// @lass Home.Router @extends Backbone.Router
export = Backbone.Router.extend({
    initialize: function(application) {
        this.application = application;
    }
});
