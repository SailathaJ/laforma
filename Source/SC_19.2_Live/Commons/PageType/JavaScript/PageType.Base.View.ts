/// <amd-module name="PageType.Base.View"/>

import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

// @module PageType

// @class PageType.Base.View @extends Backbone.View
// Base PageType class from where all the PageType Views extend from
export = {
    PageTypeBaseView: BackboneView.extend({
        constructor: function initialize(options) {
            // adapt to a acceptable format for extensibility layer
            options.pageInfo = {
                name: options.pageInfo.get('name'),
                url: options.pageInfo.get('urlPath'),
                header: options.pageInfo.get('page_header'),
                title: options.pageInfo.get('page_title'),
                fields: options.pageInfo.get('fields')
            };
            BackboneView.apply(this, arguments);
            // this method is needed by core to render proper breadcrumb, it should not be overwritten
            this.getBreadcrumbPages = function getBreadcrumbPages() {
                const { pageInfo } = options;
                const { url } = pageInfo;
                const path = Utils.correctURL(url);

                return [{ href: path, text: pageInfo.title || pageInfo.header }];
            };
        },
        // @method beforeShowContent
        // The method 'showContent' will be executed only after the returned promise is resolved
        // @return {jQuery.Deferred}
        beforeShowContent: function beforeShowContent() {
            return jQuery.Deferred().resolve();
        }
    })
};
