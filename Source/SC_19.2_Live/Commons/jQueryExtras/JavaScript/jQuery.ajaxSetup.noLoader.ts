/// <amd-module name="jQuery.ajaxSetup.noLoader"/>
/// <reference path="../../Utilities/JavaScript/GlobalDeclarations.d.ts"/>

// jQuery.ajaxSetup.js
// -------------------
// Adds the loading icon, updates icon's placement on mousemove
// Changes jQuery's ajax setup defaults

import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');
import Utils = require('../../Utilities/JavaScript/Utils');

// http://api.jquery.com/jQuery.ajaxSetup/
jQuery.ajaxSetup({
    beforeSend: function(jqXhr, options) {
        // BTW: "!~" means "== -1"
        if (!~(<any>options.contentType).indexOf('charset')) {
            // If there's no charset, we set it to UTF-8
            jqXhr.setRequestHeader('Content-Type', options.contentType + '; charset=UTF-8');
        }

        // Add header so that suitescript code can know the current touchpoint
        jqXhr.setRequestHeader('X-SC-Touchpoint', SC.ENVIRONMENT.SCTouchpoint);
    }
});
