/*
@module jQueryExtras
#jQuery.scSeeMoreLess
A jQuery plugin for toggle collapse. Usage: 

	this.$el.find('[data-action="tab-content"]').scSeeMoreLess()
*/

define('jQuery.scSeeMoreLess', ['jQuery'], function(jQuery) {
    jQuery.fn.scSeeMoreLess = function(options) {
        const settings = jQuery.extend({}, jQuery.fn.push.defaults, options);

        return this.each(function() {
            const self = this;
            const $this = jQuery(this);
            const tabHeight = $this.height();
            const maxHeight = 400;
            if (tabHeight >= maxHeight) {
                $this.addClass('colapsed');
            }
        });
    };

    jQuery.fn.push.defaults = {};
});
