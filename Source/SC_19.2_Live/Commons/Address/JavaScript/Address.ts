/// <amd-module name="Address"/>

// Address.js
// -----------------
// Defines the Address  module (Model, Collection, Views, Router)

import Utils = require('../../Utilities/JavaScript/Utils');
import AddressEditView = require('./Address.Edit.View');
import AddressListView = require('./Address.List.View');

const Address: any = {
    mountToApp: function(application: any) {
        const pageType = application.getComponent('PageType');
        pageType.registerPageType({
            name: 'AddressBook',
            routes: ['addressbook'],
            view: AddressListView,
            defaultTemplate: {
                name: 'address_list.tpl',
                displayName: 'Address Book Default',
                thumbnail: Utils.getThemeAbsoluteUrlOfNonManagedResources('img/default-layout-address-book.png')
            }
        });

        pageType.registerPageType({
            name: 'AddressEdit',
            routes: ['addressbook/:id'],
            view: AddressEditView,
            defaultTemplate: {
                name: 'address_edit.tpl',
                displayName: 'Address Edit Default',
                thumbnail: Utils.getThemeAbsoluteUrlOfNonManagedResources('img/default-layout-address-book-edit.png')
            }
        });
    }
};

export = Address;
