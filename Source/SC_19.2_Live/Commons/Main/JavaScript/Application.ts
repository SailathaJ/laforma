/// <amd-module name="Application"/>
/// <reference path="../../../Commons/Utilities/JavaScript/GlobalDeclarations.d.ts" />
import ApplicationSkeleton = require('../../ApplicationSkeleton/JavaScript/ApplicationSkeleton');
// Application Creation:
// Applications will provide by default: Layout (So your views can talk to)
// and a Router (so you can extend them with some nice defaults)
// If you like to create extensions to the Skeleton you should extend SC.ApplicationSkeleton
SC._applications = {};

SC.Application = function(application_name) {
    SC._applications[application_name] =
        SC._applications[application_name] || new ApplicationSkeleton(application_name);
    return SC._applications[application_name];
};
export = SC.Application;
