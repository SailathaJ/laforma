define('Loggers.ElasticLogger', ['SC.Models.Init', 'ReleaseMetadata', 'Utils', 'underscore'], function(
    ModelsInit,
    ReleaseMetadata,
    Utils,
    _
) {
    const logger = {
        info: [],

        flush: function flush(common_data) {
            try {
                if (!this.info || !this.info.length) {
                    return;
                }

                const product = BuildTimeInf.product.toLowerCase();

                const url = nlapiResolveURL(
                    'SUITELET',
                    'customscript_' + product + '_loggerendpoint',
                    'customdeploy_' + product + '_loggerendpoint',
                    'external'
                );

                common_data = common_data || {};
                common_data.url = Utils.getShoppingDomain();

                const data = {
                    type: product.toUpperCase(),
                    info: this._mergeData(this.info, common_data),
                    error: null
                };

                nlapiRequestURL(
                    url,
                    JSON.stringify(data),
                    { 'Content-Type': 'application/json' },
                    'POST'
                );
            } catch (error) {
                console.log(JSON.stringify(error));
            }

            this.info = [];
        },

        log: function log(info) {
            const product = BuildTimeInf.product.toLowerCase();
            if (product === 'scis' || !info.method_name) {
                return;
            }

            this.info.push({
                suiteScriptAppVersion: ReleaseMetadata.getVersion(),
                extensibilityLayerMethodName: info.method_name,
                componentArea: info.componentArea
            });
        },

        _mergeData: function(data, common_data) {
            if (!common_data) {
                return data;
            }

            const common_data_map = {
                url: { key: 'clientRequestURL', regex: '^https?://([^/]*).*$', replace: '$1' }
            };

            return _.map(data, function(info) {
                _.each(common_data, function(value, key) {
                    const mapped_key = common_data_map[key];
                    if (mapped_key) {
                        if (mapped_key.regex && mapped_key.replace) {
                            value = value.replace(new RegExp(mapped_key.regex), mapped_key.replace);
                        }
                        info[mapped_key.key] = value;
                    }
                });

                return info;
            });
        }
    };

    return logger;
});
