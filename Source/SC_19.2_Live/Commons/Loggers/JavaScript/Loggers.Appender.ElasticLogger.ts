/// <amd-module name="Loggers.Appender.ElasticLogger"/>
/// <reference path="../../Utilities/JavaScript/GlobalDeclarations.d.ts" />

import { LoggersAppender } from './Loggers.Appender';

import Url = require('../../Utilities/JavaScript/Url');
import jQuery = require('../../Utilities/JavaScript/jQuery');

export class LoggersAppenderElasticLogger implements LoggersAppender {
    private static instance: LoggersAppenderElasticLogger;

    private queueErrorTemp = [];

    private queueInfoTemp = [];

    private isWaiting = false;

    private enabled = !SC.isPageGenerator();

    private clearQueues() {
        localStorage.setItem('queueError', JSON.stringify(this.queueErrorTemp));
        localStorage.setItem('queueInfo', JSON.stringify(this.queueInfoTemp));
        this.queueErrorTemp = [];
        this.queueInfoTemp = [];
        this.isWaiting = false;
    }

    private appendTemp() {
        if (this.queueErrorTemp.length > 0) {
            const stringQueueError = localStorage.getItem('queueError');
            const queueError = stringQueueError == null ? [] : JSON.parse(stringQueueError);
            localStorage.setItem(
                'queueError',
                JSON.stringify(queueError.concat(this.queueErrorTemp))
            );
        }
        if (this.queueInfoTemp.length > 0) {
            const stringQueueInfo = localStorage.getItem('queueInfo');
            const queueInfo = stringQueueInfo == null ? [] : JSON.parse(stringQueueInfo);
            localStorage.setItem('queueInfo', JSON.stringify(queueInfo.concat(this.queueInfoTemp)));
        }
        this.isWaiting = false;
    }

    private sendQueues(isAsync: boolean) {
        const parsedURL = new Url().parse(SC.ENVIRONMENT.baseUrl);
        const { product } = SC.ENVIRONMENT.BuildTimeInf;
        const relativeURL =
            '/app/site/hosting/scriptlet.nl?script=customscript_' +
            product.toLowerCase() +
            '_loggerendpoint&deploy=customdeploy_' +
            product.toLowerCase() +
            '_loggerendpoint';
        const URL = parsedURL.schema
            ? parsedURL.schema + '://' + parsedURL.netLoc + relativeURL
            : relativeURL;
        const queueError = JSON.parse(localStorage.getItem('queueError'));
        const queueInfo = JSON.parse(localStorage.getItem('queueInfo'));
        if ((queueInfo && queueInfo.length > 0) || (queueError && queueError.length > 0)) {
            this.isWaiting = true;
            const data = {
                type: product,
                info: queueInfo,
                error: queueError
            };
            if (navigator.sendBeacon) {
                const status = navigator.sendBeacon(URL, JSON.stringify(data));
                if (status) {
                    this.clearQueues();
                } else {
                    this.appendTemp();
                }
            } else {
                jQuery
                    .ajax({
                        type: 'POST',
                        url: URL,
                        data: JSON.stringify(data),
                        contentType: 'text/plain; charset=UTF-8',
                        async: isAsync
                    })
                    .done(() => {
                        this.clearQueues();
                    })
                    .fail(() => {
                        this.appendTemp();
                    });
            }
        }
    }

    protected constructor() {
        if (this.enabled) {
            setInterval(() => {
                this.sendQueues(true);
            }, 60000);

            window.addEventListener('beforeunload', () => {
                this.sendQueues(false);
            });
        }
    }

    ready(): boolean {
        return this.enabled;
    }

    info(data) {
        data.suiteScriptAppVersion = SC.ENVIRONMENT.RELEASE_METADATA.version;
        data.message = 'clientSideLogDateTime: ' + new Date().toISOString();
        if (this.isWaiting) {
            this.queueInfoTemp.push(data);
        } else {
            const queueInfo = JSON.parse(localStorage.getItem('queueInfo')) || [];
            queueInfo.push(data);
            localStorage.setItem('queueInfo', JSON.stringify(queueInfo));
        }
    }

    error(data) {
        data.suiteScriptAppVersion = SC.ENVIRONMENT.RELEASE_METADATA.version;
        data.message = 'clientSideLogDateTime: ' + new Date().toISOString();
        if (this.isWaiting) {
            this.queueErrorTemp.push(data);
        } else {
            const queueError = JSON.parse(localStorage.getItem('queueError')) || [];
            queueError.push(data);
            localStorage.setItem('queueError', JSON.stringify(queueError));
        }
    }

    start(_action: string) {
        return {};
    }

    end(_startOptions, _options) {}

    static getInstance() {
        if (!LoggersAppenderElasticLogger.instance) {
            LoggersAppenderElasticLogger.instance = new LoggersAppenderElasticLogger();
        }

        return LoggersAppenderElasticLogger.instance;
    }
}
