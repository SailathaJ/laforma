export interface LoggersAppender {
    ready(): boolean;
    info(data: any);
    error(data: any);
    start(action: string, options: object): object;
    end(startOptions: object, options: object);
}
