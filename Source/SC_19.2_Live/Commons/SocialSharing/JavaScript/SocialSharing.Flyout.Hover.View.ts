/// <amd-module name="SocialSharing.Flyout.Hover.View"/>

import social_sharing_flyout_hover_tpl = require('../Templates/social_sharing_flyout_hover.tpl');
import BackboneView = require('../../BackboneExtras/JavaScript/Backbone.View');

// @class SocialSharing.Flyout.Hover.View @extends Backbone.View
const SocialSharingFlyoutHoverView: any = BackboneView.extend({
    template: social_sharing_flyout_hover_tpl,

    // @method getContext @returns {SocialSharing.Flyout.Hover.View.Context}
    getContext: function() {
        // @class SocialSharing.Flyout.Hover.View.Context
        return {};
    }
});

export = SocialSharingFlyoutHoverView;
