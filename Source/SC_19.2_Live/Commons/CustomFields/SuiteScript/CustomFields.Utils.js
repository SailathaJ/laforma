// @module CustomFields
define('CustomFields.Utils', ['Configuration'], function(Configuration) {
    // @class CustomFields.Utils Define a set of utilities related with custom fields
    return {
        // @method getCustomFieldsIdToBeExposed Get the list of fields id from configuration that should be exposed
        // return {Array} A array with the custom fields id
        getCustomFieldsIdToBeExposed: function(recordType) {
            if (Configuration.get().customFields) {
                return Configuration.get().customFields[recordType] || [];
            }

            return [];
        }
    };
});
