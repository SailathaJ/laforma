// CreditMemo.ServiceController.js
// ----------------
// Service to manage CreditMemo requests
define('CreditMemo.ServiceController', ['ServiceController', 'CreditMemo.Model'], function(
    ServiceController,
    CreditMemoModel
) {
    // @class CreditMemo.ServiceController Manage credit memo requests
    // @extend ServiceController
    return ServiceController.extend({
        // @property {String} name Mandatory for all ssp-libraries model
        name: 'CreditMemo.ServiceController',

        // @property {Service.ValidationOptions} options. All the required validation, permissions, etc.
        // The values in this object are the validation needed for the current service.
        // Can have values for all the request methods ('common' values) and specific for each one.
        options: {
            common: {
                requireLogin: true
            }
        },

        // @method get The call to CreditMemo.Service.ss with http method 'get' is managed by this function
        // @return {CreditMemo.Model}
        get: function() {
            const id = this.request.getParameter('internalid');
            return CreditMemoModel.get('creditmemo', id);
        }
    });
});
