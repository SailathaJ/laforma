/// <amd-module name="Product.Collection"/>

import ProductModel = require('./Product.Model');
import Backbone = require('../../Utilities/JavaScript/backbone.custom');

// @class Product.Collection @extends Backbone.Collection
export = Backbone.Collection.extend({
    // @property {Product.Model} model
    model: ProductModel
});
