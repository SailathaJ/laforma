/// <amd-module name="Product.Option.Collection"/>

import ProductOptionModel = require('./Product.Option.Model');
import ProductLineOptionCollection = require('../../ProductLine/JavaScript/ProductLine.Option.Collection');

// @class Product.Option.Collection @extend ProductLine.Option.Collection
export = ProductLineOptionCollection.extend({
    // @property {Product.Options.Model} model
    model: ProductOptionModel
});
