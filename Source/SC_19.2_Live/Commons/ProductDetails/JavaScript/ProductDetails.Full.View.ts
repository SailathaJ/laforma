/// <amd-module name="ProductDetails.Full.View"/>

import * as _ from 'underscore';

import ProductDetailsBaseView = require('./ProductDetails.Base.View');
import Configuration = require('../../Utilities/JavaScript/SC.Configuration');
import ItemRelationsRelatedView = require('../../ItemRelations/JavaScript/ItemRelations.Related.View');
import ItemRelationsCorrelatedView = require('../../ItemRelations/JavaScript/ItemRelations.Correlated.View');
import ProductDetailsInformationView = require('./ProductDetails.Information.View');
import SocialSharingFlyoutView = require('../../SocialSharing/JavaScript/SocialSharing.Flyout.View');
import product_details_full_tpl = require('../Templates/product_details_full.tpl');

// @class ProductDetails.Full.View Handles the PDP and quick view @extend Backbone.View
const ProductDetailsFullView: any = ProductDetailsBaseView.extend({
    // @property {Function} template
    template: product_details_full_tpl,

    // @property {Object} attributes List of HTML attributes applied by Backbone into the $el
    attributes: {
        id: 'ProductDetails.Full.View',
        class: 'view product-detail',
        'data-root-component-id': 'ProductDetails.Full.View'
    },

    bindings: _.extend({}, ProductDetailsBaseView.prototype.bindings, {}),

    // @method initialize Override default method to update the url on changes in the current product
    // @param {ProductDetails.Full.View.Initialize.Options} options
    // @return {Void}
    initialize: function initialize() {
        ProductDetailsBaseView.prototype.initialize.apply(this, arguments);
        this.model.on('change', this.updateURL, this);
        // Tracker.getInstance().trackProductView(this.model);
    },

    childViews: _.extend({}, ProductDetailsBaseView.prototype.childViews, {
        'Correlated.Items': function() {
            return new ItemRelationsCorrelatedView({
                itemsIds: this.model.get('item').get('internalid'),
                application: this.application
            });
        },
        'Related.Items': function() {
            return new ItemRelationsRelatedView({
                itemsIds: this.model.get('item').get('internalid'),
                application: this.application
            });
        },
        'Product.Information': function() {
            return new ProductDetailsInformationView({
                model: this.model
            });
        },
        'SocialSharing.Flyout': function() {
            return new SocialSharingFlyoutView({});
        }
    }),

    // @method destroy Override default method to detach from change event of the current product
    // @return {Void}
    destroy: function destroy() {
        this.model.off('change');
        return this._destroy();
    },

    // @method showOptionsPusher Override parent method to allow hide/show the option's pusher on mobile depending on the configuration value: ItemOptions.maximumOptionValuesQuantityWithoutPusher
    // @return {Booelan}
    showOptionsPusher: function showOptionsPusher() {
        const options_values_length = _.reduce(
            this.model.getVisibleOptions().map(function(option) {
                if (_.isArray(option.get('values'))) {
                    const invalid_values = _.filter(option.get('values'), function(value: any) {
                        return !value.internalid;
                    });

                    return option.get('values').length - invalid_values.length;
                }
                return 0;
            }),
            function(memo: any, num: any) {
                return memo + num;
            },
            0
        );

        return (
            options_values_length >
            Configuration.get('ItemOptions.maximumOptionValuesQuantityWithoutPusher', 1)
        );
    },

    // @method getContext
    // @return {ProductDetails.Full.View.Context}
    getContext: function getContext() {
        // @class ProductDetails.Full.View.Context @extend ProductDetails.Base.View.Context
        return _.extend(ProductDetailsBaseView.prototype.getContext.apply(this, arguments), {});
        // @class ProductDetails.Full.View
    }
});

export = ProductDetailsFullView;

// @class ProductDetails.Full.View.Initialize.Options @extend ProductDetails.Base.View.Initialize.Options
