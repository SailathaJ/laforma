/// <amd-module name="Categories.Collection"/>

import * as _ from 'underscore';

import Backbone = require('../../Utilities/JavaScript/backbone.custom');
import Utils = require('../../Utilities/JavaScript/Utils');
import Configuration = require('../../Utilities/JavaScript/SC.Configuration');

// @class Categories.Collection @extends Backbone.Collection
const CategoriesCollection = Backbone.Collection.extend({
    url: function() {
        const url = Utils.addParamsToUrl(Utils.getAbsoluteUrl('services/Categories.Service.ss'), {
            menuLevel: Configuration.get('categories.menuLevel')
        });

        return url;
    },

    initialize: function(options) {
        this.options = options;
    }
});

export = CategoriesCollection;
