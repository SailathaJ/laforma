// Categories.ServiceController.js
// ----------------
// Service to manage categories
define('Categories.ServiceController', ['ServiceController', 'Categories.Model', 'Utils', 'Configuration'], function (
    ServiceController,
    CategoriesModel,
    Utils,
    Configuration
) {
    return ServiceController.extend({
        name: 'Categories.ServiceController',

        options: {
            common: {
                requireLoggedInPPS: true
            }
        },

        get: function () {
            const fullurl = this.request.getParameter('fullurl');
            const pcv_all_items = this.request.getParameter('pcv_all_items');

            if (fullurl) {
                return CategoriesModel.get(fullurl, null, null, pcv_all_items, false);
            }
            const menuLevel = this.request.getParameter('menuLevel');

            if (menuLevel) {
                return CategoriesModel.getCategoryTree(menuLevel, null, null, pcv_all_items, false);
            }
        }
    });
});
