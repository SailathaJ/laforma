/// <amd-module name="Quote.ListExpirationDate.View"/>

import quote_list_expiration_date_tpl = require('../Templates/quote_list_expiration_date.tpl');
import BackboneView = require('../../BackboneExtras/JavaScript/Backbone.View');
import Backbone = require('../../Utilities/JavaScript/backbone.custom');

// @class Quote.ListExpirationDate.View @extends Backbone.View
export = BackboneView.extend({
    // @property {Function} template
    template: quote_list_expiration_date_tpl,

    // @method getContext
    // @return {Quote.ListExpirationDate.View.Context}
    getContext: function() {
        // @class Quote.ListExpirationDate.View.Context
        return {
            // @property {Quote.Model} collection
            model: this.model
        };
        // @class Quote.ListExpirationDate.View
    }
});
