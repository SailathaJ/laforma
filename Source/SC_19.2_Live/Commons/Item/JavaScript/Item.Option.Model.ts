/// <amd-module name="Item.Option.Model"/>
import ProductLineOptionModel = require('../../ProductLine/JavaScript/ProductLine.Option.Model');

const ItemOptionModel: any = ProductLineOptionModel.extend({});
export = ItemOptionModel;
