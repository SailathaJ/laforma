/// <amd-module name="Item.Option.Collection"/>
import ItemOptionModel = require('./Item.Option.Model');
import ProductLineOptionCollection = require('../../ProductLine/JavaScript/ProductLine.Option.Collection');

// @class Item.Option.Collection @extend ProductLine.Option.Collection
const ItemOptionCollection: any = ProductLineOptionCollection.extend({
    // @property {Item.Option.Model} model
    model: ItemOptionModel
});
export = ItemOptionCollection;
