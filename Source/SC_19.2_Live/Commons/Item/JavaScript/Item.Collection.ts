/// <amd-module name="Item.Collection"/>
import * as _ from 'underscore';

import BackboneCachedCollection = require('../../BackboneExtras/JavaScript/Backbone.CachedCollection');
import ItemModel = require('./Item.Model');
import Session = require('../../Session/JavaScript/Session');
import ProfileModel = require('../../Profile/JavaScript/Profile.Model');
import Utils = require('../../Utilities/JavaScript/Utils');

const ItemCollection: any = BackboneCachedCollection.extend({
    url: function() {
        const profile = ProfileModel.getInstance();
        const url = Utils.addParamsToUrl(
            profile.getSearchApiUrl(),
            _.extend({}, this.searchApiMasterOptions, Session.getSearchApiParams()),
            profile.isAvoidingDoubleRedirect()
        );

        return url;
    },

    model: ItemModel,

    // http://backbonejs.org/#Model-parse
    parse: function(response) {
        // NOTE: Compact is used to filter null values from response
        return _.compact(response.items) || null;
    }
});
export = ItemCollection;
