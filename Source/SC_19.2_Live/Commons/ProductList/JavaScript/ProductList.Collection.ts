/// <amd-module name="ProductList.Collection"/>

import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');
import Utils = require('../../Utilities/JavaScript/Utils');
import ProductListModel = require('./ProductList.Model');

// @class ProductList.Collection @extends Backbone.Collection
export = Backbone.Collection.extend({
    url: Utils.getAbsoluteUrl('services/ProductList.Service.ss'),

    model: ProductListModel,

    // Filter based on the iterator and return a collection of the same type
    filtered: function(iterator) {
        return new this.constructor(this.filter(iterator));
    }
});
