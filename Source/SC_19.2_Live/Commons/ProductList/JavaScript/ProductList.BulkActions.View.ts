/// <amd-module name="ProductList.BulkActions.View"/>

import product_list_bulk_actions_tpl = require('../Templates/product_list_bulk_actions.tpl');
import BackboneView = require('../../BackboneExtras/JavaScript/Backbone.View');

// @class ProductList.BulkActions.View @extends Backbone.View
export = BackboneView.extend({
    template: product_list_bulk_actions_tpl,

    // @method getContext @return ProductList.BulkActions.View.Context
    getContext: function() {
        const { model } = this.options;
        const isAtLeastOneItemChecked = model.someCheckedItemsExist();
        return {
            // @property {Boolean} isAtLeastOneItemChecked
            isAtLeastOneItemChecked: isAtLeastOneItemChecked,
            // @property {Boolean} hasItems
            hasItems: model.get('items').length,
            // @property {Boolean} isAddToCartEnabled
            isAddToCartEnabled: isAtLeastOneItemChecked && model.canBeAddedToCart(true),
            // @property {Boolean} isTypePredefined
            isTypePredefined: model.get('typeName') === 'predefined'
        };
    }
});
