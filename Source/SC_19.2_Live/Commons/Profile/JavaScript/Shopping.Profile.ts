/// <amd-module name="Shopping.Profile"/>
import ProfileModel = require('./Profile.Model');
import jQuery = require('../../Utilities/JavaScript/jQuery');

// -----------------
// Defines the Profile module (Collection, Views, Router)

const ShoppingProfile: any = {
    mountToApp: function(application) {
        application.getUser = function() {
            const profile_promise = jQuery.Deferred();

            ProfileModel.getPromise()
                .done(function() {
                    profile_promise.resolve(ProfileModel.getInstance());
                })
                .fail(function() {
                    profile_promise.reject.apply(this, arguments);
                });

            return profile_promise;
        };
    }
};

export = ShoppingProfile;
