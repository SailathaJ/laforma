/// <amd-module name="Profile.UpdatePassword.Model"/>
import Backbone = require('../../Utilities/JavaScript/backbone.custom');
import Utils = require('../../Utilities/JavaScript/Utils');

// Profile.UpdatePassword.Model.js
// -----------------------
// View Model for changing user's password

// @class Profile.UpdatePassword.Model @extends Backbone.Model
const ProfileUpdatePasswordModel: any = Backbone.Model.extend({
    urlRoot: 'services/Profile.Service.ss',
    validation: {
        current_password: { required: true, msg: Utils.translate('Current password is required') },
        confirm_password: [
            { required: true, msg: Utils.translate('Confirm password is required') },
            {
                equalTo: 'password',
                msg: Utils.translate('New Password and Confirm Password do not match')
            }
        ],

        password: { required: true, msg: Utils.translate('New password is required') }
    }
});

export = ProfileUpdatePasswordModel;
