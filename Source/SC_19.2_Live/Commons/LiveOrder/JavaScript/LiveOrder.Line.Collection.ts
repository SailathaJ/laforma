/// <amd-module name="LiveOrder.Line.Collection"/>

import * as _ from 'underscore';

import TransactionLineCollection = require('../../../Commons/Transaction/JavaScript/Transaction.Line.Collection');
import LiveOrderLineModel = require('../../../Commons/LiveOrder/JavaScript/LiveOrder.Line.Model');

import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

export = TransactionLineCollection.extend({
    model: LiveOrderLineModel,

    url: Utils.getAbsoluteUrl('services/LiveOrder.Line.Service.ss')
});
