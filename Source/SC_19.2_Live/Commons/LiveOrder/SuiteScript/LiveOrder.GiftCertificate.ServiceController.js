// LiveOrder.GiftCertificate.ServiceController.js
// ----------------
// Service to manage gift certificates in the live order
define('LiveOrder.GiftCertificate.ServiceController', [
    'ServiceController',
    'LiveOrder.Model'
], function(ServiceController, LiveOrderModel) {
    // @class LiveOrder.GiftCertificate.ServiceController Manage gift certificates in the live order
    // @extend ServiceController
    return ServiceController.extend({
        // @property {String} name Mandatory for all ssp-libraries model
        name: 'LiveOrder.GiftCertificate.ServiceController',

        // @method post The call to LiveOrder.GiftCertificate.Service.ss with http method 'post' is managed by this function
        // @return {LiveOrder.Model.Data || EmptyObject} Returns the live order or an empty object
        post: function() {
            LiveOrderModel.setGiftCertificates(this.data.giftcertificates);
            return LiveOrderModel.get() || {};
        }
    });
});
