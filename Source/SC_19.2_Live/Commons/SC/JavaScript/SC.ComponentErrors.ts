/// <amd-module name="SC.ComponentErrors"/>

// @module SC
export = {
    // @property {Object} notImplemented
    notImplemented: {
        // @property {String} code
        code: 'NOT_IMPLEMENTED',
        // @property {String} message
        message: 'Not implemented.'
    }
};
