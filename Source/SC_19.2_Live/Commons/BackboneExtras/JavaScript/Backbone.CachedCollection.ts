/// <amd-module name="Backbone.CachedCollection"/>
// It's just an extension of the original Backbone.Collection but it uses the Backbone.cachedSync

import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');
import BackboneCachedSync = require('./Backbone.CachedSync');

Backbone.CachedCollection = Backbone.Collection.extend({
    sync: BackboneCachedSync.cachedSync,
    addToCache: BackboneCachedSync.addToCache,
    isCached: BackboneCachedSync.isCached
});

export = Backbone.CachedCollection;
