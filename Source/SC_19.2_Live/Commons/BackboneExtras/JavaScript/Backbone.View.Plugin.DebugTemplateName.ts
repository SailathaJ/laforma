/// <amd-module name="Backbone.View.Plugin.DebugTemplateName"/>

import * as _ from 'underscore';

import BackboneView = require('./Backbone.View.render');
import Utils = require('../../Utilities/JavaScript/Utils');

export = {
    mountToApp: function() {
        if (!_.result(SC, 'isPageGenerator')) {
            BackboneView.postCompile.install({
                name: 'debugTemplateName',
                priority: 10,
                execute: function(tmpl_str, view) {
                    const template_name = view.template.Name;
                    const prefix = Utils.isPageGenerator()
                        ? ''
                        : '\n\n<!-- TEMPLATE STARTS: ' + template_name + '-->\n';
                    const posfix = Utils.isPageGenerator()
                        ? ''
                        : '\n<!-- TEMPLATE ENDS: ' + template_name + ' -->\n';

                    tmpl_str = prefix + tmpl_str + posfix;

                    return tmpl_str;
                }
            });
        }
    }
};
