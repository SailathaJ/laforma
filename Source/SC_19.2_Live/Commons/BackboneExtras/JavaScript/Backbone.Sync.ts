/// <amd-module name="Backbone.Sync"/>
/*
Overrides native Backbone.sync to pass company and site id on all requests
*/
import * as _ from 'underscore';

import Backbone = require('../../../Commons/Utilities/JavaScript/backbone.custom');
import jQuery = require('../../../Commons/Utilities/JavaScript/jQuery');
import Utils = require('../../Utilities/JavaScript/Utils');

// @method sync SCA Overrides @?method Backbone.Model.url to add model's
// 'internalid' as parameter @return {String}
Backbone.sync = _.wrap(Backbone.sync, function(fn, method, model, options): void {
    let url = options.url || _.result(model, 'url');

    if (url) {
        options = options || {};

        if (url.indexOf('&c=') < 0 && url.indexOf('?c=') < 0) {
            url =
                url +
                (~url.indexOf('?') ? '&' : '?') +
                jQuery.param({
                    // Account Number
                    c: SC.ENVIRONMENT.companyId
                });
        }

        if (url.indexOf('&n=') < 0 && url.indexOf('?n=') < 0) {
            url =
                url +
                (~url.indexOf('?') ? '&' : '?') +
                jQuery.param({
                    // Site Number
                    n: SC.ENVIRONMENT.siteSettings.siteid
                });
        }

        if (
            !Utils.isSingleDomain() &&
            url.indexOf('_ss2') > 0 &&
            url.indexOf('&domain=') < 0 &&
            url.indexOf('?domain=') < 0
        ) {
            url = Utils.addParamsToUrl(url, {
                domain:
                    SC.ENVIRONMENT.siteSettings.touchpoints.home &&
                    SC.ENVIRONMENT.siteSettings.touchpoints.home.replace(
                        /^https?:\/\/([^/?#:]+).*$/,
                        '$1'
                    )
            });
        }
    }

    options.url = url;

    const actionDetails = model.get('actionDetails');
    if (actionDetails) {
        // Adds extra information to improve the performance metrics
        options.url = Utils.addParamsToUrl(options.url, actionDetails);
        model.unset('actionDetails');
    }

    return fn.apply(this, [method, model, options]);
});

export = Backbone.sync;
