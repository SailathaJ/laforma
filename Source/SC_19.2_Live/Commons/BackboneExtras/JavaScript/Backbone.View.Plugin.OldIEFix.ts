/// <amd-module name="Backbone.View.Plugin.OldIEFix"/>

import BackboneView = require('./Backbone.View.render');
import Utils = require('../../Utilities/JavaScript/Utils');

export = {
    mountToApp: function() {
        if (Utils.oldIE()) {
            BackboneView.postCompile.install({
                name: 'oldIEFix',
                priority: 20,
                // Workaround for internet explorer 7. href is overwritten with the absolute path so we save the original href
                // in data-href (only if we are in IE7)
                // IE7 detection courtesy of Backbone
                // More info: http://www.glennjones.net/2006/02/getattribute-href-bug/
                execute: function(tmpl_str) {
                    return (tmpl_str || '').replace(/href="(.+?)(?=")/g, '$&" data-href="$1');
                }
            });
        }
    }
};
