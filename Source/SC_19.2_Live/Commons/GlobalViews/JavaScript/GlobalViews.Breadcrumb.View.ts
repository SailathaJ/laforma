/// <amd-module name="GlobalViews.Breadcrumb.View"/>
/// <reference path="../../../Commons/Utilities/JavaScript/UnderscoreExtended.d.ts" />

import * as _ from 'underscore';

import global_views_breadcrumb_tpl = require('../Templates/global_views_breadcrumb.tpl');
import BackboneView = require('../../BackboneExtras/JavaScript/Backbone.View');

// @class GlobalViews.Breadcrumb.View @extends Backbone.View
const GlobalViewsBreadcrumbView = BackboneView.extend({
    template: global_views_breadcrumb_tpl,

    initialize: function(options) {
        const opt_pages = options.pages;

        if (_.isUndefined(opt_pages)) {
            this.pages = [];
        } else if (_.isArray(opt_pages)) {
            this.pages = opt_pages;
        } else {
            this.pages = [opt_pages];
        }
    },

    // @method getContext @return GlobalViews.Breadcrumb.View.Context
    getContext: function() {
        _.each(this.pages, function(page: any) {
            if (page['data-touchpoint']) {
                page.hasDataTouchpoint = true;
            }

            if (page['data-hashtag']) {
                page.hasDataHashtag = true;
            }
        });

        // @class GlobalViews.Breadcrumb.View.Context
        return {
            // @property {Array<Object>} pages
            pages: this.pages
        };
    }
});

export = GlobalViewsBreadcrumbView;
