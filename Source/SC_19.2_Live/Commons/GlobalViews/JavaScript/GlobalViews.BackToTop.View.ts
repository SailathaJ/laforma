/// <amd-module name="GlobalViews.BackToTop.View"/>

import global_views_back_to_top_tpl = require('../Templates/global_views_back_to_top.tpl');
import BackboneView = require('../../BackboneExtras/JavaScript/Backbone.View');
import jQuery = require('../../Utilities/JavaScript/jQuery');

// @class GlobalViews.BackToTop.View @extends Backbone.View
const GlobalViewsBackToTopView: any = BackboneView.extend({
    template: global_views_back_to_top_tpl,

    events: {
        'click [data-action="back-to-top"]': 'backToTop'
    },

    // @method backToTop
    backToTop: function() {
        jQuery('html, body').animate({ scrollTop: '0px' }, 300);
    }
});

export = GlobalViewsBackToTopView;
