/// <amd-module name="RecentlyViewedItems"/>

import RecentlyViewedItemsView = require('./RecentlyViewedItems.View');
import CartDetailedView = require('../../Cart/JavaScript/Cart.Detailed.View');

const RecentlyViewedItems: any = {
    // @method mountToApp
    // @param {ApplicationSkeleton} application
    // @return {Void}
    mountToApp: function(application) {
        CartDetailedView.addChildViews({
            'RecentlyViewed.Items': function wrapperFunction() {
                return function() {
                    return new RecentlyViewedItemsView({
                        application: application
                    });
                };
            }
        });
    }
};

export = RecentlyViewedItems;
