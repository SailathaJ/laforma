// ! © 2018 NetSuite Inc.
/**
 *  @module PosApplication
 *  @class  SCIS.Session
 **/
define('SC.Session', ['SC.Model', 'Configuration', 'underscore'], function(
    SCModel,
    Configuration,
    _
) {
    const context = nlapiGetContext();
    let isSCIS;
    let location;
    let subsidiary;

    return SCModel.extend({
        name: 'SC.Session',
        /*
         *	Returns the location id for the employee.
         *		if SCIS => the selecetd scis location
         *		else => the employee location field.
         */
        getCurrentLocation: function getCurrentLocation() {
            isSCIS = _.isUndefined(isSCIS) ? Configuration.get().isSCIS : isSCIS;

            if (isSCIS) {
                location = location || JSON.parse(context.getSessionObject('location'));
                location = location && _.isObject(location) ? location.internalid : location;
            } else {
                location = location || context.getLocation();
            }
            return location + '';
        },

        /*
         *	Returns the current subsidiary for this session.
         */
        getCurrentSubsidiary: function getCurrentSubsidiary() {
            subsidiary = subsidiary || context.getSubsidiary();
            return subsidiary;
        }
    });
});
