/// <amd-module name="ErrorManagement.ForbiddenError.View"/>
/// <reference path="../../Utilities/JavaScript/GlobalDeclarations.d.ts" />
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');
import ErrorManagementView = require('./ErrorManagement.View');
import error_management_forbidden_error_tpl = require('../Templates/error_management_forbidden_error.tpl');

const ErrorManagementForbiddenErrorView: any = ErrorManagementView.extend({
    template: error_management_forbidden_error_tpl,
    attributes: {
        id: 'forbidden-error',
        class: 'forbidden-error'
    },
    title: Utils.translate('NOT ALLOWED'),
    page_header: Utils.translate('NOT ALLOWED'),

    initialize: function() {
        if (SC.ENVIRONMENT.jsEnvironment === 'server') {
            nsglobal.statusCode = 403;
        }
    },

    // @method getContext @returns {ErrorManagement.ForbiddenError.View.Context}
    getContext: function() {
        // @class ErrorManagement.ForbiddenError.View.Context
        return {
            // @property {String} title
            title: this.title,
            // @property {String} pageHeader
            pageHeader: this.page_header
        };
    }
});

export = ErrorManagementForbiddenErrorView;
