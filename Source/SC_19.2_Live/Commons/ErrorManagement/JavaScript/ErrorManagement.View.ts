/// <amd-module name="ErrorManagement.View"/>

import BackboneView = require('../../BackboneExtras/JavaScript/Backbone.View');
import '../../BackboneExtras/JavaScript/Backbone.View.render';

const ErrorManagementView: any = BackboneView.extend({
    isErrorView: true
});

export = ErrorManagementView;
