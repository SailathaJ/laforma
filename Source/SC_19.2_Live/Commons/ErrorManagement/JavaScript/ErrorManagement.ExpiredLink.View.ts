/// <amd-module name="ErrorManagement.ExpiredLink.View"/>
/// <reference path="../../Utilities/JavaScript/GlobalDeclarations.d.ts" />
/// <reference path="../../../Commons/Utilities/JavaScript/UnderscoreExtended.d.ts" />

import ErrorManagementView = require('./ErrorManagement.View');
import error_management_expired_link_tpl = require('../Templates/error_management_expired_link.tpl');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

const ErrorManagementExpiredLinkView: any = ErrorManagementView.extend({
    template: error_management_expired_link_tpl,
    attributes: {
        id: 'expired_link',
        class: 'expired_link'
    },

    initialize: function(options) {
        if (options.title) {
            this.title = options.title;
        }

        if (options.page_header) {
            this.page_header = options.page_header;
        }

        if (options.message) {
            this.message = options.message;
        }

        if (SC.ENVIRONMENT.jsEnvironment === 'server') {
            nsglobal.statusCode = 500;
        }
    },

    // @method getContext @returns {ErrorManagement.ExpiredLink.View.Context}
    getContext: function() {
        // @class ErrorManagement.ExpiredLink.View.Context
        return {
            // @property {String} pageHeader
            pageHeader: this.page_header || '',
            // @property {String} message
            message:
                this.message ||
                Utils.translate('Sorry, we could not load the content you requested.')
        };
    }
});

export = ErrorManagementExpiredLinkView;
