/// <amd-module name="ErrorManagement.PageNotFound.View">
/// <reference path="../../../Commons/Utilities/JavaScript/GlobalDeclarations.d.ts" />

import ErrorManagementView = require('./ErrorManagement.View');
import error_management_page_not_found_tpl = require('../Templates/error_management_page_not_found.tpl');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

const ErrorManagementPageNotFoundView: any = ErrorManagementView.extend({
    template: error_management_page_not_found_tpl,
    attributes: {
        id: 'page-not-found',
        class: 'page-not-found'
    },
    title: Utils.translate('Page not found'),
    page_header: Utils.translate('Page not found'),

    initialize: function() {
        if (SC.ENVIRONMENT.jsEnvironment === 'server') {
            nsglobal.statusCode = 404;
        }
    },

    // @method getContext @returns {ErrorManagement.PageNotFound.View.Context}
    getContext: function() {
        // @class ErrorManagement.PageNotFound.View.Context
        return {
            // @property {String} title
            title: this.title,
            // @property {String} pageHeader
            pageHeader: this.page_header
        };
    }
});

export = ErrorManagementPageNotFoundView;
