/// <amd-module name="ErrorManagement.LoggedOut.View"/>

import ErrorManagementView = require('./ErrorManagement.View');
import error_management_logged_out_tpl = require('../Templates/error_management_logged_out.tpl');
import BackboneView = require('../../../Commons/BackboneExtras/JavaScript/Backbone.View');
import Utils = require('../../../Commons/Utilities/JavaScript/Utils');

const ErrorManagementLoggedOutView = ErrorManagementView.extend({
    template: error_management_logged_out_tpl,
    attributes: {
        id: 'logged-out'
    },
    title: Utils.translate('Logged out'),

    initialize: function() {
        this.labels = {
            title: Utils.translate('You have been logged out'),
            explanation: Utils.translate(
                'Your session expired or someone else logged in another device with your account. You must log in again to continue.'
            ),
            login: Utils.translate('Log in')
        };
    },
    showError: function() {},

    render: function() {
        const res = BackboneView.prototype.render.apply(this, arguments);
        this.$containerModal.find('[data-dismiss="modal"]').remove();
        return res;
    },

    // @method getContext @returns {ErrorManagement.LoggedOut.View.Context}
    getContext: function() {
        // @class ErrorManagement.LoggedOut.View.Context
        return {
            // @property {Object} labels
            labels: this.labels
        };
    }
});

export = ErrorManagementLoggedOutView;
