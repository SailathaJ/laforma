// ! © 2015 NetSuite Inc.

/* global release_metadata  */
define('ReleaseMetadata', ['SC.Models.Init', 'underscore'], function(ModelsInit, _) {
    return {
        available: function() {
            return release_metadata !== undefined;
        },

        get: function() {
            release_metadata.ns_version = release_metadata.ns_version
                ? release_metadata.ns_version
                : ModelsInit.context.getVersion();

            return release_metadata;
        },

        getVersion: function() {
            return release_metadata && release_metadata.version;
        },

        asHTMLComment: function() {
            if (!release_metadata) {
                return '';
            }

            // Generates something like [ bundle_id "48040" ] [ baselabel "POS_ML" ] ...
            const bracketVals = _.chain(release_metadata)
                .omit('name')
                .map(function(value, key) {
                    return '[ ' + key + ' ' + JSON.stringify(value) + ' ]';
                })
                .value()
                .join(' ');

            return '<!-- ' + release_metadata.name + ' ' + bracketVals + ' -->';
        }
    };
});
