/**
 * @NApiVersion 2.x
 * @NModuleScope Public
 */

import * as record from 'N/record';
import * as runtime from 'N/runtime';

import '../../third_parties/underscore.js';
import { unauthorizedError } from '../../Libraries/SspLibraries/RequestErrors';
import { SCModel, ColumnMap } from '../../Libraries/SspLibraries/SC.Model';
import { IListable } from '../../Libraries/SspLibraries/IListable';
import Configuration from '../../Libraries/Configuration/Configuration';
import { SearchRecord, SearchResult } from '../../Libraries/SearchRecord/SearchRecordHelper';

interface CaseInput {
    id?: string;
    reply?: string;
    statusid?: string;
    title?: string;
    message?: string;
    category?: string;
    email?: string;
    customerId?: number;
}

export interface Case {
    internalid: string;
    caseNumber: string;
    title: string;
    status: CaseField;
    origin: CaseField;
    category: CaseField;
    company: CaseField;
    priority: CaseField;
    createdDate: string;
    lastMessageDate: string;
    email: string;
    messages_count: number;
    grouped_messages: {
        date: string;
        messages: {
            author: string;
            text: string;
            messageDate: string;
            internalid: string;
            initialMessage: boolean;
        }[];
    }[]; // Keep this name for backwards compatibility.
}

export class CaseModel extends SCModel {
    public readonly name = 'Case';

    private search_helper: SearchRecord;

    private readonly record_type = record.Type.SUPPORT_CASE;

    private columns: ColumnMap = {
        internalid: { name: 'internalid' },
        caseNumber: { name: 'casenumber' },
        title: { name: 'title' },
        status: { name: 'status' },
        origin: { name: 'origin' },
        category: { name: 'category' },
        company: { name: 'company' },
        createdDate: { name: 'createddate' },
        lastMessageDate: { name: 'lastmessagedate' },
        priority: { name: 'priority' },
        email: { name: 'email' }
    };

    public constructor() {
        super();
        this.search_helper = new SearchRecord(this.record_type);
    }

    public getCaseFields(): CaseFields {
        const case_record = record.create({
            type: this.record_type,
            isDynamic: true
        });
        const category_field = case_record.getField({ fieldId: 'category' });
        const origin_field = case_record.getField({ fieldId: 'origin' });
        const status_field = case_record.getField({ fieldId: 'status' });
        const priority_field = case_record.getField({ fieldId: 'priority' });

        return {
            categories: this.createCaseFieldValues(category_field.getSelectOptions()),
            origins: this.createCaseFieldValues(origin_field.getSelectOptions()),
            statuses: this.createCaseFieldValues(status_field.getSelectOptions()),
            priorities: this.createCaseFieldValues(priority_field.getSelectOptions())
        };
    }

    public get(id: number): Case {
        const filters: any[] = [['internalid', 'is', id], 'and', ['isinactive', 'is', 'F']];
        const columns: ColumnMap = this.getColumns();
        const result = this.searchHelper(filters, columns, 1, true);

        if (result.records.length >= 1) {
            return result.records[0];
        }

        throw unauthorizedError;
    }

    public search(list_header_data: IListable): SearchResult<Case> {
        const filters: any[] = [['isinactive', 'is', 'F']];
        const columns: ColumnMap = this.getColumns();
        const selected_filter = parseInt(list_header_data.filter, 10);

        if (!_.isNaN(selected_filter)) {
            filters.push('and');
            filters.push(['status', 'anyof', selected_filter]);
        }

        this.setSortOrder(list_header_data.sort, list_header_data.order, columns);

        return this.searchHelper(filters, columns, list_header_data.page, false);
    }

    public create(data: CaseInput): number {
        const new_case_record = record.create({
            type: this.record_type
        });

        if (data.title) {
            new_case_record.setValue('title', super.sanitizeString(data.title));
        }
        if (data.message) {
            new_case_record.setValue('incomingmessage', super.sanitizeString(data.message));
        }
        if (data.category) {
            new_case_record.setValue('category', data.category);
        }
        if (data.email) {
            new_case_record.setValue('email', data.email);
        }
        if (data.customerId) {
            new_case_record.setValue('company', data.customerId);
        }

        new_case_record.setValue(
            'status',
            Configuration.getDefault('cases.defaultValues.statusStart.id')
        ); // Not Started
        new_case_record.setValue(
            'origin',
            Configuration.getDefault('cases.defaultValues.origin.id')
        ); // Web

        return new_case_record.save();
    }

    public update(data: CaseInput): void {
        if (data && data.statusid) {
            let values: { incomingmessage?: string; messagenew?: string; status: string };

            if (data.reply && data.reply.length > 0) {
                values = {
                    incomingmessage: super.sanitizeString(data.reply),
                    messagenew: 'T',
                    status: data.statusid
                };
            } else {
                values = { status: data.statusid };
            }

            record.submitFields({
                type: this.record_type,
                id: data.id,
                values: values
            });
        }
    }

    private createCaseFieldValues(select_options: { value: any; text: string }[]): CaseField[] {
        const option_values = [];
        _(select_options).each(function(select_option: { value: any; text: string }) {
            const option_value = {
                id: select_option.value,
                text: select_option.text
            };

            option_values.push(option_value);
        });

        return option_values;
    }

    private searchHelper(
        filters: any[],
        columns: ColumnMap,
        page: number,
        join_messages: boolean
    ): SearchResult<Case> {
        const converter = (rcd, col) => ({ id: rcd.getValue(col), name: rcd.getText(col) });
        const result = this.search_helper
            .startQuery<Case>()
            .setColumns(columns)
            .setFilters(filters)
            .setConverters({
                status: converter,
                origin: converter,
                category: converter,
                company: converter,
                priority: converter,
                createdDate: this.search_helper.Converters.toDateString,
                lastMessageDate: this.search_helper.Converters.toDateString
            })
            .getPaginated(page);

        result.records = result.records.map(rcd => {
            rcd.grouped_messages = [];
            if (join_messages) {
                rcd = this.appendMessagesToCase(rcd);
            }
            return rcd;
        });

        return result;
    }

    private getColumns(): ColumnMap {
        return this.columns;
    }

    private appendMessagesToCase(caseObj: Case): Case {
        const customer_id = String(runtime.getCurrentUser().id);

        // define columns
        const columns = {
            text: { name: 'message', join: 'messages' },
            messageDate: {
                name: 'messagedate',
                join: 'messages',
                sort: this.search_helper.Sort.DESC
            },
            author: { name: 'author', join: 'messages' },
            internalid: { name: 'internalid', join: 'messages' }
        };

        interface MessageResult {
            author: string;
            text: string;
            messageDate: string;
            internalid: string;
            initialMessage: boolean;
        }

        // get records
        const message_result = this.search_helper
            .startQuery<MessageResult>()
            .setFilters([
                ['internalid', this.search_helper.Op.IS, caseObj.internalid],
                'and',
                ['messages.internalonly', this.search_helper.Op.IS, 'F']
            ])
            .setColumns(columns)
            .setConverters({
                text: this.search_helper.Converters.stripHTML,
                author: (rcd, col) => (rcd.getValue(col) === customer_id ? 'You' : rcd.getText(col))
            })
            .getAll();

        caseObj.messages_count = message_result.length;

        const messages_by_date = _.groupBy(
            message_result,
            (message: MessageResult, index: number) => {
                const message_date: string = this.search_helper.Converters.toDateString(
                    message.messageDate
                );
                const today_date: string = this.search_helper.Converters.toDateString(new Date());
                const date_to_show: string = today_date == message_date ? 'Today' : message_date;
                message.messageDate = this.search_helper.Converters.toTimeOfDay(
                    message.messageDate
                );
                message.initialMessage = index === message_result.length - 1;
                return date_to_show;
            }
        );

        caseObj.grouped_messages = _.map(
            messages_by_date,
            (messages: MessageResult, date: string) => ({ date, messages })
        );

        return caseObj;
    }

    // @method setSortOrder
    // Adds sort condition to the respective column
    // @param {String} sort column name
    // @param {Number} order
    // @param {Array} columns columns array
    private setSortOrder(sort: string, order: number, columns: ColumnMap): void {
        const direction = order > 0 ? this.search_helper.Sort.DESC : this.search_helper.Sort.ASC;
        switch (sort) {
            case 'createdDate':
            case 'lastMessageDate':
                columns[sort].sort = direction;
                break;
            default:
                columns.internalid.sort = direction;
        }
    }
}

export interface CaseField {
    id: string;
    name: string;
}

export interface CaseFields {
    categories: CaseField[];
    origins: CaseField[];
    statuses: CaseField[];
    priorities: CaseField[];
}
