/**
 * @NApiVersion 2.x
 * @NModuleScope Public
 */

import { ServiceController } from '../../Libraries/SspLibraries/ServiceController';
import { CaseFields, CaseModel } from './Case.Model';

class CaseFieldsServiceController extends ServiceController {
    public name = 'Case.Fields.ServiceController2';

    private caseModel: CaseModel;

    public constructor() {
        super();
        this.caseModel = new CaseModel();
    }

    protected options = {
        common: {
            requireLogin: true,
            requirePermissions: {
                list: ['lists.listCase.1']
            }
        }
    };

    // @method get The call to Case.Fields.ss with
    // http method 'get' is managed by this function
    // @return {CaseFields} New Case record
    public get(): CaseFields {
        return this.caseModel.getCaseFields();
    }
}

export = {
    service: function(ctx) {
        new CaseFieldsServiceController().service(ctx);
    }
};
