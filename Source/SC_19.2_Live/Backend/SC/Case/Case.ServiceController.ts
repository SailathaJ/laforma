/**
 * @NApiVersion 2.x
 * @NModuleScope Public
 */

import * as runtime from 'N/runtime';
import * as log from 'N/log';

import { ServiceController } from '../../Libraries/SspLibraries/ServiceController';
import { IListable } from '../../Libraries/SspLibraries/IListable';
import { Case, CaseModel } from './Case.Model';
import { SearchResult } from '../../Libraries/SearchRecord/SearchRecordHelper';

class CaseServiceController extends ServiceController {
    public name = 'Case.ServiceController2';

    private caseModel: CaseModel;

    public constructor() {
        super();
        this.caseModel = new CaseModel();
    }

    protected options = {
        common: {
            requireLogin: true,
            requirePermissions: {
                list: ['lists.listCase.1']
            }
        }
    };

    public get(): Case | SearchResult<Case> {
        try {
            // TODO: Check if this functions can be separated!
            const { internalid } = this.request.parameters;

            if (internalid) {
                return this.caseModel.get(internalid);
            }
            const list_header_data: IListable = {
                filter: this.request.parameters.filter,
                order: this.request.parameters.order,
                sort: this.request.parameters.sort,
                from: this.request.parameters.from,
                to: this.request.parameters.to,
                page: this.request.parameters.page
            };

            return this.caseModel.search(list_header_data);
        } catch (error) {
            log.error({ title: 'Error Record Error', details: 'Error: ' + error });
        }
    }

    public post(): Case {
        const caseInput = {
            statusid: this.data.status && this.data.status.id,
            title: this.data.title,
            message: this.data.message,
            category: this.data.category,
            email: this.data.email,
            customerId: runtime.getCurrentUser().id
        };

        const new_case_id = this.caseModel.create(caseInput);

        return this.caseModel.get(new_case_id);
    }

    public put(): Case {
        const internalid = this.request.parameters.internalid || this.data.internalid;
        const caseInput = {
            id: internalid,
            statusid: this.data.status && this.data.status.id,
            reply: this.data.reply
        };

        this.caseModel.update(caseInput);

        return this.caseModel.get(internalid);
    }
}

export = {
    service: function(ctx) {
        new CaseServiceController().service(ctx);
    }
};
