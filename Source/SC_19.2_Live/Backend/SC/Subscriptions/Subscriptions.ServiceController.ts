/**
 * @NApiVersion 2.x
 * @NModuleScope Public
 */

import { SubscriptionsModel } from './Subscriptions.Model';
import { badRequestError } from '../../Libraries/SspLibraries/RequestErrors';
import { ServiceController } from '../../Libraries/SspLibraries/ServiceController';
import Configuration from '../../Libraries/Configuration/Configuration';

class SubscriptionsServiceController extends ServiceController {
    public readonly name = 'Subscriptions.ServiceController';

    protected options = {
        common: {
            requireLogin: true
        }
    };

    public get() {
        const internal_id: number = this.request.parameters.internalid;
        let subscriptionsModel: SubscriptionsModel;
        if (internal_id) {
            subscriptionsModel = new SubscriptionsModel(internal_id);
            return subscriptionsModel.get(internal_id);
        }
        const { page } = this.request.parameters;
        subscriptionsModel = new SubscriptionsModel();
        return subscriptionsModel.search(page);
    }

    public put() {
        if (!this.data.internalid) {
            throw badRequestError;
        }

        const subscriptionsModel: SubscriptionsModel = new SubscriptionsModel(this.data.internalid);

        if (this.data.action === 'delete' && !_.isUndefined(this.data.lineNumber)) {
            if (
                Configuration.get('subscriptions.lineStatusChange') !== "Don't Allow Status Changes"
            ) {
                subscriptionsModel.suspendLine(this.data);
            } else {
                throw 'Cancelling subscription lines is not allowed. Please contact your sales representative.';
            }
        } else if (!_.isUndefined(this.data.quantity) && !_.isUndefined(this.data.lineNumber)) {
            subscriptionsModel.updateLine(this.data);
        } else if (
            Configuration.get('subscriptions.generalStatusChange') === 'Allow Suspending / Resuming' &&
            _.isUndefined(this.data.lineNumber)
        ) {
            subscriptionsModel.reactivateSubscription(this.data);
        } else {
            throw 'Resuming subscriptions is not allowed. Please contact your sales representative.';
        }
        return subscriptionsModel.get(this.data.internalid);
    }

    public delete() {
        const id = this.request.parameters.internalid;
        if (!id) {
            throw badRequestError;
        }

        const subscriptionsModel: SubscriptionsModel = new SubscriptionsModel(id);

        if (
            Configuration.get('subscriptions.generalStatusChange') !== "Don't Allow Status Changes"
        ) {
            subscriptionsModel.suspendSubscription();
        } else {
            throw 'Cancelling subscriptions is not allowed. Please contact your sales representative.';
        }

        return subscriptionsModel.get(id);
    }
}

export = {
    service: function(ctx) {
        new SubscriptionsServiceController().service(ctx);
    }
};
