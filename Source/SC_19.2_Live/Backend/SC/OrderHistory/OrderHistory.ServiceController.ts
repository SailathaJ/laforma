/**
 * @NApiVersion 2.x
 * @NModuleScope Public
 */

import * as record from 'N/record';
import * as log from 'N/log';

import { ServiceController } from '../../Libraries/SspLibraries/ServiceController';

class OrderHistoryServiceController extends ServiceController {
    public name = 'OrderHistory.ServiceController2';

    protected options = {
        common: {
            requireLogin: true,
            requirePermissions: {
                list: ['transactions.tranFind.1', 'transactions.tranSalesOrd.1']
            }
        }
    };

    public get() {
        if (!this.request.parameters.internalid) {
            throw 'internalid parameter not present';
        }

        try {
            const objRecord = record.load({
                type: record.Type.SALES_ORDER,
                id: this.request.parameters.internalid,
                isDynamic: true
            });

            const taxTotals: any = objRecord.executeMacro({
                id: 'getSummaryTaxTotals',
                params: null
            });
            return taxTotals && taxTotals.response && taxTotals.response.taxTotals
                ? taxTotals.response.taxTotals
                : [];
        } catch (error) {
            log.error({
                title: 'Load Record Error',
                details: 'You may need to enable Suite Tax Feature. ' + error
            });
            return [];
        }
    }
}

export = {
    service: function(ctx) {
        new OrderHistoryServiceController().service(ctx);
    }
};
