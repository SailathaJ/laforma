import * as search from 'N/search';
import * as log from 'N/log';
import { configurationManifest } from './configurationManifest';
import * as Utils from '../SspLibraries/Utils';

class Configuration {
    private domain: string;

    private siteId: number;

    private values: object;

    private default_values: object = configurationManifest();

    private extension_values: object = {};

    // TODO Now extension_values will be always an empty object. See overrideByDomain description.
    protected record_type: string = 'customrecord_ns_sc_configuration';

    private custrecord_ns_scc_key: string = 'custrecord_ns_scc_key';

    private custrecord_ns_scc_value: string = 'custrecord_ns_scc_value';

    public constructor() {}

    public get(path?: string): any {
        return Utils.getPathFromObject(this.values, path);
    }

    public set(path: string, value: any): void {
        Utils.setPathFromObject(this.values, path, value);
    }

    public getDefault(path?: string): any {
        return Utils.getPathFromObject(this.default_values, path);
    }

    /**
     * Merge and return all default and custom configuration.
     * @return {object}
     */
    private allConfigurations(): object {
        // from each module file and extensions (default configuration):
        const valuesFromModules = Utils.deepExtend(this.default_values, this.extension_values);
        // from record (custom configuration):
        const customValuesFromRecord = this.getValuesFromRecord();

        return Utils.deepExtend(valuesFromModules, customValuesFromRecord);
    }

    private getValuesFromRecord(): object {
        const key = `${this.siteId}|${this.domain || 'all'}`;
        let rcd: search.Result;
        // Search without SearchRecordHelper to avoid circular dependencies
        try {
            [rcd] = search
                .create({
                    type: this.record_type,
                    columns: [search.createColumn({ name: this.custrecord_ns_scc_value })],
                    filters: [
                        search.createFilter({
                            name: this.custrecord_ns_scc_key,
                            operator: search.Operator.IS,
                            values: key
                        })
                    ]
                })
                .run()
                .getRange({ start: 0, end: 1 });
        } catch (e) {
            log.debug('No configuration record found', { key });
        }
        return rcd ? JSON.parse(<string>rcd.getValue(this.custrecord_ns_scc_value)) : {};
    }

    /**
     * Extensions doesn't support SS2 yet. A method with this name is now being called
     * in the ssp_libraries_ext file for SS1 Configuration.
     * When the configuration data from extensions becomes available for SS2 we
     * can call this method or change it to some other implementation.
     * TODO consider a new name, I.e. setExtensionConfiguration.
     * @param {object} extension_configuration
     * @return {void}
     */
    public overrideByDomain(extension_configuration: object): void {
        this.extension_values = extension_configuration;
    }

    private adaptValuestoOldFormat(): void {
        // TODO, adapt the values of this.values.multiDomain.hosts.languages and
        // this.values.multiDomain.hosts.currencies to the structure requiered by hosts
    }

    /**
     * We have no method in SS2 to get domain name and site id, which are
     * necessary to create the key that is related to the specific configuration record
     *
     * Call this method to setup configuration with specific domain and site.
     * @param {number} siteid
     * @param {string} domain
     * @return {void}
     */
    public setConfig(siteid: number, domain: string): void {
        this.siteId = siteid;
        this.domain = domain;

        this.values = this.allConfigurations();
    }
}

export default new Configuration();
