import '../../third_parties/underscore.js';
import * as search from 'N/search';
import Configuration from '../Configuration/Configuration';
import { SearchResult } from './SearchRecordHelper';

type MapConverter<T> = { [P in keyof T]?: (r: search.Result, c: search.Column) => T[P] };
type MapColumn<T> = { [P in keyof T]?: search.Column };

export default class Query<T> {
    private record_type: string;

    private filters: any[] = [];

    private converters: MapConverter<T> = {};

    private columns: MapColumn<T> = {};

    public constructor(record_type: string) {
        this.record_type = record_type;
    }

    public setConverters(converters: MapConverter<T>): Query<T> {
        this.converters = converters;
        return this;
    }

    public setColumns(columns: MapColumn<T>): Query<T> {
        this.columns = columns;
        return this;
    }

    public setFilters(filters: any[]): Query<T> {
        this.filters = [filters];
        return this;
    }

    public getAll(): T[] {
        const result: search.Result[] = [];
        this.run().each(function(row): boolean {
            result.push(row);
            return true;
        });
        return this.convertResult(result);
    }

    public getPaginated(
        page: number = 1,
        results_per_page: number = Configuration.get('suitescriptResultsPerPage')
    ): SearchResult<T> {
        const range_start: number = page * results_per_page - results_per_page;
        const range_end: number = page * results_per_page;
        const records: T[] = this.getRange(range_start, range_end);

        return {
            page: page,
            recordsPerPage: results_per_page,
            totalRecordsFound: this.getAll().length,
            records: records
        };
    }

    private getRange(start: number, end: number): T[] {
        const result: search.Result[] = this.run().getRange({
            start: start,
            end: end
        });
        return this.convertResult(result);
    }

    private convertResult(results): T[] {
        return results.map((result: search.Result) => {
            return _.mapObject(this.columns, (column: search.Column, public_name: string) => {
                const converter = this.converters[public_name];
                return _.isFunction(converter)
                    ? converter(result, column)
                    : result.getValue(column);
            });
        });
    }

    private run(): search.ResultSet {
        return search
            .create({
                type: this.record_type,
                columns: _.map(this.columns, (P: search.Column) => _.identity(P)),
                filters: this.filters
            })
            .run();
    }
}
