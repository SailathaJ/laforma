import * as search from 'N/search';
import '../../third_parties/underscore.js';
import Query from './Query.js';
import * as Converters from './Converters.js';
import Configuration from '../Configuration/Configuration';

export interface SearchResult<T> {
    page: number;
    recordsPerPage: number;
    totalRecordsFound: number;
    records: T[];
}

export interface SearchQuery {
    filters?: search.Filter[];
    columns?: search.Column[];
    column_count?: search.Column;
    page?: number;
    results_per_page?: number;
}

export class SearchRecord {
    protected record_type: string;

    public Op = search.Operator;

    public Sort = search.Sort;

    public Converters = Converters;

    public constructor(record_type: string) {
        this.record_type = record_type;
    }

    paginatedSearch<T>(
        options: SearchQuery = {},
        transform: (result: search.Result) => T
    ): SearchResult<T> {
        const results_per_page: number =
            options.results_per_page || Configuration.get('suitescriptResultsPerPage');

        const page: number = options.page || 1;

        const columns = options.columns || [];

        const filters = options.filters || [];

        const record_type: string = this.record_type;

        const range_start: number = page * results_per_page - results_per_page;

        const range_end: number = page * results_per_page;

        const do_real_count: boolean = _.any(columns, function(column: search.Column) {
            // check if at least one column have a summary
            return !_.isUndefined(column.summary);
        });

        const result: SearchResult<T> = {
            page: page,
            recordsPerPage: results_per_page,
            records: [],
            totalRecordsFound: 0
        };

        if (!do_real_count || options.column_count) {
            const column_count =
                options.column_count ||
                search.createColumn({
                    name: 'internalid',
                    summary: search.Summary.COUNT
                });

            const count_search = search
                .create({
                    type: record_type,
                    filters: filters,
                    columns: [column_count]
                })
                .run();

            count_search.each(function(countResult) {
                result.totalRecordsFound = parseInt(<string>countResult.getValue(column_count), 10);
                return true;
            });
        }

        if (
            do_real_count ||
            (result.totalRecordsFound > 0 && result.totalRecordsFound > range_start)
        ) {
            const records_search = search
                .create({
                    type: record_type,
                    filters: filters,
                    columns: columns
                })
                .run();

            const range = records_search.getRange({
                start: range_start,
                end: range_end
            });
            result.records = _.map(range, transform);

            if (do_real_count && !options.column_count) {
                result.totalRecordsFound = records_search.getRange({
                    start: 0,
                    end: 1000
                }).length;
            }
        }

        return result;
    }

    public search<T>(
        columns: search.Column[],
        filters: search.Filter[],
        range: { start: number; end: number } = { start: 0, end: 1000 },
        transform: (result: search.Result) => T
    ): T[] {
        const result: search.Result[] = search
            .create({
                type: this.record_type,
                columns: columns,
                filters: filters
            })
            .run()
            .getRange(range);

        return _.map(result, transform);
    }

    public searchOne<T>(
        columns: search.Column[],
        filters: search.Filter[],
        transform: (result: search.Result) => T
    ): T | null {
        const result = this.search(columns, filters, { start: 0, end: 1 }, transform);
        return result.length ? result[0] : null;
    }

    public startQuery<T>(record_type?: string): Query<T> {
        return new Query<T>(record_type || this.record_type);
    }
}
