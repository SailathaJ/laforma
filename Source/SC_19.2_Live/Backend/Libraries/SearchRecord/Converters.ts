import '../../third_parties/underscore.js';
import * as format from 'N/format';

const dummy_date = new Date().toISOString();

function toDate(record, column): Date {
    return <Date>format.parse({
        value: toValueString(record, column),
        type: format.Type.DATE
    });
}

export function toDateString(record, column?): string {
    return <string>format.format({
        value: toDate(record, column) || dummy_date,
        type: format.Type.DATE
    });
}

export function toTimeOfDay(record, column?): string {
    return <string>format.format({
        value: toDate(record, column) || dummy_date,
        type: format.Type.TIMEOFDAY
    });
}

export function stripHTML(record, column?): string {
    return toValueString(record, column)
        .replace(/<br\s*[\/]?>/gi, '\n')
        .replace(/<(?:.|\n)*?>/gm, '');
}

export function toValueString(record, column?): string {
    return String(toValue(record, column));
}

export function toValue(record, column?): string | boolean | string[] {
    return column ? record.getValue(column) : record;
}

export function toText(record, column?): string {
    return column ? record.getText(column) : record;
}
