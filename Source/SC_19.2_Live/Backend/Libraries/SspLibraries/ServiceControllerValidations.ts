import '../../third_parties/underscore.js';
import * as runtime from 'N/runtime';
import * as log from 'N/log';
import { forbiddenError, unauthorizedError } from './RequestErrors';

type PermissionOperator = 'and' | 'or';

class ServiceControllerValidations {
    private user: runtime.User = runtime.getCurrentUser();

    // @method _evaluatePermissions Evaluates if the permissions demanded are fulfilled by the user of the service
    // @parameter {transactions: Object, lists: Object} user_permissions The permissions the user has
    // @parameter {Object} required_permissions Object literal with all the permissions required by the current service
    // @parameter {String} permissions_operator The operator that must be applied to the array of permissions required by the current service
    // @return {Boolean} True if the permissions have been validated
    private evaluatePermissions(
        required_permissions: string[],
        permissions_operator: PermissionOperator = 'and'
    ): boolean {
        const user_permissions = this.getPermissions();
        let evaluation: boolean = permissions_operator !== 'or';

        if (permissions_operator !== 'and' && permissions_operator !== 'or') {
            log.error(
                'Permissions Error',
                'Invalid permissions operator. Allowed values are: or, and'
            );
            return false;
        }

        if (!_.isArray(required_permissions)) {
            log.error('Permissions Error', 'Invalid permissions format');
            return false;
        }

        _.each(required_permissions, function(perm): void {
            const tokens: string[] = perm.split('.');

            const partial_evaluation = !(
                tokens.length === 3 &&
                Number(tokens[2]) < 5 &&
                user_permissions &&
                user_permissions[tokens[0]] &&
                user_permissions[tokens[0]][tokens[1]] < tokens[2]
            );

            if (permissions_operator === 'or') {
                evaluation = evaluation || partial_evaluation;
            } else {
                evaluation = evaluation && partial_evaluation;
            }
        });
        return evaluation;
    }

    public getPermissions(): {
        transactions: { [value: string]: runtime.Permission };
        lists: { [value: string]: runtime.Permission };
    } {
        const purchases_permissions: runtime.Permission[] = [
            this.user.getPermission({ name: 'TRAN_SALESORD' }),
            this.user.getPermission({ name: 'TRAN_CUSTINVC' }),
            this.user.getPermission({ name: 'TRAN_CASHSALE' })
        ];
        const purchases_returns_permissions: runtime.Permission[] = [
            this.user.getPermission({ name: 'TRAN_RTNAUTH' }),
            this.user.getPermission({ name: 'TRAN_CUSTCRED' })
        ];

        return {
            transactions: {
                tranCashSale: this.user.getPermission({ name: 'TRAN_CASHSALE' }),
                tranCustCred: this.user.getPermission({ name: 'TRAN_CUSTCRED' }),
                tranCustDep: this.user.getPermission({ name: 'TRAN_CUSTDEP' }),
                tranCustPymt: this.user.getPermission({ name: 'TRAN_CUSTPYMT' }),
                tranStatement: this.user.getPermission({ name: 'TRAN_STATEMENT' }),
                tranCustInvc: this.user.getPermission({ name: 'TRAN_CUSTINVC' }),
                tranItemShip: this.user.getPermission({ name: 'TRAN_ITEMSHIP' }),
                tranSalesOrd: this.user.getPermission({ name: 'TRAN_SALESORD' }),
                tranEstimate: this.user.getPermission({ name: 'TRAN_ESTIMATE' }),
                tranRtnAuth: this.user.getPermission({ name: 'TRAN_RTNAUTH' }),
                tranDepAppl: this.user.getPermission({ name: 'TRAN_DEPAPPL' }),
                tranSalesOrdFulfill: this.user.getPermission({ name: 'TRAN_SALESORDFULFILL' }),
                tranFind: this.user.getPermission({ name: 'TRAN_FIND' }),
                tranPurchases: _.max(purchases_permissions),
                tranPurchasesReturns: _.max(purchases_returns_permissions)
            },
            lists: {
                regtAcctRec: this.user.getPermission({ name: 'REGT_ACCTREC' }),
                regtNonPosting: this.user.getPermission({ name: 'REGT_NONPOSTING' }),
                listCase: this.user.getPermission({ name: 'LIST_CASE' }),
                listContact: this.user.getPermission({ name: 'LIST_CONTACT' }),
                listCustJob: this.user.getPermission({ name: 'LIST_CUSTJOB' }),
                listCompany: this.user.getPermission({ name: 'LIST_COMPANY' }),
                listIssue: this.user.getPermission({ name: 'LIST_ISSUE' }),
                listCustProfile: this.user.getPermission({ name: 'LIST_CUSTPROFILE' }),
                listExport: this.user.getPermission({ name: 'LIST_EXPORT' }),
                listFind: this.user.getPermission({ name: 'LIST_FIND' }),
                listCrmMessage: this.user.getPermission({ name: 'LIST_CRMMESSAGE' })
            }
        };
    }

    public isLoggedIn(): boolean {
        return this.user.id > 0 && this.user.role !== 17;
    }

    // @method requireLogin If user not logged in, throw an error
    public requireLogin(): void {
        // Not logged in user:
        // {"id":-4,"name":"-System-","email":"","location":0,"department":0,
        // "role":17,"roleId":"shopper","roleCenter":"WEBSITE","subsidiary":1}
        if (!this.isLoggedIn()) {
            throw unauthorizedError;
        }
    }

    // @method requirePermissions
    // @parameter {Object} options
    public requirePermissions(options: {
        list: string[];
        extraList: string[];
        operator?: PermissionOperator;
    }): void {
        const required_permissions: string[] = (options.list || []).concat(options.extraList || []);
        if (!this.evaluatePermissions(required_permissions, options.operator)) {
            throw forbiddenError;
        }
    }
}

export default new ServiceControllerValidations();
