/**
 * @NApiVersion 2.x
 * @NModuleScope Public
 */

import * as error from 'N/error';
import { ServerRequest, ServerResponse } from 'N/http';
import {
    methodNotAllowedError,
    badRequestError,
    forbiddenError,
    notFoundError
} from './RequestErrors';
import '../../third_parties/underscore.js';
import Configuration from '../Configuration/Configuration';

import ServiceControllerValidations from './ServiceControllerValidations';

export abstract class ServiceController {
    abstract name: string;

    protected options: any = {};

    public request: ServerRequest;

    public response: ServerResponse;

    public method: string;

    public data;

    public service(context) {
        try {
            this.handle(context.request, context.response);
        } catch (ex) {
            this.response = context.response;
            this.request = context.request;
            this.sendError(ex);
        }
    }

    public getMethod(): string {
        return (<string>this.request.method).toLowerCase();
    }

    private _deepObjectExtend(target, source) {
        for (const prop in source) {
            if (prop in target) {
                this._deepObjectExtend(target[prop], source[prop]);
            } else {
                target[prop] = source[prop];
            }
        }
        return target;
    }

    public getOptions(method: string) {
        this.options = _.isFunction(this.options) ? this.options() : this.options;
        return this._deepObjectExtend(this.options.common || {}, this.options[method] || {});
    }

    public validateRequest(validation_options) {
        _.each(validation_options, function(validation_option, validation_name) {
            if (validation_option && _.isFunction(ServiceControllerValidations[validation_name])) {
                ServiceControllerValidations[validation_name](validation_option);
            }
        });
    }

    public handle(request: ServerRequest, response: ServerResponse) {
        this.request = request;
        this.response = response;
        this.method = this.getMethod();
        this.data = JSON.parse(this.request.body || '{}');
        const options = this.getOptions(this.method);
        Configuration.setConfig(this.getSite(), this.getDomain());

        this.validateRequest(options);

        if (_.isFunction(this[this.method])) {
            const result = this[this.method]();

            return this.sendContent(result, options);
        }
        return this.sendError(methodNotAllowedError);
    }

    public sendContent(content: string, options) {
        // Default options
        options = _.extend({ status: 200, cache: false, jsonp_enabled: false }, options || {});

        // We set a custom status
        this.response.setHeader({
            name: 'Custom-Header-Status',
            value: parseInt(options.status, 10).toString()
        });

        // The content type will be here
        let content_type: string = null;

        // If its a complex object we transform it into an string
        if (_.isArray(content) || _.isObject(content)) {
            content_type = 'application/json';
            content = JSON.stringify(content);
        }

        // If you set a jsonp callback this will honor it
        if (options.jsonp_enabled && this.request.parameters.jsonp_callback) {
            content_type = 'application/x-javascript';
            content = this.request.parameters.jsonp_callback + '(' + content + ');';
        }

        // Set the response cache option
        if (options.cache) {
            this.response.setCdnCacheable(options.cache);
        }

        if (options.content_type) {
            content_type = options.content_type;
        }

        // Content type was set so we send it
        if (content_type) {
            this.response.setHeader({
                name: 'Content-Type',
                value: content_type
            });
        }

        if (!content) {
            throw notFoundError;
        }

        this.response.write({ output: content });
    }

    public processError(e) {
        let status = 500;
        let code = 'ERR_UNEXPECTED';
        let message = 'error';

        if (e.getCode && e.getDetails) {
            code = e.getCode();
            message = e.getDetails();
            status = badRequestError.status;
        } else if (_.isObject(e) && !_.isUndefined(e.status)) {
            status = e.status;
            code = e.code;
            message = e.message;
        } else {
            const theError = error.create(e);
            code = theError.id;
            message = theError.message;
        }

        if (code === 'INSUFFICIENT_PERMISSION') {
            status = forbiddenError.status;
            code = forbiddenError.code;
            message = forbiddenError.message;
        }

        const content = {
            errorStatusCode: status.toString(),
            errorCode: code,
            errorMessage: message,
            errorDetails: null,
            errorMessageParams: null
        };

        if (e.errorDetails) {
            content.errorDetails = e.errorDetails;
        }

        if (e.messageParams) {
            content.errorMessageParams = e.messageParams;
        }

        return content;
    }

    public sendError(e, options?) {
        options = _.extend({ jsonp_enabled: false }, options || {});

        _.extend(e, { serviceControllerName: this.name });

        const error_content = this.processError(e);
        let content_type = 'application/json';

        this.response.setHeader({
            name: 'Custom-Header-Status',
            value: error_content.errorStatusCode
        });

        let content: string = JSON.stringify(error_content);

        // If you set a jsonp callback this will honor it
        if (options.jsonp_enabled && this.request.parameters.jsonp_callback) {
            content_type = 'application/x-javascript';
            content = this.request.parameters.jsonp_callback + '(' + content + ');';
        }

        this.response.setHeader({
            name: 'Content-Type',
            value: content_type
        });

        this.response.write({ output: content });
    }

    private getSite(): number {
        return Number(this.request.parameters.n);
    }

    private getDomain(): string {
        // domain parameter exist only when is not single domain
        return (
            this.request.parameters.domain ||
            this.request.url.replace(/^https?:\/\/([^/?#:]+).*$/, '$1')
        );
    }
}
