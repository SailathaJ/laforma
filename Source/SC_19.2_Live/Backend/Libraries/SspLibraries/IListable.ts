export interface IListable {
    filter: string;
    order: number;
    sort: string;
    from: number;
    to: number;
    page: number;
}
