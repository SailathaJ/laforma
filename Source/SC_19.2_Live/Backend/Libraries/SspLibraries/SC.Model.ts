import '../../third_parties/underscore.js';

import Events = require('./Events');
import { Column } from 'N/search'

export interface ColumnMap {[name: string]: Column}

export class SCModel extends Events {
    constructor() {
        super();
    }

    // @method sanitizeString Remove any HTML code form the string
    // @param {String} text String with possible HTML code in it
    // @return {String} HTML-free string
    sanitizeString(text: string): string {
        // Could this be related to SC validation?
        return text
            ? text
                  .replace(/<br>/g, '\n')
                  .replace(/</g, '&lt;')
                  .replace(/\>/g, '&gt;')
            : '';
    }
}
