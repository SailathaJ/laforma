/**
 * @NApiVersion 2.x
 * @NModuleScope Public
 */

import * as search from 'N/search';
import * as record from 'N/record';
import * as log from 'N/log';

export = {
    getFieldsAsObject(rec:any, values?:any,texts?:any,sublist_name?:string)
    {
        var obj = {};

        if(sublist_name)
        {
            _.each(values,function(value,key){
                obj[key] = rec.getCurrentSublistValue({
                    sublistId: sublist_name,
                    fieldId: value
                });
            });
            _.each(texts,function(value,key){
                obj[key] = rec.getCurrentSublistText({
                    sublistId: sublist_name,
                    fieldId: value
                });
            });
        }
        else
        {
            _.each(values,function(value,key){
                obj[key] = rec.getValue({fieldId: value});
            });
            _.each(texts,function(value,key){
                obj[key] = rec.getText({fieldId: value});
            });
        }
        return obj;
    },

    iterateSublist(rec:any, sublist_name:string, fn:any)
    {
        const lines_length = rec.getLineCount({
            sublistId: sublist_name
        });

        for (let line = 0; line < lines_length; line++) {
            rec.selectLine({
                sublistId: sublist_name,
                line: line
            });

            var result = fn(line);
            if(result === false)
            {
                break;
            }
        }
    },

    iterateAllSearchResults(type, columns, filters, page, fn)
    {
        const paged = !!page ? {
            pageSize: 20
        } : {};
        const paged_search = search.create({
            type: type,
            columns: columns,
            filters: filters
        }).runPaged(paged);

        if(paged_search.count == 0) return;

        if(!!page){
            const my_page = paged_search.fetch({index: page-1});
            my_page.data.forEach(fn);

            return my_page.pagedData.count;
        }else {
            paged_search.pageRanges.forEach(function(pageRange){
            const my_page = paged_search.fetch({index: pageRange.index});
                my_page.data.forEach(fn);
            });
        }
    }
};
