import '../../third_parties/underscore.js';

import Events = require('./Events');

// This stands for SuiteCommerce
const SC = {};

class Application extends Events {
    constructor() {
        super();
    }
}

export = new Application();
