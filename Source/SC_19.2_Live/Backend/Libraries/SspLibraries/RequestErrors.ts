export const badRequestError = {
    status: 400,
    code: 'ERR_BAD_REQUEST',
    message: 'Bad Request'
};

export const unauthorizedError = {
    status: 401,
    code: 'ERR_USER_NOT_LOGGED_IN',
    message: 'Not logged In'
};

export const sessionTimedOutError = {
    status: 401,
    code: 'ERR_USER_SESSION_TIMED_OUT',
    message: 'User session timed out'
};

export const forbiddenError = {
    status: 403,
    code: 'ERR_INSUFFICIENT_PERMISSIONS',
    message: 'Insufficient permissions'
};

export const notFoundError = {
    status: 404,
    code: 'ERR_RECORD_NOT_FOUND',
    message: 'Not found'
};

export const methodNotAllowedError = {
    status: 405,
    code: 'ERR_METHOD_NOT_ALLOWED',
    message: 'Sorry, you are not allowed to perform this action.'
};

export const invalidItemsFieldsAdvancedName = {
    status: 500,
    code: 'ERR_INVALID_ITEMS_FIELDS_ADVANCED_NAME',
    message: 'Please check if the fieldset is created.'
};
