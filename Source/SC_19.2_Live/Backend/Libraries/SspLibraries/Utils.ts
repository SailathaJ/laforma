import '../../third_parties/underscore.js';

/**
 *
 * TODO: We should take a look at this when migrating Utils backend file and
 * create some new classes instead of a large file full of unrelated functions.
 */

export function deepExtend(target: any = {}, source: any = {}) {
    if (_.isArray(target) || !_.isObject(target)) {
        return source;
    }

    _.each(source, function(value, key) {
        if (key in target) {
            target[key] = deepExtend(target[key], value);
        } else {
            target[key] = value;
        }
    });

    return target;
}

export function getPathFromObject(object: object, path?: string, default_value?: any): any {
    if (!path) {
        return object;
    }
    if (object) {
        const tokens: string[] = path.split('.');
        let prev: object = object;
        let n = 0;

        while (!_.isUndefined(prev) && n < tokens.length) {
            prev = prev[tokens[n++]];
        }

        if (!_.isUndefined(prev)) {
            return prev;
        }
    }

    return default_value;
}

export function setPathFromObject(object: object, path: string, value: any): void {
    const tokens = path.split('.');
    let prev = object;

    for (let token_idx = 0; token_idx < tokens.length - 1; ++token_idx) {
        const current_token = tokens[token_idx];

        if (_.isUndefined(prev[current_token])) {
            prev[current_token] = {};
        }
        prev = prev[current_token];
    }

    prev[_.last(tokens)] = value;
}
